<?php
# Enviador de correo
# Dominio publico. 2004. Sin garantias.  vtamra@pasosdeJesus.org
# Referencias: 
#	http://pear.php.net/manual/en/package.mail.mail.factory.php
#	http://capacitacion.pasosdeJesus.org

# Pasar al menos: recepetor, tema, redireccionar
# los demas campos seran enviados como parte del cuerpo

# El siguiente es una porcion de una pagina HTML que incluya un formulario
# que envia datos por este script
/*
 <FORM ACTION="/herramientas/envia_correo.php" METHOD="post">
	<INPUT NAME="tema" VALUE="Forma de Contacto de sitio.org" TYPE="hidden">
	<INPUT NAME="receptor" VALUE="correo@sitio.org" TYPE="hidden">
        <INPUT NAME="redireccionar" VALUE="http://sitio.org/gracias.html" TYPE="hidden">
        <INPUT NAME="dato1">
 </FORM>
*/


include "Mail.php";

/**
 * Escapa el valor de una variable o de valores en un arreglo.
 * Si $v es null retorna ''
 * Agradecimientos por correciones a garcez@linuxmail.org
 *
 * Tomada de SIVeL sivel.sf.net
 *
 * @param string  $v       Nombre de variable 
 * @param handle  &$db     Conexion a BD.
 * @param integer $maxlong Longitud mÃxima
 *
 * @return string Cadena escapada
 */
function var_escapa($v, &$db = null, $maxlong = 1024)
{
    if (isset($v)) {
        if (is_array($v)) {
            $r = array();
            foreach ($v as $k => $e) {
                $r[$k] = var_escapa($e, $db, $maxlong);
            }
            return $r;
        } else {
            /** Evita buffer overflows */
            $nv = substr($v, 0, $maxlong);

            /** Evita falla %00 en cadenas que vienen de HTTP */
            $p1 = str_replace("\0", ' ', $v);

            /** Evita XSS */
            $p2 = htmlspecialchars($p1);

	    /** Evita inyeccion de codigo SQL */
            if (isset($db) && $db != null && !PEAR::isError($db)) {
                $p3 = $db->escapeSimple($p2);
            } else {
                // Tomado de libreria de Pear DB/pgsql.php
                $p3 = (!get_magic_quotes_gpc())?str_replace(
                    "'", "''",
                    str_replace('\\', '\\\\', $p2)
                ) : $p2;
            }
            return $p3;
        }
    } else {
        return '';
    }
}

$ref = var_escapa($_SERVER['HTTP_REFERER']);
if (($pp = strpos($ref,  '.')) == FALSE) {
	die("HTTP_REFERER errado (1)");
}
if (($qr = substr(strtoupper($ref), $pp + 1, 16)) != "PASOSDEJESUS.ORG") {
	die("HTTP_REFERER errado (2)");
}

$m=Mail::factory('smtp',array('host' => '127.0.0.1'));
if ($m==NULL) {
	die("No puede enviar correo");
}

$rec = str_replace("\n", "", var_escapa($_REQUEST['receptor']));
$rec = str_replace("\r", "", $rec);
if ($rec === "") {
	print_r($_REQUEST);
	die("No envia correos sin receptor");
}
$tema = var_escapa($_REQUEST['tema']);
if ($tema === "") {
	die("No envia correos sin tema");
}
$red = var_escapa($_REQUEST['redireccionar']);
if ($tema === "") {
	die("No envia correos sin redireccion");
}
$headers['To'] = $rec;
$headers['Subject'] = $tema;
$headers['From'] = 'info@pasosdeJesus.org';

$body="Los siguientes datos fueron enviados desde el formulario \n$ref\n\n";
foreach($_REQUEST as $k => $v) {
	if ($k != 'receptor' && $k != 'tema' && $k != 'redireccionar' 
		&& $k != 'Enviar' && k != 'Submit') {
		$body .= var_escapa($k) . ": " . print_r(var_escapa($v), true) . "\n";
	}
}

$m->send($rec, $headers, $body);
header("Location: " . $red);

?>
