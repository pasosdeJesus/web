#!/bin/sh
# Renueva certificado, usable en cron

u=`whoami`
if (test "$u" != "root") then {
  echo "Correr como root";
  exit 1;
} fi;

f=`date +"%Y-%m-%d"`

if (test 1 == 2) then {
/usr/local/bin/letsencrypt certonly --agree-tos \
  --email vtamara@pasosdeJesus.org --cert-name pasosdeJesus.org \
  --force-renewal --expand -n --webroot \
  -w /var/www/pasosdeJesus/pasosdeJesus80/ -d pasosdeJesus.org -d www.pasosdeJesus.org \
  -w /var/www/pasosdeJesus/aprendiendo80/ -d aprendiendo.pasosdeJesus.org \
  -w /var/www/pasosdeJesus/dhobsd80/ -d dhobsd.pasosdeJesus.org \
  -w /var/www/pasosdeJesus/fe80/ -d fe.pasosdeJesus.org \
  -w /var/www/pasosdeJesus/mt7780/ -d mt77.pasosdeJesus.org \
  -w /var/www/pasosdeJesus/seguridad80/ -d seguridad.pasosdeJesus.org \
  -w /var/www/pasosdeJesus/traduccion80/ -d traduccion.pasosdeJesus.org \
  -w /var/www/pasosdeJesus/vtamara/ -d vtamara.pasosdeJesus.org

c="$?"
if (test "$c" != "0") then {
  exit $c;
} fi;
} fi;

ls -lat /etc/letsencrypt/live/
cp /etc/letsencrypt/live/pasosdeJesus.org/fullchain.pem /etc/ssl/pasosdeJesus-le.crt
cp /etc/letsencrypt/live/pasosdeJesus.org/privkey.pem /etc/ssl/private/pasosdeJesus-le.key
rcctl -d restart nginx
