// @author Vladimir Tamara vtamara@pasosdeJesus.org
// @copyright Dominio Publico de acuerdo a legislación colombiana


function calcDonacion() {
	var vh = $('#valor').val();
	var cristianos = $('input[id=cristianos]:checked').length
        // Por Lucas 9:49-50
	var p = 1;
	if ((cristianos==1 && vh < 60000) || (cristianos == 0 && vh >= 60000 && vh < 80000)) {
		p = 2;
	} else if ((cristianos==1 && vh>=60000 && vh<80000) || (cristianos ==0 && vh >= 80000 && vh < 100000)) {
		p = 3;
	} else if ((cristianos==1 && vh >= 80000) || (cristianos == 0 && vh >= 100000)) {
		p = 4;
	}
	$("#prioridad").val(p);
	
	var plan = $('input:radio[name=plan]:checked').val();
	if (plan == 1) {
		$("input:radio[name=h1]:nth(0)").attr("checked", true);
		$("input:radio[name=so1]:nth(0)").attr("checked", true);
		$("input:radio[name=ci]:nth(0)").attr("checked", true);
		$("input:radio[name=sr]:nth(0)").attr("checked", true);
		$("input:radio[name=c1]:nth(0)").attr("checked", true);
		$("input:radio[name=i]:nth(0)").attr("checked", true);
		$("input:radio[name=as1]:nth(0)").attr("checked", true);
		$("input:radio[name=sop]:nth(0)").attr("checked", true);
		$("input:radio[name=mon]:nth(0)").attr("checked", true);
		$("input:radio[name=cap]:nth(0)").attr("checked", true);
	} else if (plan == 2) {
		$("input:radio[name=h1]:nth(1)").attr("checked", true);
		$("input:radio[name=so1]:nth(1)").attr("checked", true);
		$("input:radio[name=ci]:nth(1)").attr("checked", true);
		$("input:radio[name=sr]:nth(1)").attr("checked", true);
		$("input:radio[name=c1]:nth(1)").attr("checked", true);
		$("input:radio[name=i]:nth(1)").attr("checked", true);
		$("input:radio[name=as1]:nth(1)").attr("checked", true);
		$("input:radio[name=sop]:nth(1)").attr("checked", true);
		$("input:radio[name=mon]:nth(1)").attr("checked", true);
		$("input:radio[name=cap]:nth(1)").attr("checked", true);
	} else if (plan == 3) {
		$("input:radio[name=h1]:nth(0)").attr("checked", true);
		$("input:radio[name=so1]:nth(1)").attr("checked", true);
		$("input:radio[name=ci]:nth(1)").attr("checked", true);
		$("input:radio[name=sr]:nth(0)").attr("checked", true);
		$("input:radio[name=c1]:nth(0)").attr("checked", true);
		$("input:radio[name=i]:nth(0)").attr("checked", true);
		$("input:radio[name=as1]:nth(1)").attr("checked", true);
		$("input:radio[name=sop]:nth(1)").attr("checked", true);
		$("input:radio[name=mon]:nth(1)").attr("checked", true);
		$("input:radio[name=cap]:nth(1)").attr("checked", true);
	}
	if (plan == 1 || plan == 2 || plan == 3) {
		$("input:radio[name=h1]").attr("readonly", true);
		$("input:radio[name=so1]").attr("readonly", true);
		$("input:radio[name=ci]").attr("readonly", true);
		$("input:radio[name=sr]").attr("readonly", true);
		$("input:radio[name=c1]").attr("readonly", true);
		$("input:radio[name=i]").attr("readonly", true);
		$("input:radio[name=as1]").attr("readonly", true);
		$("input:radio[name=sop]").attr("readonly", true);
		$("input:radio[name=mon]").attr("readonly", true);
		$("input:radio[name=cap]").attr("readonly", true);

	} else {
		$("input:radio[name=h1]").attr("readonly", false);
		$("input:radio[name=so1]").attr("readonly", false);
		$("input:radio[name=ci]").attr("readonly", false);
		$("input:radio[name=sr]").attr("readonly", false);
		$("input:radio[name=c1]").attr("readonly", false);
		$("input:radio[name=i]").attr("readonly", false);
		$("input:radio[name=as1]").attr("readonly", false);
		$("input:radio[name=sop]").attr("readonly", false);
		$("input:radio[name=mon]").attr("readonly", false);
		$("input:radio[name=cap]").attr("readonly", false);
	}

	var nc=["so1", "ci"];
	var ha;
	if (plan == 2) {
		ha=[1, 2];
	} else {
		ha=[4, 3];
	}
	var spm = 0;	
	for(var i=0; i < nc.length; i++) {
		var q=$("input:radio[name=" + nc[i] + "]:checked").val();
		$("input#h" + nc[i]).val(ha[i] + "h");
		if (q == "n") {
			$("input#v" + nc[i]).val(ha[i]*vh);
			spm += ha[i]*vh;
		} else {
			$("input#v" + nc[i]).val("");
		}
	}
	spm += Math.abs($("input#dd1").val());
	$("input#tpm").val(spm);

	nc = ["sr", "as1", "sop", "mon", "cap"];
	if (plan == 2) {
		ha = [1, 2, 1, 3, 2];
	} else {
		ha = [4, 6, 2, 3, 4];
	}
	sa = 0;
	for(var i=0; i < nc.length; i++) {
		var q=$("input:radio[name=" + nc[i] + "]:checked").val();
		$("input#h" + nc[i]).val(ha[i] + "h");
		if (q == "n") {
			$("input#v" + nc[i]).val(ha[i]*vh);
			sa += ha[i]*vh;
		} else {
			$("input#v" + nc[i]).val("");
		}
	}
	nc = ["c1", "i"];
	va = [200000, 300000];
	for(var i=0; i < nc.length; i++) {
		var q=$("input:radio[name=" + nc[i] + "]:checked").val();
		if (q == "n") {
			$("input#v" + nc[i]).val(va[i]);
			sa += va[i];
		} else {
			$("input#v" + nc[i]).val("");
		}
	}
	sa += Math.abs($("input#dd2").val());
	$("input#ta").val(sa);

}

$(document).ready(function(){
	$('input').change(calcDonacion);
	calcDonacion();
})

