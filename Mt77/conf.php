<?php # Par�metros de configuraci�n 
# Generado autom�ticamente por conf.sh.  No editar
/* Configuration variables */
$PROYECTO="Mt77";
$PRY_VERSION="1.0a1";
$PRY_DESC="Motor de b�squeda veloz, preciso y apto para indexaci�n distribuida";
$URLSITE="http://mt77.pasosdejesus.org/";
$ACT_PROC="act-scp";
$MES_ACT="Enero de 2010";
$FECHA_ACT="06/01/2010";
$AWK="/usr/bin/awk";
$CP="/bin/cp";
$CVS="/usr/bin/cvs";
$ED="/bin/ed";
$FIND="/usr/bin/find";
$GZIP="/usr/bin/gzip";
$MAKE="/usr/bin/make";
$MKDIR="/bin/mkdir";
$MV="/bin/mv";
$NCFTPPUT="";
$PERL="/usr/bin/perl";
$RM="/bin/rm";
$SCP="/usr/bin/scp";
$SED="/usr/bin/sed";
$TAR="/bin/tar";
$TOUCH="/usr/bin/touch";
$W3M="/usr/local/bin/w3m";
$W3M_OPT="";
$ZIP="/usr/local/bin/zip";
$INSBIN="/usr/local/bin";
$INSDOC="/usr/local/share/doc/Mt77";
$INSDATA="/usr/local/share/Mt77";
?>
