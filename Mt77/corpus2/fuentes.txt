Evangelios de dominio p�blico
http://de.geocities.com/nuestroamigoJesus/

esrv.txt 
Santa Biblia: Reina Valera. 
Dominio P�blico.  
http://www.ccel.org/ccel/bible/esrv.html 

essav.txt
Sagradas Escrituras.
Dominio P�blico.  
http://www.ccel.org/ccel/bible/essav.html

M�ximas Morales en Ilocano y Castellano Para Uso de los Ni�os.
Dominio P�blico.  
http://www.gutenberg.org/files/16149/16149-8.txt

Reina Valera New Testament of the Bible 1602
Dominio P�blico.  
http://www.gutenberg.org/dirs/etext04/8va0210.txt

Reina Valera New Testament of the Bible 1858
Dominio P�blico. 
http://www.gutenberg.org/dirs/etext04/8va5810.txt

Reina Valera New Testament of the Bible 1862
http://www.gutenberg.org/dirs/etext04/8vato P�rez Galdos

Bail�n
http://www.gutenberg.org/files/14311/14311-8.txt
6210.txt

Reina Valera New Testament of the Bible 1865
Dominio P�blico. 
http://www.gutenberg.org/dirs/etext04/8va6510.txt

Reina Valera New Testament of the Bible 1909
Dominio P�blico.  
http://www.gutenberg.org/dirs/etext04/8va0910.txt

sages10.txt
Sagradas Escrituras Version Antigua
Dominio P�blico. 
http://www.gutenberg.org/etext/6528

La Ni�a de Luzmela. Concha Espina. 
Dominio P�blico.  
http://www.gutenberg.org/files/11657/11657-8.txt


La Puerta de Bronce y Otros Cuentos. Manuel Romero de Terreros, Marqu�s de San Francisco
Dominio P�blico.  
http://www.gutenberg.org/files/11669/11669-8.txt

Platero Y Yo. Juan Ramon Jimenez.
Dominio P�blico.  
http://www.ibiblio.org/pub/docs/books/gutenberg/etext06/8pltr10.txt

Relacion historica de los sucesos de la rebelion de Jose Gabriel
Tupac-Amaru en las provincias del Peru, el a�o de 1780.
Dominio P�blico.  
http://www.gutenberg.org/files/10293/10293-8.txt

Torquemada en la hoguera. B. P�rez Galdos
Dominio P�blico. 
http://www.gutenberg.org/files/15206/15206-8.txt

Bail�n.  Benito P�rez Galdos
Dominio P�blico. 
http://www.gutenberg.org/files/14311/14311-8.txt

C�diz.  Benito P�rez Gald�s
Dominio P�blico.  
http://www.gutenberg.org/files/21906/21906-8.txt

La desheredada.  Benito P�rez Gald�s
Dominio P�blico.  
http://www.gutenberg.org/files/25956/25956-8.txt

Do�a Perfecta.  Benito P�rez Gald�s
Dominio P�blico.  
http://www.gutenberg.org/files/15725/15725-8.txt

Electra.  Benito P�rez Gald�s.
Dominio P�blico.  
http://www.gutenberg.org/files/28002/28002-8.txt

Un faccioso m�s y algunos frailes menos.  Benito P�rez Gald�s
Dominio P�blico.  
http://www.gutenberg.org/files/17443/17443-8.txt

Fortunata y Jacinta dos historias de casadas.  Benito P�rez Gald�s
Dominio P�blico.  
http://www.gutenberg.org/files/17013/17013-8.txt

Marianela.  Benito P�rez Gald�s
Dominio P�blico.  
http://www.gutenberg.org/files/17340/17340-8.txt


Misericordia.  Benito P�rez Gald�s.
Dominio P�blico.  
http://www.gutenberg.org/files/21831/21831-8.txt


Trafalgar.  Benito P�rez Gald�s.
Dominio P�blico.  
http://www.gutenberg.org/files/16961/16961-8.txt


Mindanao: Su Historia y Geograf�a. Jos� Nieto Aguilar
Dominio P�blico.  
http://www.gutenberg.org/files/15334/15334-8.txt


Rese�a Veridica de la Revoluci�n Filipina.  Emilio Famy Aguinaldo
Dominio P�blico.  
http://www.gutenberg.org/files/14307/14307-8.txt


Novelas Cortas.  Pedro Antonio de Alarc�n
Dominio P�blico.  
http://www.gutenberg.org/files/15532/15532-8.txt


Filosofia fundamental.  Jaime Balmes
Dominio P�blico.  
http://www.gutenberg.org/files/13608/13608-8.txt

La Mejor Cocinera: Recetas de Cocina.  Calleja
Dominio P�blico.  
http://www.gutenberg.org/dirs/etext05/7mjrc10.txt

