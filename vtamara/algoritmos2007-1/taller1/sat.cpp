/** 
 * Decide si una f�rmula en l�gica de predicados es satisfasible 
 * recorriendo el espacio de b�squeda completo sin heur�stica.
 *
 * @author Vladimir T�mara Pati�o. 2006. Dominio p�blico. Sin garant�as.
 */

#include <iostream>
#include <sstream>
#include <map>
#include "arbin.hpp"
#include "fbf.hpp"
using namespace std;


/* Recibe una asignaci�n y si es posible retorna true y la tranforma
 * en la siguiente */
bool siguiente_asig(map<string, bool> &asig)
{
	map<string, bool>::iterator e=asig.begin();
	int n;
	if (e==asig.end()) {
		return false;
	}
	for(n=asig.size() ; e!=asig.end() && e->second==true;  n--,e++) {
		e->second=false;
	}
	if (e==asig.end() || n==0) {
		return false;
	}
	//cout<<"Cambiando a true "<<e->first<<endl;
	e->second=true;

	return true;
}

long long pow2(int n) {
	return 1<<n;
}

int main() {
	int ner=0;
	string f;
	cout << "Este programa determina si existe o no una valuaci�n que "<<endl
		<<"haga cierta una f�rmula en l�gica de predicados."<<endl
		<<"Las f�rmulas deben ser: "<<endl
		<<"* p,q y dem�s variables de una o m�s letras iniciadas por letra min�scula"<<endl
		<<"* -(f) negaci�n de la fbf f"<<endl
		<<"* (f)&(g) conjunci�n de las fbfs f y g"<<endl
		<<"* (f)|(g) disyunci�n de las fbfs f y g"<<endl<<endl;
	cout << "Ejemplos de f�rmulas:"<<endl
		<< "x"<<endl
		<< "(x)|(y)"<<endl
		<< "((x)&(y))&(z)"<<endl<<endl;

	cout << "�F�rmula por emplear? ";
	cin>>f;
	//f="((x)&(y))&(z)";
	arbin<string> fbf=string_a_fbf(f,0,f.size());
	cout << "En notaci�n interna: "<<fbf<<endl;

	bool sat=false;	
	map<string, bool> v;
	saca_variables(fbf.nodo_raiz(), v);
	long long t=pow2((int)v.size());
	cout <<"Buscando en espacio de tama�o "<<t<<" ..."<<endl<<"%: ";
	int i=0,lp=0;
	do {
		//cout << "Las variables son: "<<v<<endl;
		if (evaluar(fbf.nodo_raiz(),v)==true) {
			sat=true;
		}
		i++;
		if ((100*i)/t>lp+5) {
			lp=(100*i)/t;
			cout<<" "<<lp;
		}
	} while (!sat && siguiente_asig(v));
	cout<<endl;
	if (sat) {
		cout<<"Es satisfasible con "<<v<<endl;
		return 0;
	}
	cout<<"No es satisfasible"<<endl;
	return 1;
}


