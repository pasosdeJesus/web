/** Pruebas de regresi�n para la clase Arbol Binario 
 * @author Vladimir T�mara Pati�o. 2006. Dominio p�blico. Sin garant�as.
 */

#include <iostream>
#include <sstream>
#include "arbin.hpp"
using namespace std;

template <class T>
std::ostream &operator<<(std::ostream &os, list<T> &l)
{
	typename list<T>::iterator i;
	os<<"[";
	for(i=l.begin(); i!=l.end(); i++) {
		if (i!=l.begin()) {
			os<<", ";
		}
		os<<*i;
	}
	os<<"]";
	cerr << endl;
	return os;
}

template <class T>
string list2string(list<T> l) {
	string r="";
	typename list<T>::iterator i;
	r+="[";
	for(i=l.begin(); i!=l.end(); i++) {
		if (i!=l.begin()) {
			r+=", ";
		}
		ostringstream ss;
		ss<<*i<<flush;
		r+=ss.str();
	}
	r+="]";
	return r;
} 


int main() {
	int ner=0;
	cout << "Comenzando con �rbol vac�o" << endl;

	arbin<int> a;
	if (!a.es_vacio()) {
		cerr << " * es_vacio deber�a ser true"<<endl;
		ner++;
	}
	string sp=list2string(a.preorden());
	if (sp!="[]") {
		cerr << " * Preorden de vaci� deber�a ser lista vac�a, no " 
			<< sp <<endl;
		ner++;
	}
	sp=list2string(a.inorden());
	if (sp!="[]") {
		cerr << " * Inorden de vaci� deber�a ser lista vac�a, no " 
			<< sp << endl;
		ner++;
	}
	sp=list2string(a.posorden());
	if (sp!="[]") {
		cerr << " * Posorden de vaci� deber�a ser lista vac�a, no " 
			<< sp << endl;
		ner++;
	}
	sp=list2string(a.niveles());
	if (sp!="[]") {
		cerr << " * niveles de vaci� deber�a ser lista vac�a, no " 
			<< sp << endl;
		ner++;
	}
	if (a.altura()!=0) {
		cerr<<"Altura de vac�o deber�a ser 0";
	}

	cout << "Probando inserci�n y recorridos" << endl;
	a.inserta_ord(-3);
	sp=list2string(a.preorden());
	if (sp!="[-3]") {
		cerr << " * Preorden de primero deber�a ser [-3], no " 
			<< sp <<endl;
		ner++;
	}
	sp=list2string(a.inorden());
	if (sp!="[-3]") {
		cerr << " * Inorden de primero deber�a ser [-3], no " 
			<< sp << endl;
		ner++;
	}
	sp=list2string(a.posorden());
	if (sp!="[-3]") {
		cerr << " * Posorden de primero deber�a ser [-3], no " 
			<< sp << endl;
		ner++;
	}
	sp=list2string(a.niveles());
	if (sp!="[-3]") {
		cerr << " * niveles de primero deber�a ser [-3], no " 
			<< sp << endl;
		ner++;
	}
	if (a.altura()!=1) {
		cerr<<" * Tras insertar primer elemento se esperaba altura 1";
		ner++;
	}
	a.inserta_ord(5);
	sp=list2string(a.preorden());
	if (sp!="[-3, 5]") {
		cerr << " * Preorden de segundo deber�a ser [-3,5], no " 
			<< sp <<endl;
		ner++;
	}
	sp=list2string(a.inorden());
	if (sp!="[-3, 5]") {
		cerr << " * Inorden de segundo deber�a ser [-3, 5], no " 
			<< sp << endl;
		ner++;
	}
	sp=list2string(a.posorden());
	if (sp!="[5, -3]") {
		cerr << " * Posorden de segundo deber�a ser [5,-3], no " 
			<< sp << endl;
		ner++;
	}
	sp=list2string(a.niveles());
	if (sp!="[-3, 5]") {
		cerr << " * niveles de primero deber�a ser [-3,5], no " 
			<< sp << endl;
		ner++;
	}
	if (a.altura()!=2) {
		cerr<<" * Tras insertar segundo elemento se esperaba altura 2";
		ner++;
	}

	a.inserta_ord(-5);
	sp=list2string(a.preorden());
	if (sp!="[-3, -5, 5]") {
		cerr << " * Preorden de tercero deber�a ser [-3,-5,5], no " 
			<< sp <<endl;
		ner++;
	}
	sp=list2string(a.inorden());
	if (sp!="[-5, -3, 5]") {
		cerr << " * Inorden de tercero deber�a ser [-5, -3, 5], no " 
			<< sp << endl;
		ner++;
	}
	sp=list2string(a.posorden());
	if (sp!="[-5, 5, -3]") {
		cerr << " * Posorden de tercero deber�a ser [-5, 5,-3], no " 
			<< sp << endl;
		ner++;
	}
	sp=list2string(a.niveles());
	if (sp!="[-3, -5, 5]") {
		cerr << " * niveles de primero deber�a ser [-3,-5,5], no " 
			<< sp << endl;
		ner++;
	}
	if (a.altura()!=2) {
		cerr<<" * Tras insertar tercer elemento se esperaba altura 2";
		ner++;
	}

	cout << "Probando modificaci�n " << endl;
	arbin<int> b(2);
	a.pone_izq(b);
	sp=list2string(a.niveles());
	if (sp!="[-3, 2, 5]") {
		cerr << " * niveles de modificado 1 no deber�a ser " 
			<< sp << endl;
		ner++;
	}
	a.pone_der(b);
	sp=list2string(a.niveles());
	if (sp!="[-3, 2, 2]") {
		cerr << " * niveles de modificado 2 no deber�a ser " 
			<< sp << endl;
		ner++;
	}

	cout << "Probando asignaci�n y constructora de copia" << endl;
	arbin<int> c(a);
	sp=list2string(c.niveles());
	if (sp!="[-3, 2, 2]") {
		cerr << " * niveles de construido por copia no deber�a ser " 
			<< sp << endl;
		ner++;
	}
	c=b;
	sp=list2string(b.niveles());
	if (sp!="[2]") {
		cerr << " * niveles de asignado no deber�a ser " 
			<< sp << endl;
		ner++;
	}


	cout <<"N�mero de errores: "<<ner<<endl;

	return ner?1:0;
}


