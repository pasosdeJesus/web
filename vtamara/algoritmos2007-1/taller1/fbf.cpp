/** Formulas bien formadas (fbf).   
 * Ver documentaci�n b�sica de cada funci�n en encabezado.
 * @author Vladimir T�mara Pati�o. 2006. Dominio p�blico. Sin garant�as.
 */

#include <iostream>
#include <sstream>
#include <map>
#include "arbin.hpp"
#include "fbf.hpp"
using namespace std;


int 
par_cierra(string fbf, int ini, int fin) 
{
	assert(fbf[ini]=='(');
	int pf=fin;
	int cp=1;
	for(int i=ini+1; i<fin && cp>0; i++) {
		if (fbf[i]==')') {
			cp--;
			if (cp==0) {
				pf=i;
			}
		}
		else if (fbf[i]=='(') {
			cp++;
		}
	}
	return pf;
}

arbin<string> 
string_a_fbf(string fbf, int ini, int fin) 
{
	assert(ini>=0);
	assert(fin<=fbf.length());
	if (fbf[ini]=='-') { /* Negaci�n */
		int pcf=par_cierra(fbf, ini+1, fin);
		if (pcf>=fin) {
			cerr << "* No cerr� par�ntesis iniciado en posici�n "
				<<ini+1<<endl;
			throw exception();
		}
		if (pcf!=fin-1) {
			cerr << "* Datos no esperados despu�s de parentesis de "
				<< "la posici�n "<<pcf<<endl;
			throw exception();
		}
		arbin<string> si=string_a_fbf(fbf, ini+2,pcf);
		arbin<string> a("-", si);
		return a;
	}
	else if (fbf[ini]=='(') { /* Op. Binario */
		int pcf=par_cierra(fbf, ini, fin);
		if (pcf>=fin) {
			cerr << "* No cerr� par�ntesis iniciado en posici�n "
				<<ini<<endl;
			throw exception();
		}
		if (pcf>=fin-1) {
			cerr << "* Falt� operador binario despu�s de "
				<<"cerrar par�ntesis en posici�n "<<pcf<<endl;
			throw exception();
		}
		if (fbf[pcf+1]!='|' && fbf[pcf+1]!='&') {
			cerr << "* Operador desconocido en posici�n "
				<<pcf+1<<endl;
			throw exception();
		}
		if (fbf[pcf+2]!='(') {
			cerr << "* Despu�s de operador falta abrir par�ntesis "
				<<pcf+1<<endl;
			throw exception();
		}
		int pcf2=par_cierra(fbf, pcf+2, fin);
		if (pcf2>=fin) {
			cerr << "* No cerr� par�ntesis iniciado en posici�n "
			     << pcf+2 << " (pcf2="<<pcf2<<")"<< endl;
			throw exception();
		}
		if (pcf2!=fin-1) {
			cerr << "* Datos no esperados despu�s de parentesis de "
			     << "la posici�n " << pcf << endl;
			throw exception();
		}

		arbin<string> si=string_a_fbf(fbf, ini+1,pcf);
		arbin<string> sd=string_a_fbf(fbf, pcf+3,pcf2);
		string nn; nn+=fbf[pcf+1];
		arbin<string> a(nn, si, sd);
		return a;
	}
	else if (fbf[ini]>='a' && fbf[ini]<='z') { /* Variable */
		string nv;
		for (int i=ini; i<fin; i++) {
			if (!((fbf[i]>='A' && fbf[i]<='Z') ||
				(fbf[i]>='a' && fbf[i]<='z') ||
				(fbf[i]>='0' && fbf[i]<='9') ||
				(fbf[i]=='_'))) {
				cerr << "* El nombre de la variable tiene "
					<<"caracteres no esperado en posici�n " 
					<<i<<endl;
				throw exception();
			}
			nv+=fbf[i];
		}
		arbin<string> a(nv);
		return a;
	}
	else {
		cerr << "La subcadena iniciada en caracter "
			<<ini<<" no es fbf"<<endl;
		throw exception();
	}
}

void 
saca_variables(nodo_arbin<string> *fbf, map<string, bool> &asig)
{
	assert(fbf!=NULL);

	if (fbf->es_hoja()) {
		asig[fbf->valor()]=false;
		return;
	}
	saca_variables(fbf->sub_izq(), asig);
	if (fbf->sub_der()!=NULL) { 
		saca_variables(fbf->sub_der(), asig);
	}
}	


bool
evaluar(nodo_arbin<string> *fbf, map<string, bool> asig) 
{
	assert(fbf!=NULL);
	
	if (fbf->es_hoja()) {
		return asig[fbf->valor()];
	}
	else if (fbf->sub_der()==NULL) { //!
		return !evaluar(fbf->sub_izq(), asig);	
	}
	// binario
	bool vi=evaluar(fbf->sub_izq(), asig);
	if (fbf->valor()=="&") {
		if (vi==false) {
			return false;
		}
		return evaluar(fbf->sub_der(), asig);
	}
	// Debe ser |
	if (vi==true) {
		return true;
	}
	return evaluar(fbf->sub_der(), asig);
}

