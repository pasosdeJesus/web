Date: Tue, 17 Dec 2002 17:15:03 +0100
From: Vladimir T�mara <vtamara@informatik.uni-kl.de>
To: auribe@presidencia.gov.co, contacto@fiscalia.gov.co,
        fiscal@colomsat.net.co, emaya@procuraduria.gov.co
Cc: emcol@t-online.de, secretaria_privada@hotmail.com
Subject: Favor garantizar vida de mujeres pacifistas

Se�or presidente Alvaro Uribe Velez
Se�or fiscal general Luis Camilo Osorio
Se�or procurador general Edgargo Maya Villaz�n

Con copia a:
Se�or Eduardo Cifuentes Mu�oz, Defensor del Pueblo
Herrn Hernan Beltz-Peralta, Kanzlei der Botschaft der Republik Kolumbien


De acuerdo a la informaci�n disponible en:
http://www2.amnesty.de/internet/deall.nsf/AlleDok/F37071C13A582D0DC1256C91005591B1?Open
  y en
http://colombia.indymedia.org/news/2002/11/423.php

las se�oras Mar�a del Socorro Mosquera, Mery del Socrro Naranjo y Teresa
Yarce, estuvieron detenidas 9 dias sin orden de captura (del 12 al 21
de Noviembre). Habr�an sido arrestadas despu�s de la operaci�n
Ori�n en la comuna 13 de Medellin, para despu�s quedar en libertad
al no encontrarse pruebas.  Habr�an sido arrestadas por se�alamientos
de informantes, siendo menor de edad el informante que las se�al�.

De acuerdo a esa informaci�n, estas se�oras estar�an trabajando en
agrupaciones pacifistas, y habr�an denunciado violaciones a los derechos
humanos por parte de servidores p�blicos durante la operaci�n Orion.
Dado que el 24 de Noviembre algunos soldados habr�an amenazado a Mar�a
del Socorro Mosquera (diciendole a su hijo de 14 a�os que ella debia
abandonar la regi�n), escribo para solicitarle humildemente:

* Me confirme la informaci�n que he recibido o su rectificaci�n.
* Garantias para la vida e  integridad de  Mar�a del Socorro Mosquera, Mery del
  Socrro Naranjo y Teresa Yarce  y en general de todas las personas de
  la comuna 13, que han sufrido o est�n sufriendo detenciones arbitrarias
  (en varios casos para ser liberadas)
* Se investigue el arresto sin orden de captura de estas se�oras.
* Se tomen medidas para asegurar que los miembros de organizaciones
  legales de la comuna 13 que defiende derechos humanos pueda realizar
  su labor.
* Reparaci�n para las victimas y muertos civiles no involucradas en el
  conflicto que esa operaci�n dej� ---9 de acuerdo a
http://www.defensoria.org.co/base_level2.php?Pageinfo=base_noti&section=news&noti=A
* No se involucre en el conflicto a la poblaci�n civil como informantes
  y mucho menos a menores de edad. Por favor dejemoslos crecer y que
  puedan decidir despu�s de haber vivido su infancia.

Tambi�n solicito humildemente:

* Que no sigan realizandose arrestos sin orden judicial.
* Que no se den atribuciones judiciales a miembros de las fuerzas armadas.
* Que se respete a la poblaci�n civil, no involucrandola en el
  conflicto armado con operaciones en las que corra riesgo como 
  con la operaci�n Orion, ni con soldados campesinos, ni con
  informantes y muchos menos que se involucre a menores de edad.
  (por cierto como puede considerarse en juicios la declaraci�n de informantes
   a la quienes se les paga?).

Un colombiano que quiere la paz de Dios:
        Vladimir T�mara Pati�o


