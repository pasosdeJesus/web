Date: Sun, 30 Jun 2002 19:34:41 +0200
From: Vladimir =?iso-8859-1?Q?T=E1mara?= <vtamara@informatik.uni-kl.de>
To: apastra@presidencia.gov.co, siden@mindefensa.gov.co,
        infprotocol@mindefensa.gov.co, minisint@col1.telecom.com.co,
        reygon@procuraduria.gov.co, contacto@fiscalia.gov.co,
        denuncie@fiscalia.gov.co, secretaria_privada@hotmail.com,
        pilaque@cgm.mil.co
Cc: omct@omct.org
Subject: Por favor =?iso-8859-1?Q?protecci=F3?=
        =?iso-8859-1?Q?n?= a indigenas

Se�or presidente Andr�s Pastrana
Se�or vicepresidente y ministro de defensa Gustavo Bell Lemus
Se�or ministro de interior Armando Estrada Villa
Se�or procurado general de la naci�n  Edgardo Jos� Maya Villaz�n
Se�or fiscal general de la naci�n Luis Camilo Osorio
Se�or defensor del pueblo Eduardo Cifuentes
Se�or comandante de las fuerzas militares Fernando Tapias Stahelin
Con copia a OMCT

Escribo para pedirles humildemente protecci�n especial para habitantes
de resguardos indigenas, en particular para los lideres indigenas del
CRIDEC en Caldas y m�s en particular: Gabriel �ngel Cartagena, Gerza�n
Jes�s D�az, Jos� Ramiro Bueno, Diego Armando Tabarquino, Fabio Enrique
Morales, Benjam�n Taba Molina,  Dar�o Edgardo Tapasco, Arbey Ga��n,
Gabriel �ngel Cartagena,  Arahugo Gan�n, Olga Luc�a Morales,
Oscar An�bal Largo.

Quienes de acuerdo a la informaci�n adjunta han sido amenzados
por grupos paramilitares, habr�an sido se�alados por la fuerza p�blica
como auxiliadores de la guerrilla y podr�an haber sido objeto de
montajes judiciales en su contra.  A�n cuando la OEA y la CIDH habr�an
decretado medidas cautelares que los amparan.

Por favor hacer paz con paz.  Mi argumento �ltimo es Dios, quien en
Jes�s mostr� que esa es la �nica forma.  A �l le  pido que nos ilumine
para hacer lo correcto, decir la verdad completa siempre, que nos de fuerza
para aprender con humildad, amor, respeto y conciencia.

Saludo de un colombiano que quiere la paz de Dios
        Vladimir T�mara Pati�o.


----- Forwarded message from OMCT <omct@omct.org> -----

Envelope-to: vtamara@localhost
Delivery-date: Sun, 30 Jun 2002 15:29:41 +0200
Mailing-List: contact colombia-help@derechos.net; run by ezmlm
Precedence: bulk
Delivered-To: mailing list colombia@derechos.net
Delivered-To: moderator for colombia@derechos.net
Date:   Sat, 29 Jun 2002 00:16:21 -0700
From: OMCT <omct@omct.org>
X-Sender: (Unverified)
To: colombia@derechos.net
X-Mailer: QUALCOMM Windows Eudora Version 4.3.2
Subject: [colombia] Colombia:  Caso COL 280602  (amenazas de muerte contra indigenas).
X-UIDL: ce9395f561e61959e57329cff462c7b9


Caso COL 280602
Amenazas de muerte / Hostigamiento

El Secretariado Internacional de la OMCT solicita su intervenci�n
urgente en la siguiente situaci�n en Colombia.

Descripci�n de la situaci�n :

El Secretariado Internacional de la OMCT ha sido informado acerca de
las amenazas de muerte dirigidas contra los Gobernadores Ind�genas
del Departamento de Caldas, as� como contra varias personas
relacionadas con el Consejo Regional Ind�gena de Caldas, CRIDEC.

De acuerdo con la Corporaci�n para la Defensa y Promoci�n de los
Derechos Humanos REINICIAR, organizaci�n miembro de la red de la
OMCT, el 26 de junio del 2002, aproximadamente a las 4 de la tarde,
miembros de grupos paramilitares hicieron una llamada telef�nica a la
sede de la Asociaci�n de Cabildos Ind�genas de Riosucio, Caldas,
ASICAL, para reiterar las amenazas de muerte contra los Gobernadores
Ind�genas de la regi�n, los cuales en ese momento se encontraban
reunidos en el lugar.  Seg�n los informes, los paramilitares dijeron
:   "Esperamos la renuncia colectiva a las 6 de la tarde de todos los
gobernadores ind�genas y sus suplentes, incluidos el presidente del
Consejo Regional Ind�gena, CRIDEC,  Arahugo Ga��n, Olga Luc�a Morales
 y  Oscar An�bal Largo  y que si no lo hacen, ya est�n ubicados".

Los informes agregan que desde el d�a 24 de junio del 2002, los
paramilitares que operan en el Eje Cafetero, vienen incrementando las
amenazas en contra de todos los l�deres ind�genas asociados al
CRIDEC.  Es importante agregar que el Consejo Regional Ind�gena de
Caldas, CRIDEC, fue fundado en 1984 y tiene programas de asistencia
t�cnica de agricultura, de exportaci�n de caf�, de educaci�n, y de
cultura ind�gena.

Tambi�n se ha informado que la cr�tica situaci�n de riesgo y amenazas
de muerte para estas comunidades ha venido siendo denunciada
constantemente, por lo cual la Comisi�n Interamericana de Derechos
Humanos, CIDH, de la Organizaci�n de los Estados Americanos, OEA,
decret� Medidas Cautelares a favor de estas comunidades desde el 15
de marzo del presente a�o.  Las informaciones agregan que a pesar de
las Medidas Cautelares, estas comunidades vienen siendo perseguidas y
hostigadas por los paramilitares y se�aladas por miembros de la
Fuerza P�blica y por algunas autoridades civiles, como auxiliadoras y
colaboradoras de la guerrilla.

De acuerdo con los informes, la situaci�n es tan cr�tica, que en los
�ltimos d�as se vienen realizando montajes judiciales en contra de
estos l�deres, como el proceso en previas No. 3545, el cual cursa en
la Fiscal�a Seccional Primera de Riosucio, en donde los acusan de
hacer parte de todos los hechos perpetrados por la guerrilla en los
�ltimos tiempos en la regi�n de Riosucio y Sup�a, en Caldas.

Adem�s, seg�n las denuncias, el 26 de junio de 2002, m�s de 300
paramilitares de las AUC, quienes portaban uniformes de uso privativo
de las Fuerzas Militares, fueron vistos cuando avanzaban con rumbo
desconocido cerca de la comunidad de La Caucana, perteneciente al
Resguardo Nuestra Se�ora Candelaria de la Monta�a.

Antecedentes de la situaci�n :

�       El lunes 24 de junio del 2002, a la 1h30 de la tarde, el Sr.
Gabriel �ngel Cartagena, Gobernador del Resguardo Ind�gena de
Ca�amomo-Lomaprieta, recibi� una llamada an�nima al tel�fono fijo de
la sede de ese resguardo, la cual fue contestada por la secretaria de
dicho lugar.  El hombre quien hizo la llamada dijo textualmente:  "
Ya estamos ubicados, sabemos donde est�n, donde trabajan y que
hacen".  Inmediatamente despu�s colgaron el tel�fono.

�       El martes 25 de junio del 2002 hacia las 8h30 de la ma�ana, el Sr.
Gabriel �ngel Cartagena recibi� una nueva llamada al tel�fono fijo de
la sede del resguardo de Ca�amomo-Lomaprieta, durante la cual le
manifestaron entre otros lo siguiente:  "Que depende de �l la vida de
los compa�eros de los otros resguardos y que les dan hasta el 26 de
junio del 2002 para que renuncien a las organizaciones ind�genas como
l�deres".

Adem�s los paramilitares anunciaron con nombre propio a los l�deres
que ser�an asesinados de no cumplir sus perentorias �rdenes ;
nombraron a los Sres.  Gabriel �ngel Cartagena y su gobernador
suplente Gerza�n Jes�s D�az;  Jos� Ramiro Bueno, del resguardo de
Escopetera Pirza y su gobernador suplente  Diego Armando Tabarquino;
Fabio Enrique Morales del Resguardo Nuestra Se�ora Candelaria de la
Monta�a y su gobernador suplente  Benjam�n Taba Molina;  Dar�o
Edgardo Tapasco, del Resguardo de San Lorenzo y su gobernador
suplente  Arbey Ga��n.

Tambi�n en esa misma ocasi�n le dijeron al Sr.  Gabriel �ngel
Cartagena que se le deb�a informar al Sr.   Arahugo Gan�n, en su
calidad de Coordinador General del CRIDEC, a la Sra.  Olga Luc�a
Morales, ex gobernadora ind�gena de Escopetera-Pirza y al Sr.  Oscar
An�bal Largo, para que abandonaran sus cargos.  Los paramilitares
terminaron la llamada telef�nica diciendo "si siguen ah� son de
caprichosos, las muertes pueden ser selectivas o grupales".

La OMCT nuevamente condena estos nuevos hechos de extrema violencia,
la persecuci�n contra la poblaci�n civil, la aparente impunidad de
los autores y la omisi�n de medidas efectivas de protecci�n por parte

de las autoridades municipales, gubernamentales y nacionales para
prevenir estos hechos y, comparte la preocupaci�n de REINICIAR por la
seguridad y la integridad f�sica y psicol�gica de las personas
amenazadas y en general de los habitantes de las mencionadas
comunidades ind�genas.

Acci�n solicitada: 
Favor escribir a las autoridades Colombianas urgi�ndolas a :

i.      tomar inmediatamente las medidas adecuadas para garantizar la
seguridad y la integridad f�sica y psicol�gica de las personas
amenazadas de muerte arriba mencionadas, as� como la de todos los
habitantes de los mencionados resguardos ind�genas; 

ii.     dar cumplimiento inmediato a las recomendaciones dadas por los
organismos internacionales y regionales de derechos humanos,
incluyendo la Oficina de la Alta Comisionada para los Derechos
Humanos, la Comisi�n de Derechos Humanos de la ONU y la Comisi�n
Interamericana de Derechos Humanos ;

iii.    garantizar el respeto por los derechos humanos y las libertades
fundamentales en todo el pa�s de conformidad con las leyes nacionales
y las normas internacionales de derechos humanos.

Direcciones:
�       S.E.  Andr�s Pastrana Arango, Presidente de la Rep�blica, Cra. 8
n�.7-26, Palacio de Nari�o, Santaf� de Bogot�. Fax: (+57 1) 566 20 71
 e-mail : apastra@presidencia.gov.co
�       Consejer�a Presidencial de Derechos Humanos,  Calle 7 n�. 6-54 Piso
3, Santaf� de Bogot�, D. C.  Fax: (+57 1) 337 13 51
�       Doctor Gustavo Bell Lemus, Ministro de la Defensa, Avenida El
Dorado con Cra. 52 CAN, Santaf� de Bogot�.  Fax:  (+57 1) 222 18 74 ;
 e-mail :  siden@mindefensa.gov.co ; infprotocol@mindefensa.gov.co ;
mdn@cable.net.co
�       Doctor  Armando Estrada Villa, Ministro del Interior,  Cra. 8 n�. 8-
09 Santa Fe de Bogot�, Santaf� de Bogot�. Fax:  (+57 1) 286 80 25 ;
e-mail:  mininterior@myrealbox.com ; minisint@col1.telecom.com.co
�       Doctor  Edgardo Jos� Maya Villaz�n, Procurador General de la
Naci�n, Carrera 5 n�. 15-80,Santa Fe de Bogot�.Fax: (+57 1) 342 97
23;(+57 1) 281 75 31 ;  e-mail : reygon@procuraduria.gov.co ;
anticorrupcion@presidencia.gov.co
�       Doctor  Luis Camilo Osorio,  Fiscal General de la Naci�n, Diagonal
22 B n�. 52-01, Santaf� de Bogot�.  Fax:  (+57 1) 570 20 00 ;
contacto@fiscalia.gov.co ;  denuncie@fiscalia.gov.co
�       Doctor  Eduardo Cifuentes, Defensor del Pueblo, Calle 55 n�. 10-32
Santa Fe de Bogot�.  Fax:  (+ 57 1) 640 04 91  e-mail :
secretaria_privada@hotmail.com
�       General  Fernando Tapias Stahelin, Comandante de las Fuerzas
Militares, Avenida el Dorado con Cra. 52, Santaf� de Bogot�. Fax:
(+57 1) 222 29 35 ;  e-mail :  siden@mindefensa.gov.co ;
pilaque@cgm.mil.co
- Representaciones Diplom�ticas de Colombia en sus respectivos pa�ses

Ginebra, 28 de junio del 2002
Favor informarnos sobre cualquier acci�n realizada acerca del  
presente caso, citando el c�digo del presente llamado en su
respuesta. 
Organizaci�n Mundial Contra la Tortura (OMCT)
Apartado postal 21
8 rue du Vieux-Billard
CH-1211 Ginebra 8
CH-1211 Ginebra 8
Suisse / Suiza
Tel. : 0041 22 809 49 39
Fax :  0041 22 809 49 29
E-mail : omct@omct.org
URL : http://www.omct.org

La OMCT coordina la red SOS-Tortura, compuesta por m�s de 240
organizaciones en el mundo, comprometidas en la lucha contra la
tortura y


otras penas o tratos crueles, inhumanos o degradantes, las
desapariciones forzadas, las ejecuciones extrajudiciales
o sumarias, y otras formas de represi�n.

La OMCT tiene estatus consultivo ante la ONU, la OIT y la Comisi�n
Africana de Derechos Humanos y de los Pueblos.




