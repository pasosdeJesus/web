Date: Wed, 21 Aug 2002 14:17:05 +0200
From: Vladimir =?iso-8859-1?Q?T=E1mara?= <vtamara@informatik.uni-kl.de>
To: auribe@presidencia.gov.co
Subject: No a la =?iso-8859-1?Q?petici=F3?= =?iso-8859-1?Q?n?= de Estados Unidos con respecto a la CPI

Se�or presidente Alvaro Uribe Velez

Escribo como Colombiano para solicitarle humildemente, rechaze
la propuesta de Estados Unidos con respecto a la inmunidad de sus
militares frente a la Corte Penal Internacional.

Si un militar de Estados Unidos comete una violaci�n a los derechos
humanos en Colombia (por ejemplo defendiendo intereses econ�micos de
Estados Unidos), no es aceptable que sea s�lo el sistema judicial de
Estados Unidos el que tenga la posibilidad de juzgarlo.  �No debe ser
una corte internacional la que juzgue casos internacionales?

La actitud de Estados Unidos al rechazar la CPI y solicitar que otros
paises los apoyen, me parece cinica.  �Por qu� m�s bien, no buscan mejorar
las deficiencias que le imputan a esa corte? �Qu� mecanismo de control
proponene ---que tal transparencia total y veeduria de todos? �Por qu�
hablan de persecuciones pol�ticas, en vez de buscar mecanismos para
asegurar que los juicios sean bien llevados, saquen siempre la verdad
completa y lleven a penas apropiadas?

El apoyo de Colombia a esta propuesta, ser�a en mi humilde opini�n,
un grave error que nos pesar�a hoy y en el futuro.  �Es acaso m�s importante
recibir armas y los dolares de "apoyo" que buscar y hacer justicia?

�No es mejor buscar justicia y que la verdad salga completa y clara mientras
estamos en esta tierra? �Si la muerte llega y no hemos lavado nuestras
culpas, no ser� MUY triste despu�s?  Ayudemos a que cada cual lave sus
culpas aqu� con un sistema de justicia internacional lo m�s justo
posible, que nos protega y haga responsables a TODOS.

Queda a su conciencia mi humilde opini�n.  Solicito el favor de
enviarme un acuse de recibido.


  Vladimir T�mara Pati�o un colombiano que quiere la paz de Dios.




