Date: Wed, 26 Jun 2002 14:52:13 +0200
From: Vladimir =?iso-8859-1?Q?T=E1mara?= <vtamara@informatik.uni-kl.de>
To: apastra@presidencia.gov.co, siden@mindefensa.gov.co,
        infprotocol@mindefensa.gov.co, minisint@col1.telecom.com.co,
        anticorrupcion@presidencia.gov.co, denuncie@fiscalia.gov.co
Cc: omct@omct.org
Subject: Por favor =?iso-8859-1?Q?atenci=F3?=
        =?iso-8859-1?Q?n?= a estos casos

Se�or presidente Andr�s Pastrana
Se�or ministro de defensa y vicepresidente Gustavo Bell Lemus
Se�or procurado general lde la naci�n Edgardo Jos� Maya
Se�or fiscal general de la naci�n Luis Camilo Osorio
Se�or comandante de las fuerzas militares Fernando Tapias
Con copia a OCMT


Recib� la informaci�n adjunta de acuerdo a la cual est�n desaparecidos
desde Septiembre de 1998 el se�or M�ximo Vargas C�rdenas y el se�or Rodrigo
(amigo de M�ximo Vargas), desde julio de 2000 el se�or Jose Weimar Toro y 
Raul, y desde diciembre de 2001 el se�or Angel Mar�a G�mez Rodr�guez.

Solicito humildemente se adelanten investigaciones para dar con el paradero
de estas personas y llevar a juicio a los responsables. En particular
en los dos primeros hay indicios de una posible participaci�n de la policia
nacional, en especial para ese caso, solicito una investigaci�n urgente
(ya llevan m�s de 3 a�os desaparecidos y no hay responsables).

Hago estas peticiones porque respeto la vida que Dios nos regalo,
le pido a �l que nos de fuerza para luchar contra la impunidad, asumir
responsabilidad, decir la verdad y reparar.

Un colombiano que quiere la paz de Dios
        Vladimir T�mara Pati�o





----- Forwarded message from OMCT <omct@omct.org> -----

Envelope-to: vtamara@localhost
Delivery-date: Wed, 26 Jun 2002 14:08:27 +0200
Mailing-List: contact colombia-help@derechos.net; run by ezmlm 
Precedence: bulk
Delivered-To: mailing list colombia@derechos.net
Delivered-To: moderator for colombia@derechos.net
Date:   Tue, 25 Jun 2002 10:30:22 -0700
From: OMCT <omct@omct.org>
X-Sender: (Unverified)
To: colombia@derechos.net

X-Mailer: QUALCOMM Windows Eudora Version 4.3.2
Subject: [colombia] Colombia:  Caso COL 190602 (varias desapariciones forzadas).
X-UIDL: 67a6d0dc872819db66d97e8de7309718

Caso COL 190602
Varias desapariciones forzadas

El Secretariado Internacional de la OMCT solicita su intervenci�n
urgente en la siguiente situaci�n en Colombia.

Breve descripci�n de la situaci�n:

El Secretariado Internacional de la OMCT ha sido informado de varias
desapariciones forzadas en hechos ocurridos en La Dorada (Caldas) ;
Marinilla  (Departamento de Antioquia), y  Popay�n (Cauca).

De acuerdo con las informaciones de la Asociaci�n de Familiares de
Detenidos Desaparecidos de Colombia (ASFADDES), en los �ltimos d�as
se han denunciado ante esa organizaci�n los siguientes casos de
desapariciones forzadas en Colombia :

-       El Sr.  M�ximo Vargas C�rdenas, de 34 a�os, natural de Facatativ�,
quien trabajaba como agricultor, sali� de su lugar de residencia
ubicada en  Sop� (Cundinamarca), el 27 de septiembre de 1998, junto
con dos amigos, los Sres.  Javier Herrera Aguirre y Rodrigo (de quien
no se conocen mayores datos), rumbo a La Dorada (Caldas), atendiendo
a una invitaci�n del Sr.  Herrera.
De acuerdo con las informaciones recibidas, el 05 octubre de 1998 los
tres amigos fueron a pescar en el R�o Guarinocito, y aproximadamente
a las 5h15 de la tarde cuando salieron a coger un colectivo para
devolverse a la ciudad de La Dorada apareci� la Polic�a perteneciente
a La Victoria, en dos motos y una patrulla, disparando con fusiles.
La polic�a retuvo a los Sres. M�ximo Vargas C�rdenas y Rodrigo, all�
los interrogaron pregunt�ndoles quienes eran, qu� hac�an en la zona,
los hicieron tender boca abajo en la carretera hasta la 7h00 de la
noche, cuando lleg� otro carro al que los hicieron subir, este carro
tomo la v�a que conduce de Guarinocito a La Dorada y la patrulla de
la Polic�a tomo la v�a que conduce a La Victoria.

Desde ese d�a no se sabe nada del paradero ni de la integridad f�sica
y/o psicol�gica del Sr. M�ximo Vargas C�rdenas ni del Sr.  Rodrigo.
Mientras tanto, el Sr.  Javier Herrera se asust� y corri� a
esconderse en un matorral que estaba a unos 20 metros de distancia.
Seg�n los informes, 8 d�as despu�s, la inspecci�n de polic�a de
Salamina (Caldas) inform� a la familia, que se hab�a encontrado dos
cuerpos y por la primera descripci�n se cre�a que pertenec�an a los
dos desaparecidos, pero por indagaciones posteriores se confirm�  que
no era as�.  A partir del d�a 31 de octubre miembros de la familia
Vargas C�rdenas se pusieron en la tarea de su b�squeda, y fueron a
diferentes lugares de la Victoria, y de La Dorada indagando por el
Sr. M�ximo ;  en uno de los bares de La Dorada, a uno de los hermanos
del Sr. M�ximo, se le acerc� un hombre quien le afirm� que al Sr.
M�ximo junto con el Sr.  Rodrigo se los hab�a llevado la polic�a y
que �l sabia a quien se los entregaban.

-       Los Sres.  Jos� Weimar Toro, de 30 a�os de edad, natural de Palmar
(Nari�o) y Ra�l (de quien no se conocen mayores datos) se encuentran
desaparecidos desde el  21 de julio del 2000, cuando se dirigieron a
la Vereda de San Carlos,  Municipio de Marinilla, (Antioquia), en una
moto de propiedad del Sr. Jos� Weimar Toro, en busca de un veh�culo
que hab�a sido hurtado (robado), y que era de propiedad del Sr.
Oliver Toro Portilla, t�o del Sr.  Jos� Weimar Toro.   Desde ese d�a

no se volvi� a tener conocimiento sobre el paradero de ninguno de
ellos.  Los informes agregan que el mencionado veh�culo que hab�a
sido robado, fue encontrado dos meses despu�s en una autopista cerca
de Bogot�.  La familia  coloc� la denuncia respectiva sobre la
desaparici�n en la Personer�a y en la Fiscal�a de Marinilla.

-       El Sr.  Angel Mar�a G�mez Rodr�guez, de 35 a�os de edad, natural de
Zetaquira (Boyac�) se encuentra desaparecido desde el 3 de diciembre
del 2001, a las 10h10 de la ma�ana, cuando sali� de su lugar de
habitaci�n hacia el Barrio la Esmeralda en Popay�n (Cauca), con el
fin de comprar material para su panader�a, la cual funcionaba en su
misma casa.  Desde ese d�a no se tiene conocimiento sobre su
paradero.   Se coloc� la correspondiente denuncia en la SIJIN 
(Polic�a T�cnico-Judicial de Investigaciones) del Cauca, y en la
Defensor�a del Pueblo, seccional Popay�n.  
Antecedentes:
Las informaciones agregan que se presume que su desaparici�n est�
relacionada con el hecho de que su compa�era la Sra.  Hermencia Majin
pertenece a la Junta de Acci�n Comunal del Barrio Avelino Ull.  La
mencionada Junta de Acci�n Comunal se propuso en 1999 terminar con
dos expendios de venta de droga, logrando conformar guardias c�vicas
y la erradicaci�n de estas ventas con la colaboraci�n de la Polic�a
durante el a�o 2000.  Todo esto ocasion� el disgusto de los
vendedores y de los consumidores.

El Secretariado Internacional de la OMCT recuerda que la desaparici�n
forzada es considerada como un crimen de lesa humanidad, y manifiesta
su condena de estos hechos de violencia, la persecuci�n contra la 
poblaci�n civil, la aparente impunidad de los autores y la omisi�n de
medidas efectivas de protecci�n por parte de las autoridades
municipales, gubernamentales y nacionales para prevenir estos hechos.



Acci�n solicitada
Favor escribir a las autoridades Colombianas urgi�ndolas a:

i.      tomar de inmediato las medidas necesarias para localizar el
paradero de las personas desaparecidas arriba mencionadas y
garantizar su seguridad y su integridad f�sica y psicol�gica ;

ii.     llevar a cabo una investigaci�n independiente e imparcial en
torno a los hechos arriba mencionados con el fin de identificar a los
responsables, llevarlos a juicio y aplicarles las sanciones penales,
civiles y/o administrativas previstas por la ley; 

iii.    dar cumplimiento inmediato a las recomendaciones dadas por los
organismos internacionales y regionales de derechos humanos,
incluyendo la Oficina de la Alta Comisionada para los Derechos
Humanos y la Comisi�n Interamericana de Derechos Humanos; 

iv.     garantizar el respeto por los derechos humanos y las libertades
fundamentales en todo el pa�s de conformidad con las leyes nacionales
y las normas internacionales de derechos humanos.

Direcciones :
�       S.E.  Andr�s Pastrana Arango, Presidente de la Rep�blica, Cra. 8
n�.7-26, Palacio de Nari�o, Santaf� de Bogot�. Fax: (+57 1) 566 20 71
 e-mail : apastra@presidencia.gov.co
�       Consejer�a Presidencial de Derechos Humanos,  Calle 7 n�. 6-54 Piso
3, Santaf� de Bogot�, D. C.  Fax: (+57 1) 337 13 51
�       Doctor Gustavo Bell Lemus, Ministro de la Defensa, Avenida El
Dorado con Cra. 52 CAN, Santaf� de Bogot�.  Fax:  (+57 1) 222 18 74 ;

�       Doctor Gustavo Bell Lemus, Ministro de la Defensa, Avenida El
Dorado con Cra. 52 CAN, Santaf� de Bogot�.  Fax:  (+57 1) 222 18 74 ;
 e-mail :  siden@mindefensa.gov.co ; infprotocol@mindefensa.gov.co ;
mdn@cable.net.co
�       Doctor  Armando Estrada Villa, Ministro del Interior,  Cra. 8 n�. 8-
09 Santa Fe de Bogot�, Santaf� de Bogot�. Fax:  (+57 1) 286 80 25 ;
e-mail:  mininterior@myrealbox.com ; minisint@col1.telecom.com.co
�       Doctor  Edgardo Jos� Maya Villaz�n, Procurador General de la
Naci�n, Carrera 5 n�. 15-80,Santa Fe de Bogot�.Fax: (+57 1) 342 97
23;(+57 1) 281 75 31 ;  e-mail : reygon@procuraduria.gov.co ;
anticorrupcion@presidencia.gov.co
�       Doctor  Luis Camilo Osorio,  Fiscal General de la Naci�n, Diagonal
22 B n�. 52-01, Santaf� de Bogot�.  Fax:  (+57 1) 570 20 00 ;
contacto@fiscalia.gov.co ;  denuncie@fiscalia.gov.co
�       Doctor  Eduardo Cifuentes, Defensor del Pueblo, Calle 55 n�. 10-32
Santa Fe de Bogot�.  Fax:  (+ 57 1) 640 04 91  e-mail :
secretaria_privada@hotmail.com
�       General  Fernando Tapias Stahelin, Comandante de las Fuerzas
Militares, Avenida el Dorado con Cra. 52, Santaf� de Bogot�. Fax:
(+57 1) 222 29 35 ;  e-mail :  siden@mindefensa.gov.co ;
pilaque@cgm.mil.co
- Representaciones Diplom�ticas de Colombia en sus respectivos pa�ses

Ginebra, 19 de junio del 2002

Favor informarnos sobre cualquier acci�n realizada acerca del
presente caso, citando el c�digo del presente llamado en su
respuesta.
Organizaci�n Mundial Contra la Tortura (OMCT)
Apartado postal 21
8 rue du Vieux-Billard
CH-1211 Ginebra 8
Suisse / Suiza
Tel. : 0041 22 809 49 39
Fax :  0041 22 809 49 29
E-mail : omct@omct.org
URL : http://www.omct.org

La OMCT coordina la red SOS-Tortura, compuesta por m�s de 240
organizaciones en el mundo, comprometidas en la lucha contra la
tortura y


otras penas o tratos crueles, inhumanos o degradantes, las
desapariciones forzadas, las ejecuciones extrajudiciales
o sumarias, y otras formas de represi�n.

La OMCT tiene estatus consultivo ante la ONU, la OIT y la Comisi�n
Africana de Derechos Humanos y de los Pueblos.


