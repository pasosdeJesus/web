Date: Fri, 30 May 2003 10:28:05 +0200
From: Vladimir =?iso-8859-1?Q?T=E1mara?= <vtamara@informatik.uni-kl.de>
To: auribe@presidencia.gov.co, rdh@presidencia.gov.co
Subject: Por favor justicia en el caso de Carlos Arturo Marulanda

Se�or presidente de Colombia Alvaro Uribe Velez

Recib� la informaci�n adjunta de acuerdo a la cual se requiere ayuda
nacional e internacional para lograr la captura y juzgamiento de
Carlos Arturo Marulanda por los delitos de Terrorismo y concierto
para delinquir.

Solicito humildemente se me informe los mecanismos que est�n empleando
para lograr la captura de este se�or, as� como un juicio justo, evitando
que se repita una liberaci�n con fundamentos errados como ya ocurri�.

De ser posible solicito tambi�n humildemente se me informe la sanci�n
de la fiscal que cambio los cargos en este caso.  Tambi�n solicito se
de a conocer la situaci�n de los predios de la hacienda Bellacruz, pues
seg�n entiendo varios NO ser�an propiedad de Carlos Arturo Marulanda y
su titulaci�n a los campesinos ha sido frustada con violencia por parte
de paramilitares de esa hacienda.
C�mo dice en la sentencia T-227/97 de la sal civil del tribunal superior
de Santaf� de Bogot�:

"
1.1. Actuaci�n ante el INCORA respecto al predio Bellacruz

Declara la subgerente jur�dica del INCORA:

EL INCORA en el a�o 90 inici� un procedimiento de clarificaci�n de la
propiedad
 sobre los predios de la hacienda Bellacruz , el cual culmin� en el a�o
94 , por
 Resoluci�n que establec�a que unos predios eran de propiedad privada
porque ten
�an t�tulo suficiente que asilo acreditaba en tanto que otros predios
que hacen 
parte de la mencionada finca , creo que unos 6, se declar� que no hab�an
salido 
del patrimonio del Estado , contra esa Resoluci�n no interpusieron los
recursos,
 pero en el mes de octubre del a�o 95, el apoderado de la familia
Marulanda form
ul� una revocatoria directa, la cual fue resuelta desfavorablemente en
el a�o 96
, en abril, confirmando la resoluci�n que hab�a decidido el
procedimiento de cla
rificaci�n. Una vez en firme esta resoluci�n el INCORA deb�a proceder a
adelanta
r los tr�mites de titulaci�n como bald�os de estos predios a los
campesinos que 
los ven�an ocupando y explotando adecuadamente desde hac�a m�s de 5 a�os

1.2. Frustrada Titulaci�n

"Nos comprometimos a iniciar prontamente el trabajo de titulaci�n ,
firmamos un 
convenio con la gobernaci�n del Cesar a fin de que ellos contrataran los
top�gra
fos que se requer�an para hacer los levantamientos topogr�ficos de los
predios b
ald�os. Inmediatamente la gobernaci�n suscribi� el contrato y entreg� el
anticip
o , los top�grafos se desplazaron a cumplir con su trabajo , pero
tuvieron que r
egresarse porque recibieron amenazas de atentar contra su integridad
f�sica si c
ontinuaban realizando el trabajo y a un funcionario del INCORA que los
acompa�ab
a, le despellejaron la espalda .."

Cuando consiguieron nuevos top�grafos , "Ellos viajaron, pero en raz�n a
que la 
base militar quedaba distante unos 45 minutos en carro de la Hacienda se
dedicar
on a la tarea de arrendar un veh�culo que hiciera los desplazamientos
diariament
e lo cual fue imposible porque la poblaci�n no le suministr� este
servicio y la 
Alcald�a municipal s�lo contaba con un cami�n que se encontraba a
�rdenes de la 
Fiscal�a porque en �l hab�an sido asesinados el Secretario de Gobierno,
el Tesor
ero y el conductor del municipio de Pelaya."(declaraci�n de la
subgerente jur�di
ca del Incora).

En conclusi�n, la tramitaci�n del INCORA se suspendi� por la violencia
contra lo
s funcionarios que tienen que adelantar los procesos de adjudicaci�n de
tierras.
"


Un colombiano que quiere la paz de Dios
	Vladimir T�mara Pati�o.

----- Forwarded message from Editor Equipo Nizkor <nizkor@derechos.org> -----

Date:  Fri, 30 May 2003 00:13:04 +0200
From: Editor Equipo Nizkor <nizkor@derechos.org>
To: Equipo Nizkor <nizkor@derechos.org>
Subject: Col - Continua la escalada paramilitar en Viota y piden apoyo 
 internacional para que sea detenido el ex embajador Carlos Arturo 
 Marulanda.

...

ii) PIDEN APOYO INTERNACIONAL PARA QUE EL GOBIERNO URIBE DETENGA Y PONGA
A DISPOSICION DE LA JUSTICIA AL EX EMBAJADOR MARULANDA.
http://www.derechos.org/nizkor/colombia/doc/marulanda5.html

La Corporaci�n Colectivo de Abogados "Jos� Alvear Restrepo",
Organizaci�n No Gubernamental Colombiana, les remite a la comunidad
nacional e internacional el presente comunicado realizado por las
familias campesinas desplazadas por la violencia de la Hacienda BELLA
CRUZ.

COMUNICADO A LA OPINI�N PUBLICA NACIONAL E INTERNACIONAL

Las familias campesinas desplazadas por la violencia el 14 de febrero de
1996 de la Hacienda BELLA CRUZ en el sur del departamento del Cesar y
que en el mes de Noviembre de 2002 rechazamos la decisi�n adoptada por
la fiscal�a delegada ante el tribunal suprior de Bogot�: de cambiar la
tipificaci�n de los delitos de concierto para delinquir y terrorismo,
principales delitos por los que se procesa y estaba privado de libertad
el ex ministro CARLOS ARTURO MARULANDA, al considerar como amenazas, el
delito de terrorismo; declarar la prescripci�n de la acci�n para los
delitos de amenazas, da�o en bien ajeno y perturbaci�n de la posesi�n de
inmueble y precluir la investigaci�n pro los mismos; delimitar el
tr�mite del proceso �nicamente al delito de incendio; y la de revocar la
detenci�n preventiva por el cargo de concierto para delinquir y en
consecuencia ordenar la libertad de CARLOS ARTURO MARULANDA, hoy, con
satisfacci�n y sin triunfalismo, comunicamos a la opini�n p�blica
nacional e internacional que, como resultado de la acci�n de tutela
promovida por nosotros mismos, la Corte Suprema de Justicia, mediante
sentencia de fecha Marzo 11 de 2003 decidi� tutelarnos los derechos
constitucionales a la VERDAD, a la JUSTICIA, al ACCESO A LA JUSTICIA y
al DEBIDO PROCESO, transgredidos por la Unidad de fiscal�a Delegada ante
el tribunal Superior del Distrito Judicial de Bogot� y le ordeno a la
fiscal dejar con vigencia la detenci�n preventiva contra el procesado
CARLOS ARTURO MARULANDA por los delitos de TERRORISMO y CONCIERTO PARA
DELINQUIR y a expedir la correspondiente ORDEN DE CAPTURA.

Si bien los resultados obtenidos a trav�s de la Acci�n de Tutela son
importantes, esto es a penas el comienzo de lo que necesariamente debe
terminar en una sentencia condenatoria ejemplar contra CARLOS ARTURO
MARULANDA, como autor intelectual de los delitos de CONCIERTO para
DELINQUIR y TERRORISMO, por lo cual lucharemos sin descanso.

En el marco de este prop�sito con fundamento en los derechos
constitucionales reconocidos en el fallo de la tutela referido, le
solicitamos a la Fiscal�a que conoce del proceso penal, se dicte
resoluci�n acusatoria contra CARLOS ARTURO MARULANDA y haga uso de todos
los mecanismos judiciales nacionales e internacionales existentes para
lograr su recaptura y encarcelamiento. Si el estado fue el responsable
de dejarlo en libertad es tambi�n responsabilidad y obligaci�n lograr su
captura.

Lo hasta ahora logrado y lo que falta por obtener no lo hemos conseguido
ni lo conseguiremos solos, para ello, ha sido y ser�a necesario el curso
de la solidaridad nacional e internacional de personas y organizaciones
que luchan por la verdad, la justicia y contra la impunidad.

Sin esa solidaridad no ser{a posible obtener la verdad y la justicia,
con la cual esperamos seguir contando.
Bogot�, 27 de mayo de 2003
Firma de todas las familias de la Hacienda Bella Cruz.
...



----- End forwarded message -----

