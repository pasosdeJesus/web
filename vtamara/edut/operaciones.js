// Operaciones aritm�ticas y verificaci�n
// Dominio p�blico. 2011. vtamara@pasosdeJesus.org



function assert(b) {
    if (!b) {
        alert("Aserci�n fallo");
    }
}

// Elije operandos aleatoriamente del m�ximo de cifras dado y
// retorno una suma en HTML cuyos identificadores comienzan con
// el prefijo pref
// @param string pref Prefijo
// @param int    maxcif M�ximo de cifras
// @return array(string, array) HTML con suma y arreglo con respuesta
function agregaSuma(pref, maxcif) {
    assert(maxcif>0);
    var n1 = Math.floor(Math.random()*Math.pow(10, maxcif));
    var n2 = Math.floor(Math.random()*Math.pow(10, maxcif));
//    n1 = 6614; n2 = 3400;
    var n3 = n1 + n2;
    var an1 = [], // Digitos de primer operando
        an2 = [], // Digitos de segundo operando
        res = []; // Digitos de resultado, pares lo que lleva, impares respuesta
    an1[0] = 0;
    res[0] = 0;
    res[1] = 0;
    var nd1 = 0, nd2 = 0, nd3 = 0; // N�mero de digitios de operandos y resp.
    var i;
    for (i = n1; i > 0; i = Math.floor(i / 10)) {
        d = i % 10;
        an1[nd1] = d;
        nd1++;
    }
    if (nd1 == 0) {
        nd1 = 1;
    }
    for (i = n2; i > 0; i = Math.floor(i / 10)) {
        d = i % 10;
        an2[nd2] = d;
        nd2++;
    }
    if (nd2 == 0) {
        nd2 = 1;
    }
    for (i = n3; i > 0; i = Math.floor(i / 10)) {
        d = i % 10;
        var inum = nd3 * 2;
        res[inum + 1] = d;
        sp = 0;
        if (nd3 < nd1 && nd3 < nd2) {
            sp = an1[nd3] + an2[nd3];
        }
        if (nd3 > 0) {
            sp += res[inum];
        }
        res[inum + 2] = Math.floor(sp / 10);
        nd3++;
    }
    if (nd3 == 0) {
        nd3 = 1;
    }
    if (nd3 == nd2 || nd3 == nd1) {
        res[ nd3 * 2 + 1] = 0;
        nd3++;
    }
    reshtml = "";
    reshtml += "<table border='1'><tr>";
    for (i = 0; i < nd3; i++) {
        var inum = (nd3 - 1 - i) * 2;
        var ielem = pref + inum;
        reshtml += "<td id='" + ielem + "' "
            + " class='resnormal' "
            + " onclick='daFoco(\"" + ielem + "\")'" 
            + ">&nbsp;</td>";
    }
    reshtml += "</tr><tr>";
    for (i = 0; i < nd3; i++) {
        reshtml += "<td>";
        if (((nd3 - 1 - i) >= 0 && (nd3 - 1 - i) < nd1) ||
                (i == (nd3 - 1) && n1 == 0)) {
            reshtml += an1[nd3-1-i];
        }
        reshtml += "</td>";
    }
    reshtml += "</tr><tr>";
    reshtml += "<td>+</td>";
    for (i = 1; i < nd3; i++) {
        reshtml += "<td>";
        if (nd3 - 1 - i >= 0 && nd3 - 1 - i < nd2) {
            reshtml += an2[nd3-1-i];
        }
        reshtml += "</td>";
    }
    reshtml += "</tr><tr>";
    for (i = 0; i < nd3; i++) {
        reshtml += "<td></td>";
    }
    reshtml += "</tr><tr>";
    for (i = 0; i < nd3; i++) {
        var ielem = pref + ((nd3 - i) * 2 - 1);
        reshtml += "<td id='" + ielem + "' " 
            + " class='resnormal' "
            + " onclick='daFoco(\"" + ielem + "\")' "
            + ">&nbsp;</td>";
    }

    reshtml += "</tr></table>";
    reshtml += "<p id='" + pref + "resultado'>&nbsp;</p>";
    return [reshtml, nd3, res];
}



function presta(pref, n) {
    tdop1 = document.getElementById(pref + 'op1-' + n);
    tdop2 = document.getElementById(pref + 'op2-' + n);
    spanop1 = tdop1.childNodes[1];
    numop1 = parseInt(spanop1.firstChild.data);
    oppres = tdop1.firstChild.firstChild.firstChild;
    assert(oppres.data == 'P' || oppres.data == 'D');
    if (oppres.data == 'P') {
        spanop2 = tdop2.firstChild;
        numop2 = parseInt(spanop2.firstChild.data);
        aux = document.getElementById(pref + 'aux1-' + n);
        if (spanop1.style.textDecoration == 'line-through') {
            // Si ya prest� usar el nuevo valor
            tdop1 = document.getElementById(pref + 'aux1-' + n);
            spanop1 = tdop1.firstChild;
            numop1 = parseInt(spanop1.firstChild.data);
            aux = document.getElementById(pref + 'aux2-' + n);
        }
        //Verifica que necesita prestar por ser menor que el segundo
        //operando o por haber resultado menor tras prestar.
        if (numop1 >= numop2) {
            return;
        }
        // Verificar que el izquierdo puede prestar porque es >0 
        // o porque pidi� prestado
        op1izq = document.getElementById(pref + 'op1-' + (n + 1));
        assert(op1izq != null);
        spanizq = op1izq.childNodes[1];
        if (spanizq.firstChild.data == 0 && 
                spanizq.style.textDecoration != 'line-through'){
            return;
        } else if (spanizq.style.textDecoration == 'line-through') {
            // El de la izquierda ya pidi� prestado toca descontar
            // de su auxiliar
            // <td id="aux1-0"><span></span></td>
            op1izq = document.getElementById(pref + 'aux1-' + (n + 1));
            auxizq = document.getElementById(pref + 'aux2-' + (n + 1));
            numizq = op1izq.firstChild;
        } else {
            // <td id="op1-0"><sup>P</sup><span>3</span></td>
            auxizq = document.getElementById(pref + 'aux1-' + (n + 1));
            numizq = op1izq.childNodes[1];
        }
        // Tachar el de la izquierda y restarle uno
        numizq.style.textDecoration = 'line-through';
        valizq = parseInt(numizq.firstChild.data);
        auxizq.firstChild.firstChild.data = valizq - 1;

        // Tachar el que pidio prestado y el auxiliar sumar 10
        // Escribir nuevo n�mero
        spanop1.style.textDecoration = 'line-through';
        aux.firstChild.firstChild.data = 10 + numop1;

        oppres.data = 'D';
    } else {
        // quitar nuevo anterior
        // Destachar
        oppres.data = 'P';
    }
}

function agregaResta(pref, maxcif) {
//    return ['y',1, new Array()];
    maxcif = Math.floor(maxcif);
    assert(maxcif>0);
    var n1;
    var n2;
    var res = "";
    n1 = 9611; n2 = 8119;
    n1 = 1000; n2 = 999;
    n1 = 5114; n2 = 229;
    n1 = 8005; n2 = 729;
    do {
        n1 = Math.pow(10,maxcif - 1) + 
            Math.floor(Math.random() * Math.pow(10, maxcif));
        n2 = Math.floor(Math.random() * Math.pow(10, maxcif));
    } while ((n1 < n2) || (n1 > Math.pow(10, maxcif)) );
    //n1 = 5567; n2 = 4981;
    //document.write(n1 + " "); document.write(n2);
    var n3 = n1 - n2;
    var an1 = [], an2 = [], an3 = [], lleva3 = [];
    an1[0] = 0;
    an2[0] = 0;
    an3[0] = 0;
    var nd1 = 0, nd2 = 0, nd3 = 0;
    for (i = n1; i > 0; i = Math.floor(i / 10)) {
        d = i % 10;
        an1[nd1] = d;
        nd1++;
    }
    if (nd1 == 0) {
        nd1 = 1;
    }
    for (i = n2; i > 0; i = Math.floor(i / 10)) {
        d = i % 10;
        an2[nd2] = d;
        nd2++;
    }
    if (nd2 == 0) {
        nd2 = 1;
    }
    for (i = n3; i > 0; i = Math.floor(i / 10)) {
        d = i % 10 ;
        an3[nd3] = d;
        nd3++;
    }
    for (i = nd3; i < nd1; i++) {
        an3[i] = 0;
    }
    if (nd3 == 0) {
        nd3 = 1;
    }
    if (nd3 < nd1) {
        an3[nd3] = 0;
        nd3++;
    }
    res += "<table border='1'><tr>";
    for (i = 0; i <= nd1 ; i++) {
        var id = pref + "aux2-" + (nd1 - i);
        res += "<td align='right' id='" + id + "'>" +
                "<span>&nbsp;</span></td>";
    }
    res += "</tr><tr>";
    for (i = 0; i <= nd1 ; i++) {
        var id = pref + "aux1-" + (nd1 - i);
        res += "<td align='right' id='" + id + "'>" +
                "<span>&nbsp;</span></td>";
    }
    res += "</tr><tr>";
    for (i = 0; i <= nd1 ; i++) {
        var id = pref + "op1-" + (nd1 - i);
        res += "<td align='right' id='" + id + "'>";
        res += "<sup>";
        if (i > 1) {
//            res += "<a href='#' onclick='javascript:presta(" + pref + ", " +
//                    (nd1 - i) + ")'>P</a></sup>"; // puede convertir en D
        }
        res += "</sup>";
        if (i > 0) {
            res += '<span>' + an1[nd1 - i] + '</span>';
        }
        res += "</td>";
    }
    res += "</tr><tr>";
    res += "<td>-</td>";
    for (i = 1; i <= nd1; i++) {
        res += "<td align='right' id='op2-" + (nd1 - i) + "'>";
        if (nd1 - i < nd2) {
            res += '<span>' + an2[nd1 - i] + '</span>';
        }
        res += "</td>";
    }
    res += "</tr><tr>";
    for (i = 0; i <= nd1; i++) {
        res += "<td></td>";
    }
    res += "</tr><tr>";
    for (i = 0; i <= nd1; i++) {
        res += "<td align='right' id='resp-" + (nd1 - i) + "'>&nbsp;</td>";
    }

    res += "</tr></table>";
    res += "<p id='" + pref + "resultado'>&nbsp;</p>";
    return [res, nd1, an3];
}

function agregaMultiplicacion(pref, maxcif) {
//    return ['y',1, new Array()];
    maxcif = Math.floor(maxcif);
    assert(maxcif>0);
    var n1;
    var n2;
    var res = "";
    n1 = 9611; n2 = 8119;
    n1 = 1000; n2 = 999;
    n1 = 5114; n2 = 229;
    n1 = 8005; n2 = 729;
    var n1 = Math.floor(Math.random()*Math.pow(10, maxcif));
    var n2 = Math.floor(Math.random()*Math.pow(10, maxcif));
    //document.write(n1 + " "); document.write(n2);
    var n3 = n1 - n2;
    var an1 = [], an2 = [], an3 = [], lleva3 = [];
    an1[0] = 0;
    an2[0] = 0;
    an3[0] = 0;
    var nd1 = 0, nd2 = 0, nd3 = 0;
    for (i = n1; i > 0; i = Math.floor(i / 10)) {
        d = i % 10;
        an1[nd1] = d;
        nd1++;
    }
    if (nd1 == 0) {
        nd1 = 1;
    }
    for (i = n2; i > 0; i = Math.floor(i / 10)) {
        d = i % 10;
        an2[nd2] = d;
        nd2++;
    }
    if (nd2 == 0) {
        nd2 = 1;
    }
    for (i = n3; i > 0; i = Math.floor(i / 10)) {
        d = i % 10 ;
        an3[nd3] = d;
        nd3++;
    }
    for (i = nd3; i < nd1; i++) {
        an3[i] = 0;
    }
    if (nd3 == 0) {
        nd3 = 1;
    }
    if (nd3 < nd1) {
        an3[nd3] = 0;
        nd3++;
    }
    res += "<table border='1'><tr>";
    for (i = 0; i <= nd1 ; i++) {
        var id = pref + "aux2-" + (nd1 - i);
        res += "<td align='right' id='" + id + "'>" +
                "<span>&nbsp;</span></td>";
    }
    res += "</tr><tr>";
    for (i = 0; i <= nd1 ; i++) {
        var id = pref + "aux1-" + (nd1 - i);
        res += "<td align='right' id='" + id + "'>" +
                "<span>&nbsp;</span></td>";
    }
    res += "</tr><tr>";
    for (i = 0; i <= nd1 ; i++) {
        var id = pref + "op1-" + (nd1 - i);
        res += "<td align='right' id='" + id + "'>";
        res += "<sup>";
        if (i > 1) {
//            res += "<a href='#' onclick='javascript:presta(" + pref + ", " +
//                    (nd1 - i) + ")'>P</a></sup>"; // puede convertir en D
        }
        res += "</sup>";
        if (i > 0) {
            res += '<span>' + an1[nd1 - i] + '</span>';
        }
        res += "</td>";
    }
    res += "</tr><tr>";
    res += "<td>x</td>";
    for (i = 1; i <= nd1; i++) {
        res += "<td align='right' id='op2-" + (nd1 - i) + "'>";
        if (nd1 - i < nd2) {
            res += '<span>' + an2[nd1 - i] + '</span>';
        }
        res += "</td>";
    }
    res += "</tr><tr>";
    for (i = 0; i <= nd1; i++) {
        res += "<td></td>";
    }
    for (j = 0; j < nd2; j++) {
        res += "</tr><tr>";
        for (i = 0; i <= nd1; i++) {
            res += "<td align='right' id='resp-" + (nd1 - i) + "'>&nbsp;</td>";
        }
    }
    res += "</tr><tr>";
    for (i = 0; i <= nd1; i++) {
        res += "<td></td>";
    }
    res += "</tr><tr>";
    for (i = 0; i <= nd1; i++) {
        res += "<td align='right' id='resp-" + (nd1 - i) + "'>&nbsp;</td>";
    }

    res += "</tr></table>";
    res += "<p id='" + pref + "resultado'>&nbsp;</p>";
    return [res, nd1, an3];
}

function agregaDivision(pref, maxcif) {
//    return ['y',1, new Array()];
    maxcif = Math.floor(maxcif);
    assert(maxcif>0);
    var n1;
    var n2;
    var res = "";
    do {
        n1 = Math.pow(10,maxcif - 1) + 
            Math.floor(Math.random() * Math.pow(10, maxcif));
        n2 = Math.floor(Math.random() * Math.pow(10, maxcif - 2));
    } while (n2==0 || (n1 < n2) || (n1 > Math.pow(10, maxcif)) );
    //document.write(n1 + " "); document.write(n2);
    var n3 = n1 / n2;
    var an1 = [], an2 = [], an3 = [], lleva3 = [];
    an1[0] = 0;
    an2[0] = 0;
    an3[0] = 0;
    var nd1 = 0, nd2 = 0, nd3 = 0;
    for (i = n1; i > 0; i = Math.floor(i / 10)) {
        d = i % 10;
        an1[nd1] = d;
        nd1++;
    }
    if (nd1 == 0) {
        nd1 = 1;
    }
    for (i = n2; i > 0; i = Math.floor(i / 10)) {
        d = i % 10;
        an2[nd2] = d;
        nd2++;
    }
    if (nd2 == 0) {
        nd2 = 1;
    }
    for (i = n3; i > 0; i = Math.floor(i / 10)) {
        d = i % 10 ;
        an3[nd3] = d;
        nd3++;
    }
    for (i = nd3; i < nd1; i++) {
        an3[i] = 0;
    }
    if (nd3 == 0) {
        nd3 = 1;
    }
    if (nd3 < nd1) {
        an3[nd3] = 0;
        nd3++;
    }
    res += "<table border='1'><tr>";
    res += "<tr>";
    for (i = 1; i <= nd1 ; i++) {
        var id = pref + "op1-" + (nd1 - i);
        res += "<td align='right' id='" + id + "'>";
        if (i > 0) {
            res += '<span>' + an1[nd1 - i] + '</span>';
        }
        res += "</td>";
    }
    res += "<td>/</td>";
//    res += "</tr><tr>";
    nd4 = Math.max(nd2, nd3);
    for (i = 1; i <= nd4; i++) {
        if (i <= nd2) {
            res += "<td align='right' id='op2-" + (nd2 - i) + "'>";
            res += '<span>' + an2[nd2 - i] + '</span>';
            res += "</td>";
        } else {
            res += "<td>&nbsp;</td>";
        }
    }
    res += "</tr><tr>";
    for (i = 0; i <= nd1 + nd4; i++) {
        res += "<td></td>";
    }
    res += "</tr><tr>";
    for (i = 1; i <= nd1; i++) {
        res += "<td align='right' id='resp-" + (nd1 - i) + "'>&nbsp;</td>";
    }
    res += "<td align='right' id='resp-" + (nd1 - i) + "'>|</td>";
    for (i = 1; i <= nd4; i++) {
        if (i <= nd2) {
            res += "<td align='right' id='resp-" + (nd2 - i) + "'>&nbsp;</td>";
        } else {
            res += "<td>&nbsp;</td>";
        }
    }
    res += "</tr><tr>";
    for (i = 0; i <= nd1 + nd4; i++) {
        res += "<td></td>";
    }
    res += "</tr><tr>";
    for (i = 1; i <= nd1; i++) {
        res += "<td align='right' id='resp-" + (nd1 - i) + "'>&nbsp;</td>";
    }
    for (j = 1; j < nd2; j++) {
        res += "</tr><tr>";
        for (i = 1; i <= nd1; i++) {
            res += "<td align='right' id='resp-" + (nd1 - i) + "'>&nbsp;</td>";
        }
        res += "</tr><tr>";
        for (i = 1; i <= nd1; i++) {
            res += "<td></td>";
        }
        res += "</tr><tr>";
        for (i = 1; i <= nd1; i++) {
            res += "<td align='right' id='resp-" + (nd1 - i) + "'>&nbsp;</td>";
        }
    }

    res += "</tr></table>";
    res += "<p id='" + pref + "resultado'>&nbsp;</p>";
    return [res, nd1, an3];
}




// Teclado http://www.javascriptkit.com/jsref/eventkeyboardmouse.shtml
// Flechas http://www.webmasterworld.com/javascript/3519735.htm
//
/* Teclas para resta
document.onkeypress = function(e){
	var e=window.event || e;
	if (termino) {
		return;
	}
	if (e.keyCode != 13 && e.keyCode != 8 && 
			(e.keyCode < 37 || e.keyCode > 40) &&
			e.charCode != 32 && (e.charCode < 48 || e.charCode > 57) ) {
		alert('Presiona n�meros, espacio, espacio atras y ENTER por favor');
	}
	else {
		c = document.getElementById("resp-" + nd);
		c.style.backgroundColor = '';
		if (e.keyCode == 8) {
			if (nd > 1) {
				nd--;
			}
			c = document.getElementById("resp-"+nd);
			c.style.backgroundColor = 'blue';
			c.firstChild.nodeValue = ' ';
		}
		else if (e.keyCode == 13) {
			s = '';
			r = document.getElementById('resultado').firstChild;
			rerr = 0;
			lerr = 0;
			for(i = 0; i < mnd; i++) {
				sid = "resp-" + i;
				c = document.getElementById(sid);
				assert(c != null);
				v = c.firstChild.nodeValue;
				if (res[i] != v) {
					c.style.backgroundColor='red';
					rerr++;
				}
			}
			c = document.getElementById("resp-" + nd);
			c.style.backgroundColor = 'blue';
			if (rerr + lerr == 0) {
				r.nodeValue = 'Excelente!\nSabes, Dios te ama siempre.';
				termino = true;
			}
		}
		else if (e.keyCode == 37 || e.keyCode == 40) { // Flecha izquierda
			if (nd < mnd - 1) {
				nd++;
			}
			c = document.getElementById("resp-"+nd);
			c.style.backgroundColor = 'blue';
		}
		else if (e.keyCode == 39 || e.keycode == 38) { // Flecha derecha
			if (nd>=1) {
				nd--;
			}
			c=document.getElementById("resp-"+nd);
			c.style.backgroundColor='blue';
		} else {
			if (e.charCode == 32) {
				d = 32;
			}
			else {
				d = e.charCode;
			}
			c.firstChild.nodeValue = String.fromCharCode(d);

			if (nd < mnd - 1) {
                nd++;
            }
			c = document.getElementById("resp-" + nd);
			c.style.backgroundColor = 'blue';
		}
	}
} */


var nd, mnd,res,lleva;

function daFoco(idelem) {
    a = document.getElementsByClassName('resresaltado');
	if (a.length > 0) {
        a[0].className = 'resnormal';
    }

    c = document.getElementById(idelem);
    if (c != null) {
        c.className = 'resresaltado';
	    //c.style.backgroundColor='blue';
    }
    //focoactual = idelem;
}


function creaSumas(nop, ndig) {

    var e = $('#sumas')[0];
    while (e.firstChild) {
          e.removeChild(e.firstChild);
    }
    var i;
    for(i = 0; i < nop; i++) {
        pref = "s" + i + "-";
        a = agregaSuma(pref, ndig);
        e = document.createElement("div");
        e.setAttribute('class', 'tablaop');
        e.style.float = 'left';
        e.innerHTML = a[0];
        $('#sumas')[0].appendChild(e);
        //    document.writeln(a[0]);
        mnd = a[1];
        solucion[pref] = a[2];
    }
    //termino = false;
}

function creaRestas(nop, ndig) 
{
    var e = $('#restas')[0];
    while (e.firstChild) {
          e.removeChild(e.firstChild);
    }
    var i;
    for(i = 0; i < nop; i++) {
        pref = "r" + i + "-";
        a = agregaResta(pref, ndig);
        e = document.createElement("div");
        e.setAttribute('class', 'tablaop');
        e.style.float = 'left';
        //e.innerHTML = 'x';
        e.innerHTML = a[0];
        $('#restas')[0].appendChild(e);
        //mnd = a[1];
        //res[pref] = a[1];
        //lleva[pref] = a[2];
    }
}

function creaMultiplicaciones(nop, ndig) 
{
    var e = $('#multiplicaciones')[0];
    while (e.firstChild) {
          e.removeChild(e.firstChild);
    }
    var i;
    for(i = 0; i < nop; i++) {
        pref = "m" + i + "-";
        a = agregaMultiplicacion(pref, ndig);
        e = document.createElement("div");
        e.setAttribute('class', 'tablaop');
        e.style.float = 'left';
        e.innerHTML = a[0];
        $('#multiplicaciones')[0].appendChild(e);
    }
}

function creaDivisiones(nop, ndig) 
{
    var e = $('#divisiones')[0];
    while (e.firstChild) {
          e.removeChild(e.firstChild);
    }
    var i;
    for(i = 0; i < nop; i++) {
        pref = "d" + i + "-";
        a = agregaDivision(pref, ndig);
        e = document.createElement("div");
        e.setAttribute('class', 'tablaop');
        e.style.float = 'left';
        e.innerHTML = a[0];
        $('#divisiones')[0].appendChild(e);
    }
}


	

function creaOperaciones() {
    var nop = $('#numoper')[0].value;
    var ndig = $('#numdig')[0].value;
    solucion = new Array(); //Arreglo de soluciones
    creaSumas(nop, ndig);
    creaRestas(nop, ndig);
    creaMultiplicaciones(nop, ndig);
    creaDivisiones(nop, ndig);
    daFoco('s0-1');
}
 

$(document).ready(creaOperaciones());


// Teclado http://www.javascriptkit.com/jsref/eventkeyboardmouse.shtml
// Flechas http://www.webmasterworld.com/javascript/3519735.htm
document.onkeypress=function(e){
	var e=window.event || e;
    var a = document.getElementsByClassName('resresaltado');
	if (a.length <= 0) {
        return;
    }
    var eid = a[0].id;
    var oper = eid[0];  // s - suma, r - resta, m - multiplicaci�n, d - div
    var im = eid.indexOf('-');
    var pref = eid.substr(0, im + 1);
    var nd = parseInt(eid.substr(im + 1));
    var mnd = $('#numdig')[0].value;

/*	if (termino) {
		return;
	} */
	if (e.keyCode != 13 && e.keyCode != 8 && 
			(e.keyCode < 37 || e.keyCode > 40) &&
			e.charCode != 32 && (e.charCode < 48 || e.charCode > 57) ) {
		//alert('Presiona n�meros, espacio, espacio atras y ENTER por favor');
	} else {
		if (e.keyCode == 8) {
			if (nd > 1) {
				nd--;
			}
            daFoco(pref + nd);
		} else if (e.keyCode == 13) {
			s = '';
			r = document.getElementById(pref + 'resultado');
			rerr = 0;
			lerr = 0;
            //alert(mnd);
			for(i = 0; i <= mnd * 2; i++) {
				sid = pref + i;
				c = document.getElementById(sid);
				if (c != null) {
                    v = c.firstChild.nodeValue;
                    if (solucion[pref][i] != v) {
                        c.className = 'rescambiar';
                        rerr++;
                    }
                }
			}
			if (rerr == 0) {
				r.innerHTML = "<p>Excelente!\n<br>Dios te ama siempre.</p>";
                np = parseInt(pref.substr(1, pref.length - 2));
                npref = oper + (np + 1) + "-";
                if (document.getElementById(npref+"0")) {
                    pref = npref;
                    nd = 0;
                } else {
                    switch (oper) {
                        case 's': 
                            pref = 'r0-';
                            nd = 0;
                            break;
                        case 'r':
                            pref = 'm0-';
                            nd = 0;
                            break;
                        case 'm':
                            pref = 'd0-';
                            nd = 0;
                            break;
                        default:
                            break;
                    }
                }
            }
		}
		else if (e.keyCode==37) { // Flecha izquierda
			if (nd<2*mnd-2) {
				nd+=2;
			}
		}
		else if (e.keyCode==39) { // Flecha derecha
			if (nd>2) {
				nd-=2;
			}
			c=document.getElementById("d"+nd);
			c.style.backgroundColor='blue';
		}
		else if (e.keyCode==38) { // Flecha arriba
			if (nd%2==1 && nd>1) {
				nd--;
			}
			c=document.getElementById("d"+nd);
			c.style.backgroundColor='blue';
		} else if (e.keyCode==40) { // Flecha abajo
			if (nd%2==0) {
				nd++;
			}
			c=document.getElementById("d"+nd);
			c.style.backgroundColor='blue';
		} else {
			if (e.charCode==32) {
				d=32;
			}
			else {
				d=e.charCode;
			}
			c.firstChild.nodeValue=String.fromCharCode(d);

            pf = document.getElementById(pref+(nd+1));
            if (pf != null) {
                nd++;
			}
		}
	}
    daFoco(pref+nd);
}

