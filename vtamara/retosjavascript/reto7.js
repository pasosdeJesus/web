/*  Basadas en http://es.wikiversity.org/wiki/Dise�o_Web/Retos_Javascript */

function revisaFormulario() 
{
    var valido = true;
    var correo = document.getElementById("correo");

    var ia = correo.value.indexOf('@');
    var lia = correo.value.lastIndexOf('@');
    var ip = correo.value.lastIndexOf('.');
    if (ia <= 0 ||� ia != lia || ip<ia ||
            ip>=correo.value.length - 2) {
        document.getElementById('msjcorreoerror').style.display = 'inline';
        valido = false;
    } else {
        document.getElementById('msjcorreoerror').style.display = 'none';
    }
    return valido;
}


function configMisEventos()
{
    document.getElementById("respuestausuario").onsubmit = revisaFormulario;
}

window.onload = configMisEventos;
