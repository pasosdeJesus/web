/** Pruebas de regresi�n a Trie.
 *  Dominio p�blico. 2006. vtamara@pasosdeJesus.org
 */

#include <iostream>
#include <iomanip>
#include <list>
#include <vector>
#include <fstream>
#include <istream>

using namespace std;

#include "NodoTrie.hpp"

int main() {
	NodoTrie t;	
	int ne=0;
	
	if (!t.vacio()) {
		cerr<<"Se esperaba vac�o"<<endl;
		ne++;
	}
	if (t.numPalabras()!=0) {
		cerr<<"Se esperaban 0 palabras"<<endl;
		ne++;
	}
	list<Pos> *r;
	if ((r=t.busca("hola", 0))!=NULL) {
		cerr<<"Busca deber�a dar NULL";
		ne++;
	}
	t.inserta("hola", 0, Pos(1));
	if (t.vacio()) {
		cerr<<"No se esperaba vac�o"<<endl;
		ne++;
	}
	if (t.numPalabras()!=1) {
		cerr<<"Se esperaban 1 palabras"<<endl;
		ne++;
	}
	if (t.numDiferentes()!=1) {
		cerr<<"Se esperaban 1 palabras diferentes"<<endl;
		ne++;
	}
	if ((r=t.busca("hola", 0))==NULL) {
		cerr<<"Busca no deber�a dar NULL"<<endl;
		ne++;
	}

	t.inserta("mundo", 0, Pos(2));

	t.inserta("holar", 0, Pos(3));

	t.inserta("holar", 0, Pos(4));

	if (t.numPalabras()!=4) {
		cerr<<"Se esperaban 4 palabras"<<endl;
		ne++;
	}
	if (t.numDiferentes()!=3) {
		cerr<<"Se esperaban 3 palabras diferentes"<<endl;
		ne++;
	}

	if ((r=t.busca("ho", 0))==NULL) {
		cerr<<"busca de ho no deber�a dar null"<<endl;
		ne++;
	} else if (r->size()!=0) {
		cerr<<"busca de ho deber�a ser lista vac�a"<<endl;
		ne++;
	}

	if ((r=t.busca("hola", 0))==NULL) {
		cerr<<"busca de hola no deber�a dar null"<<endl;
		ne++;
	} else if (r->size()!=1) {
		cerr<<"busca de hola deber�a ser lista de tam 1"<<endl;
		ne++;
	}

	if ((r=t.busca("holar", 0))==NULL) {
		cerr<<"busca de holar no deber�a dar null"<<endl;
		ne++;
	} else if (r->size()!=2) {
		cerr<<"busca de holar deber�a ser lista de tam 2"<<endl;
		ne++;
	}
	ofstream fo("pruebaNodoTrie.txt");
	fo<<"los env�o como ovejas en medio de lobos. "<<endl;
	fo<<"Entonces sean astutos como serpientes, e inocentes como palomas."<<endl;
	fo.close();

	t.inserta("hola", 0, Pos(1));
	t.leeTexto("pruebaNodoTrie.txt");
	int np;
	if ((np=t.numPalabras())!=22) {
		cerr<<"Se esperaban 22 palabras, no "<<np<<endl;
		ne++;
	}
	if ((np=t.numDiferentes())!=18) {
		cerr<<"Se esperaban 18 palabras diferentes, no "<<np<<endl;
		ne++;
	}
	t.muestra(0,"");
	t.escribe("pruebaNodoTrie.idx");
	
	NodoTrie o;
	o.lee("pruebaNodoTrie.idx");

	if (t.numPalabras()!=o.numPalabras()) {
		cerr<<"t y o tienen distinto n�mero de palabras"<<endl;
		ne++;
	}
	if (t.numDiferentes()!=o.numDiferentes()) {
		cerr<<"t y o tienen distinto n�mero de palabras diferentes"
			<<endl;
		ne++;
	}


/*	t.leeStream("Mark.txt");
	cout<<t.numPalabras()<<endl; 
	cout<<t.numDiferentes()<<endl;  */
	
	cerr<<"N�mero de errores: "<<ne<<endl;
	return 0;
}

