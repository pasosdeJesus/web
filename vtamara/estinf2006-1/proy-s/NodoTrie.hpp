/** Trie. Apropiado para mantener posiciones de palabras en un texto
 * 	con codificaci�n ASCII.
 * Basado en http://structio.cvs.sourceforge.net/structio/plena/plena/Trie.java?revision=1.1.1.1&view=markup
 * Dominio p�blico. 2006. vtamara@pasosdeJesus.org
 */


class Pos {
public: 
	long numb;  // N�mero de byte en el que est� la palabra
	Pos(long n):numb(n) {}
	friend std::ostream &operator<<(std::ostream &os, Pos p);
};


class NodoTrie {
private:
	list<Pos> lp; // Lista de posiciones en las que est� la palabra
	vector<NodoTrie *> hijos; // Lista de hijos
	long palabras; // N�mero de palabras en este trie

public:
	// Constructora pone 255 hijos (�ndices 1 ..255) en NULL.
	// Un hijo por cada caracter ASCII posible ---excepto 0.
	NodoTrie();  

	// Destructora libera memoria de los hijos que deb�o ser
	// localizada al insertar.
	~NodoTrie();  

	// Decide si el Trie est� vac�o
	bool NodoTrie::vacio();

	// Retorna cantidad de palabras del trie
	long numPalabras(); 

	// Retorna cantidad de palabras diferentes
	long numDiferentes(); 

	// Inserta la palabra pal a partir de su cpal-esimo caracter
	// en el trie como apareciendo en posici�n p.
	// Si cpal<pal.size() se sugiere obtener el �ndice donde se
	// insertar� con:
	// 	unsigned char posHijos=pal[cpal]
	void inserta(string pal, int cpal, Pos p); 
			
	// Busca la palabra pal a partir de su cpal-esimo caracter
	list<Pos> *busca(string pal, int cpal);

	// Completa el trie insertando las palabras del archivo tipo texto 
	// con codificaci�n ASCII na.
	void leeTexto(char *na); 

	// Lee un trie en el stream is en un formato propio
	// Se sugiere obtener la posici�n donde aparece en el texto
	// con la funci�n is.tellg();
	// Se sugiere que inicialmente no piense en eficiencia, sino
	// en poder leer y escribir con facilidad.  Tenga en cuenta
	// que un stream t�pico operado con << y >> ignora espacios
	// y cambios de l�nea, as� que fuera de esos elementos puede
	// requerir separadores diferentes (e.g : < > , etc).
	void leeStream(std::istream &is);

	// Lee un trie del archivo na en un formato propio
	void lee(char *na); 

	// Escribe un trie en el stream os en un formato propio
	void escribeStream(std::ostream &os);

	// Escribe un trie en el archivo na en un formato propio
	void escribe(char *na);

	// Para depurar presenta el estado del Trie indentando a i espacios
	void muestra(int i, string ant);
};

