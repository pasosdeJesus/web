/**
 * Cadena_nm descendiente de Objeto gen�rico.
 * No permite modificacions.
 * @author Dominio p�blico. 2006. vtamara@pasosdeJesus.org
 **/ 

#include <algorithm>
#include <cassert>
using namespace std;

#include "Cadena_nm.hpp"

Cadena_nm::Cadena_nm(char *s)
{
	c=s;
}

Cadena_nm::~Cadena_nm()
{
}

Objeto *Cadena_nm::clonar() 
{
	Cadena_nm *n=new Cadena_nm(c);

	return (Objeto *)n;
}

char *Cadena_nm::valor() 
{
	return c;
}

void Cadena_nm::poner(char *s)
{
	c=s;
}

int Cadena_nm::longitud()
{
	return strlen(c);
}
