/**
 * Objeto gen�rico. Imitando Java.
 * @author Dominio p�blico. 2006. vtamara@pasosdeJesus.org
 **/ 

#ifndef OBJETO_HPP
#define OBJETO_HPP

class Objeto {
	public:
	virtual Objeto *clonar();	
};
#endif
