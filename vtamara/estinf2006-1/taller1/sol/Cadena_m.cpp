/**
 * Cadena_m descendiente de Objeto gen�rico.
 * Mutable.
 * @author Dominio p�blico. 2006. vtamara@pasosdeJesus.org
 **/ 

#include <algorithm>
#include <cassert>
using namespace std;

#include "Cadena_m.hpp"

Cadena_m::Cadena_m(char *s)
{
	c=new char[strlen(s)+1];
	strcpy(c,s);
	c[strlen(s)]='\0';
}

Cadena_m::Cadena_m(Cadena_m &s)
{
	int l=strlen(s.c);
	c=new char[l+1];
	strcpy(c,s.c);
	c[l]='\0';
}

Cadena_m::~Cadena_m()
{
	delete[] c;
	c=NULL;
}

Objeto *Cadena_m::clonar() 
{
	Cadena_m *n=new Cadena_m(c);

	return (Objeto *)n;
}

char *Cadena_m::valor() 
{
	return c;
}

void Cadena_m::poner(char *s)
{
	char *nc=new char[strlen(s)+1];
	strcpy(nc,s);
	nc[strlen(s)]='\0';
	delete[] c;
	c=nc;
}

int Cadena_m::longitud()
{
	return strlen(c);
}

/*char *Cadena_m::aCadena()
{
	char *nc=new char[longitud()+1];
	strcpy(nc,c);
	nc[longitud()]='\0';
	return nc;
} */

char Cadena_m::pos(int i)
{
	assert(i>=0);
	assert(i<longitud());
	return c[i];
}

void Cadena_m::concatenar(Cadena_m s)
{
	char *nc=new char[longitud()+s.longitud()+1];
	strcpy(nc,c);
	nc[longitud()]='\0';
	strcat(nc,s.c);
	nc[longitud()+s.longitud()]='\0';
	delete[] c;
	c=nc;

}

void Cadena_m::concatenar(char c)
{
	char *nc=new char[longitud()+2];
	strcpy(nc,this->c);
	nc[longitud()]=c;
	nc[longitud()+1]='\0';
	delete[] this->c;
	this->c=nc;
}

void Cadena_m::aMinusculas() 
{
	for(int i=0; i<longitud(); i++) {
		if (c[i]>='A' && c[i]<='Z') {
			c[i]+='a'-'A';
		}
		else {
			switch (c[i]) { 
				case '�' : c[i]='�'; break;
				case '�' : c[i]='�'; break;
				case '�' : c[i]='�'; break;
				case '�' : c[i]='�'; break;
				case '�' : c[i]='�'; break;
				case '�' : c[i]='�'; break;
				case '�' : c[i]='�'; break;
				default: break;
			}
		}
	}
}


void Cadena_m::aMayusculas() 
{
	for(int i=0; i<longitud(); i++) {
		if (c[i]>='a' && c[i]<='z') {
			c[i]-='a'-'A';
		}
		else {
			switch (c[i]) { 
				case '�' : c[i]='�'; break;
				case '�' : c[i]='�'; break;
				case '�' : c[i]='�'; break;
				case '�' : c[i]='�'; break;
				case '�' : c[i]='�'; break;
				case '�' : c[i]='�'; break;
				case '�' : c[i]='�'; break;
				default: break;
			}
		}
	}
}

void Cadena_m::remplazar(Cadena_m b, Cadena_m r)
{
	Cadena_m res(""); // La destructora debe ser llamada por el codigo generado por el compilador al salir de la funci�n
	if (b.longitud()==0) {
		return;
	}
	int j=0; // Indice sobre b.c
	int i=0; // Indice sobre this->c
	int k;
	/*cout<<"Al comenzar c es "<<c<<", b es "<<b.valor()<<", r es "<<
		r.valor()<<"\n";*/
	for(i=0; i<=longitud(); i++) {
		//cout<<"i es "<<i<<", j es "<<j<<" ";
		if (c[i]==b.c[j]) {
			//cout<<"c[i]==b.c[j] ";
			j++;
		       if (j==b.longitud()) {
			     //cout<<"j==b.longitud() ";
		       	     res.concatenar(r);
			     j=0; 
		       }
		}
		else {
			//cout<<"c[i]!=b.c[j] ";
			for (k=i-j; k<=i; k++) {
				res.concatenar(c[k]);
			}
			j=0;
		}
		//cout<<"Al final de iteraci�n res va en "<<res.valor()<<"\n";
	}
	for (k=i-j; k<i; k++) {
		res.concatenar(c[i]);
	}
	//cout<<"Al terminar res qued� en "<<res.valor()<<"\n";
	
	//Sacamos copia porque res deber� ser destruido al salir del funci�n
	char *nc=new char[res.longitud()+1];
	strcpy(nc,res.valor());
	nc[res.longitud()]='\0';
	delete[] c;
	c=nc;
}
