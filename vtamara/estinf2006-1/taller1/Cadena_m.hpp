/**
 * Cadena_m descendiente de Objeto gen�rico.
 * Mutable.
 * @author Dominio p�blico. 2006. vtamara@pasosdeJesus.org
 **/ 

#ifndef CADENA_M_HPP
#define CADENA_M_HPP

#include "Objeto.hpp"

class Cadena_m:public Objeto {
	private:
		char *c;
	public:
		/* Constructoras */
		Cadena_m(char *s="");
		Cadena_m(Cadena_m &s);
		Objeto *clonar();	

		/* Destructora (s) */
		~Cadena_m();

		/* Analizadoras */
		char *valor();
		int longitud();
		// Retorna caracter que est� en pos i-esima
		char pos(int i);

		/* Modificadoras */
		void poner(char *s);
		void concatenar(Cadena_m s);
		void concatenar(char c);
		void aMinusculas();	
		void aMayusculas();	
		// Remplaza toda ocurrencia de b en el objeto por r
		void remplazar(Cadena_m b, Cadena_m r);
};
#endif
