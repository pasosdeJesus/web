/**
 * Pruebas a Cadena_m
 * @author 2006. Dominio p�blico. vtamara@pasosdeJesus.org
 **/ 

#include <algorithm>
#include <cassert>
#include <iostream>
using namespace std;

#include "Cadena_m.hpp"

int main()
{
	int ner=0;
	char *rs; // Valor retornado en algunos casos

	cout << "Probando Cadena_m:" << "\n";
	cout << "Constructoras:\n";
	Cadena_m vacia("");  
	if (vacia.longitud()!=0) {
		cout << "* Vac�a debe ser vac�a\n";
		ner++;
	}	
	Cadena_m hola("hola");
	if (hola.longitud()!=4) {
		cout << "* Cadena hola es de 4 caracteres\n";
		ner++;
	} 
	if (strcmp(hola.valor(),"hola")!=0) {
		cout << "* hola deber�a ser hola\n";
		ner++;
	} 
	char chola2[]="hola2";
	Cadena_m hola2(chola2);
	chola2[0]='\0';
	if (strcmp(hola2.valor(),"hola2")!=0) {
		cout << "* hola2 deber�a ser de 5 puede que no haya encapsulamiento\n";
		ner++;
	} 
	Cadena_m *clona=(Cadena_m *)hola2.clonar();
	if (strcmp(rs=clona->valor(),"hola2")!=0) {
		cout << "* clonar no funciona dio "<<rs<<"\n";
		ner++;
	} 
	clona->poner("");
	if (strcmp(clona->valor(),"")!=0) {
		cout << "* poner en clona no funciona dio "<<rs<<"\n";
		ner++;
	} 
	if (strcmp(hola2.valor(),"hola2")!=0) {
		cout << "* hola2 deber�a seguir siendo hola2 despu�s de clonar, no hay encapsulamiento\n";
		ner++;
	} 

	cout << "Destructora:\n";
	delete clona;

	cout << "Analizadoras:\n";
	char rc;
	if ((rc=hola2.pos(0))!='h') {
		cout << "* pos(0) fall�, di�"<<rc<<"\n";	
		ner++;
	}

	cout << "Modificadoras:\n";
	hola2.concatenar(vacia);
	if (strcmp((rs=hola2.valor()), "hola2")!=0) {
		cout << "* concatenar vacia no funciona, dio "<<rs<<"\n";
		ner++;
	}
	hola2.concatenar(hola);
	if (strcmp(rs=hola2.valor(), "hola2hola")!=0) {
		cout << "* concatenar hola no funciona, dio "<<rs<<"\n";
		ner++;
	}
	Cadena_m novac("");
	novac.concatenar(hola);
	if (strcmp(rs=novac.valor(), "hola")!=0) {
		cout << "* concatenar a vacio hola no funciona, dio "<<rs<<"\n";
		ner++;
	}
	vacia.aMinusculas();
	if (vacia.longitud()!=0) {
		cout << "* aMinuscula de vacio no funciona dio"<<
			vacia.valor()<<"\n";
		ner++;
	}
	vacia.aMayusculas();
	if (vacia.longitud()!=0) {
		cout << "* aMayuscula de vacio no funciona dio"<<
			vacia.valor()<<"\n";
		ner++;
	}
	hola2.aMayusculas();
	if (strcmp(rs=hola2.valor(), "HOLA2HOLA")!=0) {
		cout << "* aMayusculas no funciona, dio "<<rs<<"\n";
		ner++;
	}
	hola2.aMinusculas();
	if (strcmp(rs=hola2.valor(), "hola2hola")!=0) {
		cout << "* aMinusculas no funciona, dio "<<rs<<"\n";
		ner++;
	}
	Cadena_m espanol("�������");
	espanol.aMayusculas();
	if (strcmp(rs=espanol.valor(), "�������")!=0) {
		cout << "* aMayusculas no funciona con caracteres en espa�ol, di� "<<rs<<"\n";
		ner++;
	}
	espanol.aMinusculas();
	if (strcmp(rs=espanol.valor(), "�������")!=0) {
		cout << "* aMinusculas no funciona con caracteres en espa�ol, di� "<<rs<<"\n";
		ner++;
	}	
	Cadena_m dos("2");
	hola2.remplazar(vacia, vacia);
	if (strcmp(rs=hola2.valor(), "hola2hola")!=0) {
		cout << "* Remplazar vacia por vacia no deber�a alterar cadena, di� "<<rs<<"\n";
		ner++;
	}	
	vacia.remplazar(dos, hola);
	if (strcmp(rs=vacia.valor(), "")!=0) {
		cout << "* Remplazar en vacia no deber�a alterarla, di� "<<rs<<"\n";
		ner++;
	}	
	espanol.remplazar(vacia,dos);
	if (strcmp(rs=espanol.valor(), "�������")!=0) {
		cout << "* Remplazar en espanol, vacia por dos no deber�a alterar, di� "<<rs<<"\n";
		ner++;
	}	
	hola2.remplazar(dos, vacia);
	if (strcmp(rs=hola2.valor(), "holahola")!=0) {
		cout << "* Remplazar dos por vacia en hola2 di� "<<rs<<"\n";
		ner++;
	}	
	hola2.poner("holahola");  // Para que las pruebas siguientes tengan sentido si la anterior fall�
	Cadena_m la("la");
	Cadena_m el("el");
	hola2.remplazar(la,el);
	if (strcmp(rs=hola2.valor(), "hoelhoel")!=0) {
		cout << "* Remplazar la por el en hola2 di� "<<rs<<"\n";
		ner++;
	}	
	if (strcmp(rs=la.valor(), "la")!=0) {
		cout << "* la modificado despu�s de remplazar di� "<<rs<<"\n";
		ner++;
	}	
	if (strcmp(rs=el.valor(), "el")!=0) {
		cout << "* el modificado despu�s de remplazar di� "<<rs<<"\n";
		ner++;
	}	
	hola2.poner("hoelhoel");
	Cadena_m e("e");
	hola2.remplazar(la, e);
	if (strcmp(rs=hola2.valor(), "hoelhoel")!=0) {
		cout << "* Remplazar la por e en hola2 di� "<<rs<<"\n";
		ner++;
	}	
	hola2.poner("hoelhoel");
	hola2.remplazar(el, e);
	if (strcmp(rs=hola2.valor(), "hoehoe")!=0) {
		cout << "* Remplazar el por e en hola2 di� "<<rs<<"\n";
		ner++;
	}	
	hola2.poner("hoehoe");
	Cadena_m maslarga("maslarga");
	hola2.remplazar(e, maslarga);
	if (strcmp(rs=hola2.valor(), "homaslargahomaslarga")!=0) {
		cout << "* Remplazar e por maslarga en hola2 di� "<<rs<<"\n";
		ner++;
	}	
	hola2.poner("homaslargahomaslarga");
	Cadena_m gaho("gaho");
	hola2.remplazar(gaho, e);
	if (strcmp(rs=hola2.valor(), "homaslaremaslarga")!=0) {
		cout << "* Remplazar gaho por e en hola2 di� "<<rs<<"\n";
		ner++;
	}	

	cout <<"N�mero de errores: " << ner << "\n";
	return ner;
}
