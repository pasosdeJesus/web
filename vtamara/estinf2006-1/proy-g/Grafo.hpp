/**
 * Clase Grafo dirigido etiquetado.  Implementaci�n con lista de adyacentes.
 * Dominio p�blico. 2006. vtamara@pasosdeJesus.org
 **/

#include <list>
#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <utility>

#include <cassert>
using namespace std;

// Extensi�n para la plantilla de clase pair, ver
// documentaci�n de esta clase en http://www.sgi.com/tech/stl/pair.html
template <class T1, class T2>
std::ostream &operator<<(std::ostream &os, pair<T1,T2> p)
{
	os<<"("<<p.first<<", "<<p.second<<")";
	return os;
}


// Modela un nodo del grafo dirigido.  Es el dato contenido en el nodo 
// y la lista de nodos adyacentes referenciados cada uno por su �ndice.
// T es el tipo del dato en cada nodo, mientras E el tipo del dato en 
// cada arco.
template <class T, class E>
class NodoGrafo {
	T d;  // Dato

	//Adyacentes.
	//Cada uno es pareja de �ndice a adyacente y etiqueta del arco
	list<pair<int, E> > ady; 

public:
	NodoGrafo(T n) 
	{
		d=n;
	}

	// Retorna el dato de este nodo
	T dato() 
	{
		return d;
	}

	// Retorna lista de adyacentes a este nodo
	list<pair<int, E> > &adyacentes() 
	{
		return ady;
	}

	// Agrega un nodo adyacente no repetido a este junto con etiqueta 
	// del arco
	void agregaAdyacente(int a, E etiqueta) {
		pair<int, E> n(a, etiqueta);
		ady.push_back(n);
	}

};


// Clase Grafo Dirigido con Etiquetas.
// T es tipo del dato de cada nodo.
// E es tipo de las etiquetas
template <class T, class E>
class Grafo {
	vector<NodoGrafo<T,E> > v; //Nodos
public:
	Grafo() 
	{ }

	// Retorna orden del grafo, i.e Cantidad de nodos
	int orden()
	{
		return v.size();
	}

	// Retorna cantidad de arcos del grafo
	int numArcos()
	{
		int i,numa=0;

		// Podr�a usarse find_if de STL en lugar del siguiente ciclo
		for(i=0; i<v.size(); i++) {
			numa+= v[i].adyacentes().size();
		}
		return numa;
	}

	// Decide si existe un nodo con �ndice i y en tal caso retorna el dato
	bool hayNodo(int i, T &dato)
	{
		if (i>=0 && i<orden()) {
			dato=v[i].dato();
			return true;
		}
		return false;
	}

	// Decide si existe un nodo con un dato dado y en tal caso retorna true
	// y el �ndice en i. 
	bool hayNodo(T dato, int &i) 
	{
		// La palabra reservada typename ayuda al compilador a 
		// reconocer declaraciones complicadas
		typename vector<NodoGrafo<T,E> >::iterator iv;

		// Podr�a usarse find_if de STL en lugar del siguiente ciclo
		for(iv=v.begin(), i=0; iv!=v.end(); iv++, i++) {
			if ((*iv).dato()==dato) {
				return  true;
			}
		}
		return false;
	}

	// Decide si hay un arco entre el nodo con indice fuente y el 
	// nodo con �ndice destino y en tal caso retorna true y la etiqueta en
	// etiq
	bool hayArco(int fuente, int destino, E &etiq) 
	{
		T dt;
		assert(hayNodo(fuente, dt));
		assert(hayNodo(destino, dt));

		list<pair<int, E> > ady=v[fuente].adyacentes();
		typename list<pair<int, E> >::iterator l;

		for (l=ady.begin(); l!=ady.end(); l++) {
			if (l->first==destino) {
				etiq=l->second;
				return true;
			}
		}
		return false;
	}

	// A�ade un nodo no repetido y retorna el �ndice del mismo
	int agregaNodo(T n) 
	{
		int pi;
		assert(!hayNodo(n,pi));

		NodoGrafo<T,E> ng(n);
		v.push_back(ng);
		return v.size()-1;
	}

	// A�ade un arco no repetido dados los �ndices del nodo inicial 
	// y final, as� como la etiqueta
	void agregaArco(int fuente, int destino, E etiqueta) 
	{
		assert(fuente>=0 && fuente<v.size());
		assert(destino>=0 && destino<v.size());
		E et;
		assert(!hayArco(fuente, destino, et));

		v[fuente].agregaAdyacente(destino, etiqueta);
	}

	// A�ade un arco no repetido dados el nodo inicial 
	// y el final, as� como la etiqueta
	void agregaArco(T fuente, T destino, E etiqueta) 
	{
		int pf, pd;
		bool h=hayNodo(fuente,pf);
		assert(h);
		h=hayNodo(destino,pd);
		assert(h);
		E et;
		assert(!hayArco(pf, pd, et));

		v[pf].agregaAdyacente(pd, etiqueta);
	}



	// Dado un nodo n, retorna lista de nodos adyacentes, i.e 
	// p est� en la lista si hay un arco (n, p)
	// Retorna cada adyacente junto con la etiqueta.
	list<pair<int, E> > adyacentes(int n) 
	{
		assert(n>=0 && n<v.size());

		return v[n].adyacentes();
	} 

	// Dado un nodo n, retorna lista de nodos predecesores, i.e
	// p esta en la lista de predecesores de n si hay un arco (p, n)
	list<pair<int, E> > predecesores(int n) 
	{
		assert(n>=0 && n<v.size());

		list<pair<int, E> > res;
		for (int i=0; i<v.size(); i++) {
			list<pair<int, E> > ady=v[i].adyacentes();
			typename list<pair<int, E> >::iterator l;
			for (l=ady.begin(); l!=ady.end(); l++) {
				if (l->first==n) {
					res.push_back(pair<int, E>(i, l->second));
				}
			}
		}
		return res;
	} 


	// Para mostrar el grafo. Presenta lista de nodos y funci�n de
	// etiquetado.
	void muestra(std::ostream &os) 
	{
		list<pair<int, E> > ady;
		int i;
		typename list<pair<int, E> >::iterator l;

		os<<"V={";
		// Con STL aqu� podr�a usarse copy o for_each en lugar del ciclo
		for(i=0; i<v.size(); i++) {
			if (i>0) {
				os<<", ";
			}
			os<<v[i].dato();
		}
		os<<"}\n";
		os<<"A: {";
		for(i=0; i<v.size(); i++) {
			if (i>0) {
				os<<"\n    ";
			}
			ady=v[i].adyacentes();
			for (l=ady.begin();
				l!=ady.end(); l++) {
				os<<"(("<<v[i].dato()<<", "<<v[l->first].dato()
					<<"),"<<l->second<<")";
			}
		}
		os<<"}\n";

	}

};


// Extensi�n para la plantilla de clase list
template <class T>
std::ostream &operator<<(std::ostream &os, list<T> l)
{
	typename list<T>::iterator i;
	os<<"[";
	for(i=l.begin(); i!=l.end(); i++) {
		if (i!=l.begin()) {
			os<<", ";
		}
		os<<*i;
	}
	os<<"]";
	return os;
}


