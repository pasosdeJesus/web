/**
 * Entero descendiente de Objeto gen�rico.
 * @author Dominio p�blico. 2006. vtamara@pasosdeJesus.org
 **/ 

#ifndef ENTERO_HPP
#define ENTERO_HPP

#include "Objeto.hpp"
#include "iostream"

class Entero: public Objeto {
	private:
		int e;
	public:
		Entero(int n=0);
		inline Entero(const Entero &x):e(x.e) {};
		~Entero() {};
		int valor();
		void poner(int n=0);
		Objeto *clonar();	
		char *aCadena();	

		Objeto &operator=(const Objeto &);
		// Operador de llamada funcional
		int operator()(float x);

		// Operador de subindexado
		Entero &operator[](const int &i); //No necesario

		// Operador relacionados con memoria (peligrosos)
		Entero *operator->() { 
			return this; 
		} 
		Entero operator*() { 
			return *this; 
		}  
		Entero *operator&() {  
			return this; 
		} 
		void *operator new(size_t s) { 
		//http://www.informit.com/articles/article.asp?p=30642&rl=1
			return ::operator new(s); 
		}
		void operator delete(void *p, size_t s) { 
			::operator delete(p); 
		}
		void *operator new[](size_t s) { 
			return ::operator new[](s); 
		}
		void operator delete[](void *p, size_t s) { 
			::operator delete(p); 
		}

	
		// Operadores de asignaci�n
		Entero &operator=(const Entero &f);
		Entero &operator=(const int &i);
		Entero &operator+=(const Entero &i);
		Entero &operator+=(const int &i);
		Entero &operator-=(const Entero &i);
		Entero &operator-=(const int &i);
		Entero &operator*=(const Entero &i);
		Entero &operator*=(const int &i);
		Entero &operator/=(const Entero &i);
		Entero &operator/=(const int &i);
		Entero &operator%=(const Entero &i);
		Entero &operator%=(const int &i);
		Entero &operator^=(const Entero &i);
		Entero &operator^=(const int &i);
		Entero &operator&=(const Entero &i);
		Entero &operator&=(const int &i);
		Entero &operator|=(const Entero &i);
		Entero &operator|=(const int &i);

		//Operadores de pre/post-incremento/decremento
		Entero operator++();
		Entero operator--();
		Entero operator++(int);
		Entero operator--(int);

		// Operadores unarios
		friend Entero operator-(const Entero &);
		friend Entero operator+(const Entero &);
		friend Entero operator~(const Entero &);
		friend Entero operator!(const Entero &);

		// Operadores binarios
		friend Entero operator+(const Entero &x, const Entero &y);
		friend Entero operator+(const Entero &x, const int &y);
		friend Entero operator+(const int &x, const Entero &y);
		friend Entero operator-(const Entero &x, const Entero &y);
                friend Entero operator-(const Entero &x, const int &y);
                friend Entero operator-(const int &x, const Entero &y);
		friend Entero operator*(const Entero &x, const Entero &y);
		friend Entero operator*(const Entero &x, const int &y);
		friend Entero operator*(const int &x, const Entero &y);
		friend Entero operator/(const Entero &x, const Entero &y);
		friend Entero operator/(const Entero &x, const int &y);
		friend Entero operator/(const int &x, const Entero &y);
		friend Entero operator%(const Entero &x, const Entero &y);
		friend Entero operator%(const Entero &x, const int &y);
		friend Entero operator%(const int &x, const Entero &y);
		friend Entero operator^(const Entero &x, const Entero &y);
		friend Entero operator^(const Entero &x, const int &y);
		friend Entero operator^(const int &x, const Entero &y);
		friend Entero operator&(const Entero &x, const Entero &y);
		friend Entero operator&(const Entero &x, const int &y);
		friend Entero operator&(const int &x, const Entero &y);
		friend Entero operator|(const Entero &x, const Entero &y);
		friend Entero operator|(const Entero &x, const int &y);
		friend Entero operator|(const int &x, const Entero &y);
		friend bool operator==(const Entero &x, const Entero &y);
		friend bool operator==(const Entero &x, const int &y);
		friend bool operator==(const int &x, const Entero &y);
		friend bool operator<(const Entero &x, const Entero &y);
		friend bool operator<(const Entero &x, const int &y);
		friend bool operator<(const int &x, const Entero &y);
		friend bool operator>(const Entero &x, const Entero &y);
		friend bool operator>(const Entero &x, const int &y);
		friend bool operator>(const int &x, const Entero &y);
		friend bool operator&&(const Entero &x, const Entero &y);
		friend bool operator&&(const Entero &x, const int &y);
		friend bool operator&&(const int &x, const Entero &y);
		friend bool operator!=(const Entero &x, const Entero &y);
		friend bool operator!=(const Entero &x, const int &y);
		friend bool operator!=(const int &x, const Entero &y);
		friend bool operator<=(const Entero &x, const Entero &y);
		friend bool operator<=(const Entero &x, const int &y);
		friend bool operator<=(const int &x, const Entero &y);
		friend bool operator>=(const Entero &x, const Entero &y);
		friend bool operator>=(const Entero &x, const int &y);
		friend bool operator>=(const int &x, const Entero &y);
		friend bool operator||(const Entero &x, const Entero &y);
		friend bool operator||(const Entero &x, const int &y);
		friend bool operator||(const int &x, const Entero &y);


		// Operadores para operaci�n con streams
		
		friend std::istream &operator>>(std::istream &is, 
				Entero &e);
		friend std::ostream &operator<<(std::ostream &os, 
				const Entero &e);

};
#endif
