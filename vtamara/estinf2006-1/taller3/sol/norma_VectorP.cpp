/**
 * Calcula norma de un VectorP con 10000 3eses
 * @author 2006. Dominio p�blico. vtamara@pasosdeJesus.org
 **/ 

#include <algorithm>
#include <cassert>
#include <iostream>
using namespace std;

#include "VectorP.hpp"

int raizentera(int x) 
{
	assert(x>=0);
	int r;
	for (r=0; r*r<x; r++) {}
	if (r*r==x) {
		return r;
	}
	return r-1;
}


int normaentera(VectorP<int> &v) 
{
	int s=0;
	for (int i=0; i<v.longitud(); i++) {
		int t=v[i];
		s+=t*t;
	}
	return raizentera(s);
}


int main()
{
	int ner=0;
	VectorP<int> v;
	int n; //norma
	char *r;

	cout << "Comenzando" << "\n";
	for (int i=0; i<1000; i++) {
		v.agregar(3);
		r=v.aCadena();
		delete r;
	}
	//v.depura();
	n=normaentera(v);
	cout << "Norma n="<<n<<"\n";
	return ner;
}
