/**
 * Entero descendiente de Objeto genérico.
 * @author Dominio público. 2006. vtamara@pasosdeJesus.org
 **/ 

#include <iostream>
#include <algorithm>
#include <cassert>
#include <cmath>
using namespace std;

#include "Entero.hpp"

Entero::Entero(int n)
{
	e=n;
}
int Entero::valor()
{
	return e;
}
void Entero::poner(int n)
{
	e=n;
}

Objeto *Entero::clonar() 
{
	Entero *n=new Entero(e);
	return (Objeto *)n;
}
Objeto &Entero::operator=(const Objeto &o)  
{  // Supondremos que o es Entero 
	Entero *oe=(Entero *)&o;
	e=oe->e;
	return *this;
}

/**
 * Retorna represtación del entero como cadena.
 * Si no puede localizar memoria lanza una excepción
 */
char *Entero::aCadena()
{
	double ld= e==0 ? 0 : log10((double)abs(e));
	int l=(int)(ld+1);
	char *s;
	l = (e<0) ? l+1 : l;
	s=new char[l+1];
	snprintf(s, l+1, "%d", e);
//	cout <<"OJO s es "<< s <<"\n";
	return s;
}


//Implementación de operadores con base en implementación de complex de stdlibc++ 
Entero operator-(const Entero &e)
{
	return Entero(-e.e);
}

Entero operator+(const Entero &e)
{
	return Entero(+e.e);
}

Entero operator~(const Entero &x)
{
	return Entero(~x.e);
}

Entero operator!(const Entero &e)
{
	return Entero(!e.e);
}

int Entero::operator()(float x)
{
	e+=(int)x;
	return e;
}
Entero &Entero::operator=(const Entero &f)
{
	e=f.e;
	return (*this);
}
Entero &Entero::operator=(const int &i)
{
	e=i;
	return (*this);
}

Entero &Entero::operator+=(const Entero &i)
{
	e+=i.e;
	return *this;
}

Entero &Entero::operator+=(const int &i)
{
	e+=i;
	return *this;
}

Entero &Entero::operator-=(const Entero &i)
{
	e-=i.e;
	return *this;
}

Entero &Entero::operator-=(const int &i)
{
	e-=i;
	return *this;
}

Entero &Entero::operator*=(const Entero &i)
{
	e*=i.e;
	return *this;
}

Entero &Entero::operator*=(const int &i)
{
	e*=i;
	return *this;
}

Entero &Entero::operator/=(const Entero &i)
{
	e/=i.e;
	return *this;
}

Entero &Entero::operator/=(const int &i)
{
	e/=i;
	return *this;
}

Entero &Entero::operator%=(const Entero &i)
{
	e%=i.e;
	return *this;
}

Entero &Entero::operator%=(const int &i)
{
	e%=i;
	return *this;
}

Entero &Entero::operator^=(const Entero &i)
{
	e^=i.e;
	return *this;
}

Entero &Entero::operator^=(const int &i)
{
	e^=i;
	return *this;
}

Entero &Entero::operator&=(const Entero &i)
{
	e&=i.e;
	return *this;
}

Entero &Entero::operator&=(const int &i)
{
	e&=i;
	return *this;
}

Entero &Entero::operator|=(const Entero &i)
{
	e|=i.e;
	return *this;
}

Entero &Entero::operator|=(const int &i)
{
	e|=i;
	return *this;
}

// Idea de complex de glibstdc++
Entero operator+(const Entero &x, const Entero &y)
{
	return Entero(x.e+y.e);
}

Entero operator+(const Entero &x, const int &y)
{
	return Entero(x)+=y;
}

Entero operator+(const int &x, const Entero &y)
{
	return Entero(x)+=y;
} 

Entero operator-(const Entero &x, const Entero &y)
{
	return Entero(x)-=y;
}

Entero operator-(const Entero &x, const int &y)
{
	return Entero(x)-=y;
}

Entero operator-(const int &x, const Entero &y)
{
	return Entero(x)-=y;
}

Entero operator*(const Entero &x, const Entero &y)
{
	return Entero(x)*=y;
}

Entero operator*(const Entero &x, const int &y)
{
	return Entero(x)*=y;
}

Entero operator*(const int &x, const Entero &y)
{
	return Entero(x)*=y;
}

Entero operator/(const Entero &x, const Entero &y)
{
	return Entero(x)/=y;
}

Entero operator/(const Entero &x, const int &y)
{
	return Entero(x)/=y;
}

Entero operator/(const int &x, const Entero &y)
{
	return Entero(x)/=y;
}

Entero operator%(const Entero &x, const Entero &y)
{
	return Entero(x)%=y;
}

Entero operator%(const Entero &x, const int &y)
{
	return Entero(x)%=y;
}

Entero operator%(const int &x, const Entero &y)
{
	return Entero(x)%=y;
}

Entero operator^(const Entero &x, const Entero &y)
{
	return Entero(x)^=y;
}

Entero operator^(const Entero &x, const int &y)
{
	return Entero(x)^=y;
}

Entero operator^(const int &x, const Entero &y)
{
	return Entero(x)^=y;
}

Entero operator&(const Entero &x, const Entero &y)
{
	return Entero(x)&=y;
}

Entero operator&(const Entero &x, const int &y)
{
	return Entero(x)&=y;
}

Entero operator&(const int &x, const Entero &y)
{
	return Entero(x)&=y;
}

Entero operator|(const Entero &x, const Entero &y)
{
	return Entero(x)|=y;
}

Entero operator|(const Entero &x, const int &y)
{
	return Entero(x)|=y;
}

Entero operator|(const int &x, const Entero &y)
{
	return Entero(x)|=y;
}

bool operator==(const Entero &x, const Entero &y)
{
	return x.e==y.e;
}

bool operator==(const Entero &x, const int &y)
{
	return x.e==y;
}

bool operator==(const int &x, const Entero &y)
{
	return x==y.e;
}

bool operator<(const Entero &x, const Entero &y)
{
	return x.e<y.e;
}

bool operator<(const Entero &x, const int &y)
{
	return x.e<y;
}

bool operator<(const int &x, const Entero &y)
{
	return x<y.e;
}

bool operator>(const Entero &x, const Entero &y)
{
	return x.e>y.e;
}

bool operator>(const Entero &x, const int &y)
{
	return x.e>y;
}

bool operator>(const int &x, const Entero &y)
{
	return x>y.e;
}

bool operator && (const Entero &x, const Entero &y)
{
	return x.e && y.e;
}

bool operator && (const Entero &x, const int &y)
{
	return x.e && y;
}

bool operator && (const int &x, const Entero &y)
{
	return x && y.e;
}

bool operator!=(const Entero &x, const Entero &y)
{
	return x.e!=y.e;
}

bool operator!=(const Entero &x, const int &y)
{
	return x.e!=y;
}

bool operator!=(const int &x, const Entero &y)
{
	return x!=y.e;
}

bool operator<=(const Entero &x, const Entero &y)
{
	return x.e<=y.e;
}

bool operator<=(const Entero &x, const int &y)
{
	return x.e<=y;
}

bool operator<=(const int &x, const Entero &y)
{
	return x<=y.e;
}

bool operator>=(const Entero &x, const Entero &y)
{
	return x.e>=y.e;
}

bool operator>=(const Entero &x, const int &y)
{
	return x.e>=y;
}

bool operator>=(const int &x, const Entero &y)
{
	return x>=y.e;
}

bool operator || (const Entero &x, const Entero &y)
{
	return x.e || y.e;
}

bool operator || (const Entero &x, const int &y)
{
	return x.e || y;
}

bool operator || (const int &x, const Entero &y)
{
	return x || y.e;
}

Entero &Entero::operator[](const int &i)
{
	return (*this);
}

Entero Entero::operator++()
{
	return ++e;
}

Entero Entero::operator--()
{
	return --e;
}

Entero Entero::operator++(int)
{
	return e++;
}

Entero Entero::operator--(int)
{
	return e--;
}

std::istream &operator>>(std::istream &is, Entero &e)
{
	return is >> e.e;
}

std::ostream &operator<<(std::ostream &os, const Entero &e)
{
	return os << e.e;
}
