/**
 * Vector como plantilla
 * @author Dominio p�blico. 2006. vtamara@pasosdeJesus.org
 **/ 

#ifndef VECTORP_HPP
#define VECTORP_HPP

#include <algorithm>
#include <cassert>
using namespace std;


template <class T>
class VectorP {
	private:
		T *v;
		int l;;
	public:
		inline VectorP() 
		{ 
			v=NULL; 
			l=0; 
		}
		inline VectorP(const VectorP<T> &o) 
		{ 
			l=o.l;
			v=new T[l];
			for(int i=0; i<l; i++) {
				v[i]=o[i];
			}
		}

		inline ~VectorP() 
		{ 
			if (v!=NULL) { 
				delete[] v; 
			} 
		}

		VectorP<T> operator=(VectorP<T> o)
		{
			l=o.l;
			v=new T[l];
			for(int i=0; i<l; i++) {
				v[i]=o[i];
			}
		}

		T &operator[](int i)
		{
			assert(i>=0);
			assert(i<l);
			return v[i];
		}

		void depura()
		{
			string sep="";
			cerr<<"[";
			for(int i=0; i<l; i++) {
				cerr<<sep<<v[i];
				sep=",";
			}
			cerr<<"]";
		}

		void agregar(T e) 
		{
			int i;
			T *v2=new T[l+1];
			for (i=0;i<l; i++) {
				v2[i]=v[i];
			}
			v2[i]=e;
			delete[] v;
			v=v2;
			l++;
		}

		int longitud()
		{
			return l;
		}

		char *aCadena();
};


template <>
char *VectorP<int>::aCadena() {
	char *s;
	int i,tl=0;
	char **vc=new (char *)[l];
	for (i=0; i<l; i++) {
		s=new char[10];
		snprintf(s, 10, "%i", v[i]);
		tl+=strlen(s)+2;
		vc[i]=s;
	}
	s=new char[tl+3];
	s[0]='[';s[1]='\0';
	for (i=0; i<l; i++) {
		if (i>0) {
			strcat(s, ", ");
		}
		strcat(s, vc[i]);
		delete vc[i];
		vc[i]=NULL;
	}
	strcat(s,"]");
	s[tl+2]='\0';
	delete vc;

	return s;
}

#endif
