/**
 * Cadena_m descendiente de Objeto gen�rico.
 * Mutable.
 * @author Dominio p�blico. 2006. vtamara@pasosdeJesus.org
 **/ 

#ifndef CADENA_M_HPP
#define CADENA_M_HPP

#include "Objeto.hpp"

class Cadena_m {
	private:
		char *c;
	public:
		Cadena_m(char *s);
		~Cadena_m();
		void poner(char *s);
		void concatenar(char *s);
		char *valor();
		int longitud();
		Objeto *clonar();	
};
#endif
