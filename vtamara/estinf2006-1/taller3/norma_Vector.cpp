/**
 * Calcula norma a un Vector con 1000 3es
 * @author 2006. Dominio p�blico. vtamara@pasosdeJesus.org
 **/ 

#include <algorithm>
#include <cassert>
#include <iostream>
using namespace std;

#include "Vector.hpp"
#include "Entero.hpp"

Entero raizentera(Entero &x) 
{
	assert(x>=0);
	Entero r;
	for (r=0; r*r<x; r++) {}
	if (r*r==x) {
		return r;
	}
	return r-1;
}


Entero normaentera(Vector &v) 
{
	Entero s(0);
	for (int i=0; i<v.longitud(); i++) {
		int t=((Entero *)v.valor(i))->valor();
		s+=t*t;
	}
	return raizentera(s);
}

int main()
{
	int ner=0;
	Vector v;
	Entero tres(3);
	Entero n; //norma
	char *r;

	cout << "Comenzando" << "\n";
	for (int i=0; i<1000; i++) {
		v.agregar((Objeto *)&tres);
		r=v.aCadena();
		delete r;
	}
	n=normaentera(v);
	cout << "Norma n="<<n<<"\n";

	return ner;
}
