/**
 * Cadena_nm descendiente de Objeto gen�rico.
 * No mutable.
 * @author Dominio p�blico. 2006. vtamara@pasosdeJesus.org
 **/ 

#ifndef CADENA_NM_HPP
#define CADENA_NM_HPP

#include "Objeto.hpp"

class Cadena_nm {
	private:
		char *c;
	public:
		Cadena_nm(char *s);
		~Cadena_nm();
		char *valor();
		void poner(char *s);
		int longitud();
		Objeto *clonar();	
};
#endif
