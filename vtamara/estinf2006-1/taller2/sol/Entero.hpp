/**
 * Entero descendiente de Objeto gen�rico.
 * @author Dominio p�blico. 2006. vtamara@pasosdeJesus.org
 **/ 

#ifndef ENTERO_HPP
#define ENTERO_HPP

#include "Objeto.hpp"

class Entero: public Objeto {
	private:
		int e;
	public:
		Entero(int n=0);
		inline Entero(const Entero &x):e(x.e) {};
		~Entero() {};
		int valor();
		void poner(int n=0);
		Objeto *clonar();	
		char *aCadena();	
		Entero operator-();
		int operator()(float x);
		Entero &operator=(Entero f);
		friend Entero operator+(Entero);
};
#endif
