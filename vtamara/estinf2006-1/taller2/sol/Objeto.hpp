/**
 * Objeto gen�rico. Imitando Java.
 * @author Dominio p�blico. 2006. vtamara@pasosdeJesus.org
 **/ 

#ifndef OBJETO_HPP
#define OBJETO_HPP

class Objeto {
	public:

	/** Saca una copia del objeto */
	virtual Objeto *clonar()=0;	

	/** Retorna una cadena con una representaci�n presentable del objeto 
	 * como cadena.
	 * Localiza memoria para la nueva cadena con new.  Es responsabilidad 
	 * de la funci�n llamadora liberarla.
	 * Lanza una excepci�n si no logra localizar memoria.
	 */
	virtual char *aCadena()=0;	
};
#endif
