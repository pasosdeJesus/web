/**
 * Vector descendiente de Objeto gen�rico.
 * @author Dominio p�blico. 2006. vtamara@pasosdeJesus.org
 **/ 

#include <algorithm>
#include <cassert>
using namespace std;

#include "Vector.hpp"

Vector::Vector()
{
	v=NULL;
	l=0;
}

Vector::~Vector()
{
	if (v!=NULL) {
		delete[] v;
	}
}

Objeto *Vector::clonar() 
{
	Vector *n=new Vector();
	for (int i=0; i<l; i++) {
		n->agregar(v[i]);
	}
	n->l=l;

	return (Objeto *)n;
}

char *Vector::aCadena() 
{
	char **vc;
	char *s;
	int i,tl=0;
	vc=new (char *)[l];
	for (i=0; i<l; i++) {
		s=v[i]->aCadena();
		tl+=strlen(s)+2;
		vc[i]=s;
	}
	s=new char[tl+3];
	s[0]='[';s[1]='\0';
	for (i=0; i<l; i++) {
		if (i>0) {
			strcat(s, ", ");
		}
		strcat(s, vc[i]);
		delete vc[i];
		vc[i]=NULL;
	}
	strcat(s,"]");
	s[tl+2]='\0';
	delete vc;
	
	return s;
}


Objeto *Vector::valor(int i) 
{
	return v[i];
}

void Vector::poner(int i, Objeto *e)
{
	assert(i>=0);
	assert(i<l);
	v[i]=e;
}

void Vector::agregar(Objeto *e)
{
	Objeto **v2=new (Objeto *)[l+1];
	int i;
	for (i=0;i<l; i++) {
		v2[i]=v[i];
	}
	v2[i]=e;
	delete[] v;
	v=v2;
	l++;
}

int Vector::longitud()
{
	return l;
}

