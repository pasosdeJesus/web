/**
 * Entero descendiente de Objeto gen�rico.
 * @author Dominio p�blico. 2006. vtamara@pasosdeJesus.org
 **/ 

#include <algorithm>
#include <cassert>
#include <cmath>
using namespace std;

#include "Entero.hpp"

Entero::Entero(int n)
{
	e=n;
}
int Entero::valor()
{
	return e;
}
void Entero::poner(int n)
{
	e=n;
}
Objeto *Entero::clonar() 
{
	Entero *n=new Entero(e);
	return (Objeto *)n;
}
#include <iostream>
/**
 * Retorna represtaci�n del entero como cadena.
 * Si no puede localizar memoria lanza una excepci�n
 */
char *Entero::aCadena()
{
	double ld= e==0 ? 0 : log10((double)abs(e));
	int l=(int)(ld+1);
	char *s;
	l = (e<0) ? l+1 : l;
	s=new char[l+1];
	snprintf(s, l+1, "%d", e);
//	cout <<"OJO s es "<< s <<"\n";
	return s;
}

Entero Entero::operator-()
{
	return Entero(-e);
}
int Entero::operator()(float x)
{
	e+=(int)x;
	return e;
}
Entero &Entero::operator=(Entero f)
{
	e=f.e;
	return (*this);
}
