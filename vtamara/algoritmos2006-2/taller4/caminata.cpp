/** 
 * Estima probabilidad de encontrar soluci�n a (p_1 y p_2 y ... p_n) 
 * en menos de 2^n intentos dando una caminata aleatoria por el espacio
 * de Hamming {0,1}^n
 *
 * @author Vladimir T�mara Pati�o. 2006. Dominio p�blico. Sin garant�as.
 */

#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>
using namespace std;

//http://tautology.org/software/man/3/arc4random
#define foo4random() (arc4random() % ((unsigned)RAND_MAX + 1))

/** Envia un vector a un flujo de salida */
template <class T>
std::ostream &operator<<(std::ostream &os, vector<T> &v)
{
	os<<"[";
	string sep="";
	for(int i=0; i<v.size(); i++) {
		cout<<sep<<v[i];
		sep=",";
	}
	os<<"]";
	return os;
}


/** Recibe una asignaci�n y si es posible retorna true y la tranforma
 * en la siguiente */
bool siguiente_asig(vector<bool> &asig)
{
	int k=(int)((float)foo4random()*float(asig.size())/RAND_MAX);
	asig[k] = !asig[k];
	//cout<<asig<<endl;
	return true;
}

/** Dado n calcula 2^n */
long long pow2(int n) {
	return 1<<n;
}


int main() {
	int ner=0;

	const int minn=5;   // M�nimo n�mero de variables
	const int maxn=15;  // M�ximo n�mero de variables
	const int maxinten=50; // N�mero de intentos por cada valor de n
	vector<int> res(maxn); // res[i] es cantidad de exitos con i variables
	int prom=0;  // Porcentaje promedio de �xito

	srand(time(NULL));  // No necesario con arc4random 

	cout <<"Con una caminata aleatoria intentando encontrar asignaci�n \n"
		"que satisfaga la f�rmula booleana (p_1 y p_2 y ... p_n) \n"
		"partiendo de la asignaci�n 00..000 con valores de n entre "
		<<minn<<" y "<<maxn-1<<". \n"
		"Para cada valor de n se intentan "<<maxinten<<" veces."<<endl;
	for (int n=0; n<minn; n++) {
		res[n]=-1;
	}
	for (int n=minn; n<maxn; n++) {
		long long t=pow2(n); // Tama�o del espacio de b�squeda
		res[n]=0; 
		cout<<"n="<<setw(3)<<n<<" (tam: "<<setw(6)<<
			(unsigned long long)t<<"): ";
		for (int rep=0; rep<maxinten; rep++) {
			// Asignaci�n inicial aleatoria
			vector<bool> v(n); 
			for (int i=0;i<n; i++) {
				//v[i]=(float)foo4random()/(float)RAND_MAX<0.5 ? false: true;
				v[i]=false;
			}
			// Buscamos la asignaci�n 11..1
			long long ni=0; // N�mero de iteraciones
			bool sat=false;
			do {
				int nv=0;
				for (int i=0;i<n; i++) {
					if (v[i]) {
						nv++;
					}
				}
				if (nv==n) {
					sat=true;
				}
				ni++;
			} while (!sat && siguiente_asig(v) && ni<t);
			if (sat) {
				cout<<"!";
				res[n]++;
			}
			else {
				cout<<"";
			}
		}
		cout<<" "<<res[n]*100/maxinten<<"%"<<endl;
		prom+=res[n];
	}
	cout <<"Probabilidad de �xito promedio en 2^n intentos "<<
		prom*100/(maxinten*(maxn-minn+1))<<"%"<<endl;
	return 0;
}


