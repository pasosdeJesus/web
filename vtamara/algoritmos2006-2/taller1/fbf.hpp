/** Formulas bien formadas (fbf)
 *
 * Sintaxis concreta. Una cadena es una fbf si es
 * - una variable, es decir cadena iniciada con letra min�scula y que se compone
 *   solo de letras, n�meros y _
 * - -(f)  donde f es una fbf (negaci�n)
 * - (f)&(g) donde f y g son fbf (conjunci�n)
 * - (f)|(g) donde f y g son fbf (disyunci�n)
 *
 * Arbol de sintaxis abstracta.  Una fbf se representa en un �rbol
 * binario cuyos nodos tienen cadenas as�:
 * - una variable es un �rbol que tiene un s�lo nodo cuyo dato es la variable
 * - -(f) es un �rbol cuya ra�z tiene "-", cuyo sub�rbol izquierdo es el
 *   �rbol de f y cuyo sub�rbol derecho es vac�o.
 * - (f)&(g) es un �rbol cuya ra�z tiene "&", su sub�rbol izquierdo es el
 *   �rbol de f y su sub�rbol derecho es el �rbol de g
 * - (f)|(g) es an�logo al anterio pero la ra�z tiene "|"
 *
 * @author Vladimir T�mara Pati�o. 2006. Dominio p�blico. Sin garant�as.
 */

#ifndef FBF_HPP
#define FBF_HPP

#include <iostream>
#include <sstream>
#include <map>
#include "arbin.hpp"
#include "fbf.hpp"
using namespace std;

/** Operador << para listas */
template <class T>
std::ostream &operator<<(std::ostream &os, list<T> &l)
{
	typename list<T>::iterator i;
	os<<"[";
	for(i=l.begin(); i!=l.end(); i++) {
		if (i!=l.begin()) {
			os<<", ";
		}
		os<<*i;
	}
	os<<"]";
	return os;
}

/** Operador << para maps */
template <class T1, class T2>
std::ostream &operator<<(std::ostream &os, map<T1,T2> &m)
{
	typename map<T1,T2>::iterator i;
	os<<"<";
	for(i=m.begin(); i!=m.end(); i++) {
		if (i!=m.begin()) {
			os<<", ";
		}
		os<<"("<<i->first<<", "<<i->second<<")";
	}
	os<<">";
	return os;
}



/** Retorna par�ntesis que cierra al que se abre en la posici�n ini --y
 * que se espera que est� antes de la posici�n fin
 */
int 
par_cierra(string fbf, int ini, int fin);


/** Retorna �rbol de sintaxis correspondiente a una fbf
 *  ubicada desde la posici�n hasta antes de fin de la cadena fbf.  
 *  Son formulas bien formadas:
 *  - p,q y dem�s variables iniciadas por letra min�scula
 *  - -(f) negaci�n de la fbf f
 *  - (f)&(g) conjunci�n de las fbfs f y g
 *  - (f)|(g) disyunci�n de las fbfs f y g
 **/
arbin<string> 
string_a_fbf(string fbf, int ini, int fin);


/** Saca mapeo de variables inicializando cada una en false */
void 
saca_variables(nodo_arbin<string> *fbf, map<string, bool> &asig);

bool
evaluar(nodo_arbin<string> *fbf, map<string, bool> asig);

#endif
