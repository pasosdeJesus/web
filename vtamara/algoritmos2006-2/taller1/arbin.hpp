/** Clase Arbol Binario 
 * @author Vladimir T�mara Pati�o. 2006. Dominio p�blico. Sin garant�as.
 */

#ifndef ARBIN_HPP
#define ARBIN_HPP

#include <iostream>
#include <list>
#include <cassert>
using namespace std;

/** nodo_arbin<T> es un nodo de un �rbol binario cuyos nodos tiene un dato
 * de tipo T.
 * Manejo de memoria.  Esta clase localiza sub�rboles izquierdo y derecho 
 * y por tanto los borra.  Las constructoras que reciben sub�rboles y la de
 * copia sacan copia de los �rboles recibidos.
 **/
template <class T>
class nodo_arbin {
	nodo_arbin *izq;
	nodo_arbin *der;
	T dato;

	public:

	nodo_arbin() {
		//cerr<<"**nodo_arbin::nodo_arbin()"<<endl;
		izq=NULL;
		der=NULL;
	}

	/** Constructora que inicializa dato del nodo con d */
	nodo_arbin(const T &n) {
		//cerr<<"**nod_arbin::nodo_arbin(n)"<<endl;
		izq=NULL;
		der=NULL;
		dato=n;
	}

	nodo_arbin(const T &n, const nodo_arbin<T> &i) {
		//cerr<<"**nodo_arbin(n,i)"<<endl;
		dato=n;
		izq=new nodo_arbin(i);
		der=NULL;
	}

	nodo_arbin(const T &n, const nodo_arbin<T> &i, const nodo_arbin<T> &d) {
		//cerr<<"**nodo_arbin::nodo_arbin(n,i,d)"<<endl;
		dato=n;
		izq=new nodo_arbin(i);
		der=new nodo_arbin(d);
	}

	/** Constructora de copia */
	nodo_arbin(const nodo_arbin<T> &r) {
		//cerr<<"**nodo_arbin::nodo_arbin(r)"<<endl;
		dato=r.dato;
		izq=(r.izq==NULL) ? NULL : new nodo_arbin(*(r.izq));
		der=(r.der==NULL) ? NULL : new nodo_arbin(*(r.der));
	}


	~nodo_arbin() {
		//cerr<<"**nodo_arbin::~nodo_arbin()"<<endl;
		if (izq!=NULL) {
			delete izq;
		}
		if (der!=NULL) {
			delete der;
		}
/*		if (dato!=NULL) {
			delete dato;
		} */
	}

	/** Asignaci�n */
	nodo_arbin<T> &operator=(const nodo_arbin &f) {
		//cerr<<"**nodo_arbin::operator="<<endl;
		dato=f.dato;
		if (izq!=NULL) {
			delete izq;
		}
		izq=new nodo_arbin(f.izq);
		if (der!=NULL) {
			delete der;
		}
		der=new nodo_arbin(f.der);
		return *this;
	}

	int altura() {
		//cerr<<"**nodo_arbin::altura()"<<endl;
		int t1=izq==NULL ? 0 : izq->altura();
		int t2=der==NULL ? 0 : der->altura();
		return 1+max(t1,t2);
	}

	T valor() {
		//cerr<<"**nodo_arbin::valor()"<<endl;
		return dato;
	}

	nodo_arbin<T> *sub_izq() {
		//cerr<<"**nodo_arbin::sub_izq()"<<endl;
		return izq;
	}

	nodo_arbin<T> *sub_der() {
		//cerr<<"**nodo_arbin::sub_der"<<endl;
		return der;
	}
	
	bool es_hoja() {
		//cerr<<"**nodo_arbin::es_hoja"<<endl;
		return izq==NULL && der==NULL;
	}

	void pone_dato(const T &v) {
		//cerr<<"**nodo_arbin::pone_dato"<<endl;
		dato=v;
	}

	void pone_izq(const nodo_arbin &n) {
		//cerr<<"**nodo_arbin::pone_izq"<<endl;
		if (izq!=NULL) {
			delete izq;
		}
		izq=new nodo_arbin(n);
	}

	void pone_der(const nodo_arbin &n) {
		//cerr<<"**nodo_arbin::pone_der"<<endl;
		if (der!=NULL) {
			delete der;
		}
		der=new nodo_arbin(n);
	}

	/** Inserta un dato de forma ordenada, requiere que en T est�
	 * sobrecargado el operador < 
	 **/
	void inserta_ord(const T &elem) {
		//cerr<<"**nodo_arbin::inserta_ord"<<endl;
		if (elem < dato) {
			if (izq==NULL) {
				izq=new nodo_arbin(elem);
			}
			else {
				izq->inserta_ord(elem);
			}
		}
		else {
			if (der==NULL) {
				der=new nodo_arbin(elem);
			}
			else {
				der->inserta_ord(elem);
			}
		}
	}

	void preorden(list<T> &l) {
		//cerr <<"**nodo_arbin::preorden(l)"<<endl;
		l.push_back(dato);
		if (izq!=NULL) {
			izq->preorden(l);
		}
		if (der!=NULL) {
			der->preorden(l);
		}
	}

	std::ostream &outs(std::ostream &os) {
		//cerr<<"**nodo_arbin:outs"<<endl;
		os<<dato<<", <";
		if (izq!=NULL) {
			izq->outs(os);
		}
		os<<">, <";
		if (der!=NULL) {
			der->outs(os);
		}
		os<<">";
		return os;
	}


	void inorden(list<T> &l) {
		//cerr<<"**nodo_arbin::inorden"<<endl;
		if (izq!=NULL) {
			izq->inorden(l);
		}
		l.push_back(dato);
		if (der!=NULL) {
			der->inorden(l);
		}
	}

	void posorden(list<T> &l) {
		//cerr<<"**nodo_arbin::posorden()"<<endl;
		if (izq!=NULL) {
			izq->posorden(l);
		}
		if (der!=NULL) {
			der->posorden(l);
		}
		l.push_back(dato);
	}

	void nivel(list<T> &l, int nivel) {
		//cerr<<"**nodo_arbin::nivel()"<<endl;
		if (nivel==0) {
			l.push_back(dato);
		}
		else {
			assert(nivel>0);
			if (izq!=NULL) {
				izq->nivel(l, nivel-1);
			}
			if (der!=NULL) {
				der->nivel(l, nivel-1);
			}
		}
	}


};

/** arbin<T> es un �rbol binario cuyos nodos son nodo_arbin y tienen un
 * dato tipo T.   En realidad no es indispensable, la ventaja es que puede
 * implementarse el m�todo es_vacio
 **/
template <class T>
class arbin {
	nodo_arbin<T> *raiz;

	public:
	/** Constrye �rbol vac�o */
	arbin() 
	{
		//cerr<<"**arbin:arbin()"<<endl;
		raiz=NULL;
	}

	arbin(const T &n) 
	{
		//cerr<<"**arbin:arbin(n)"<<endl;
		raiz=new nodo_arbin<T>(n);
	}

	arbin(const T &n, const arbin<T> &i) 
	{
		//cerr<<"**arbin:arbin(n,i)"<<endl;
		raiz=new nodo_arbin<T>(n, *(i.raiz));
	}

	arbin(const T &n, const arbin<T> &i, const arbin<T> &d) 
	{
		//cerr<<"**arbin:arbin(n,i,d)"<<endl;
		raiz=new nodo_arbin<T>(n, *(i.raiz), *(d.raiz));
	}

	arbin(const arbin<T> &f)
	{
		//cerr<<"**arbin:arbin(&f)"<<endl;
		if (f.raiz==NULL) {
			raiz=NULL;
		}
		else {
			raiz=new nodo_arbin<T>(*f.raiz);
		}
	}

	~arbin() {
		//cerr<<"**arbin:~arbin()"<<endl;
		if (raiz!=NULL) {
			delete raiz;
		}
	}

	// Operador de asignaci�n
	arbin<T> &operator=(const arbin &f) 
	{
		//cerr<<"**arbin:operator==()"<<endl;
		if (raiz==NULL) {
			delete raiz;
		}
		raiz=NULL;
		if (f.raiz!=NULL) {
			raiz=new nodo_arbin<T>(*(f.raiz));
		}
		return *this;
	}

	bool es_vacio() {
		return raiz==NULL;
	}

	int altura() {
		if (raiz==NULL) {
			return 0;
		}
		return raiz->altura();
	}

	nodo_arbin<T> *nodo_raiz() {
		return raiz;
	}

	/** Pone el dato de la ra�z en un valor */
	int pone_raiz(const T &v) {
		assert(raiz!=NULL);
		raiz->pone_dato(v);
	}


	/** Pone un �rbol a como sub�rbol izquierdo  (si hab�a uno lo elimina 
	 * primero) */
	void pone_izq(const arbin<T> &a) {
		assert(raiz!=NULL);
		raiz->pone_izq(*(a.raiz));
	}

	/** Poner �rbol a como sub�rbol derecho  (si hab�a uno lo elimina 
	 * primero) */
	void pone_der(const arbin<T> &a) {
		assert(raiz!=NULL);
		raiz->pone_der(*(a.raiz));
	}


	int inserta_ord(const T &elem) {
		if (raiz==NULL) {
			raiz = new nodo_arbin<T>(elem) ;
		}
		else {
			raiz->inserta_ord(elem);
		}
	}

	std::ostream &outs(std::ostream &os) {
		//cerr<<"**arbin:outs"<<endl;
		os<<"<";
		if (raiz!=NULL) {
			raiz->outs(os);
		}
		os<<">";
		return os;
	}

	list<T> preorden() {
		//cerr<<"**arbin:preorden()"<<endl;
		list<T> l;
		if (raiz!=NULL) {
			raiz->preorden(l);
		}
		return l;
	}


	list<T> inorden() {
		list<T> l;
		if (raiz!=NULL) {
			raiz->inorden(l);
		}
		return l;
	}

	list<T> posorden() {
		list<T> l;
		if (raiz!=NULL) {
			raiz->posorden(l);
		}
		return l;
	}

	list<T> niveles() {
		list<T> l;
		if (raiz!=NULL) {
			int a=raiz->altura();
			int n;
			for (n=0; n<a; n++) {
				raiz->nivel(l, n);
			}
		}
		return l;
	}

};

template <class T>
std::ostream &operator<<(std::ostream &os, arbin<T> &l)
{
	return l.outs(os);
}

#endif
