/** Pruebas de regresi�n a formulas bien formadas
 * @author Vladimir T�mara Pati�o. 2006. Dominio p�blico. Sin garant�as.
 */

#include <iostream>
#include <sstream>
#include <map>
#include "arbin.hpp"
#include "fbf.hpp"
using namespace std;


template <class T>
string list2string(list<T> l) {
	string r="";
	typename list<T>::iterator i;
	r+="[";
	for(i=l.begin(); i!=l.end(); i++) {
		if (i!=l.begin()) {
			r+=", ";
		}
		ostringstream ss;
		ss<<*i<<flush;
		r+=ss.str();
	}
	r+="]";
	return r;
} 

int main() {
	int ner=0;
	cout << "Comenzando" << endl;
	cout << " Probando reconocimiento de FBFs" << endl;
	arbin<string> a=string_a_fbf("x",0,1);
	string sp=list2string(a.preorden());
	if (sp!="[x]") {
		cerr << " * Preorden de primero no deber�a ser " 
			<< sp <<endl;
		ner++;
	}
	a=string_a_fbf("-(x)",0,4);
	sp=list2string(a.preorden());
	if (sp!="[-, x]") {
		cerr << " * Preorden de 2 no deber�a ser " 
			<< sp <<endl;
		ner++;
	}
	a=string_a_fbf("(x)&(y)",0,7);
	list<string> p=a.preorden();
	sp=list2string(p);
	if (sp!="[&, x, y]") {
		cerr << " * Preorden de 3 no deber�a ser " 
			<< sp <<endl;
		ner++;
	}
	ostringstream ss;
	ss<<a;
	if ((sp=ss.str())!="<&, <x, <>, <>>, <y, <>, <>>>") {
		cerr << " * Rep. de �rbol no deber�a ser "<< sp <<endl;
		ner++;
	}

	cout << " Probando extracci�n de variables y evaluaci�n" << endl;
	map<string, bool> v;
	saca_variables(a.nodo_raiz(), v);
	ostringstream ss2;
	ss2<<v;
	if ((sp=ss2.str())!="<(x, 0), (y, 0)>") {
		cerr << " * Rep. de asignaci�n no deber�a ser "<< sp <<endl;
		ner++;
	}

	if (evaluar(a.nodo_raiz(), v)!=false) {
		cerr << " * 1ra val de (x)&(y) deber�a ser false" <<endl;
		ner++;
	}
	a=string_a_fbf("(u)|(-(v))",0,10);
	sp=list2string(a.preorden());
	if (sp!="[|, u, -, v]") {
		cerr << " * Preorden de 4 no deber�a ser " 
			<< sp <<endl;
		ner++;
	}
	v.erase(v.begin(), v.end());
	saca_variables(a.nodo_raiz(), v);
	if (evaluar(a.nodo_raiz(), v)!=true) {
		cerr << " * 1ra val de (u)|(-v) deber�a ser true" <<endl;
		ner++;
	}
	v["v"]=true;
	if (evaluar(a.nodo_raiz(), v)!=false) {
		cerr << " * 2da val de (u)|(-v) deber�a ser false" <<endl;
		ner++;
	}
	v["v"]=false;
	v["u"]=true;
	if (evaluar(a.nodo_raiz(), v)!=true) {
		cerr << " * 3aa val de (u)|(-v) deber�a ser true" <<endl;
		ner++;
	}
	v["v"]=true;
	v["u"]=true;
	if (evaluar(a.nodo_raiz(), v)!=true) {
		cerr << " * 4ta val de (u)|(-v) deber�a ser false" <<endl;
		ner++;
	}

	cout <<"N�mero de errores: "<<ner<<endl;

	return ner?1:0;
}


