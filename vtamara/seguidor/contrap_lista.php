<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 foldmethod=marker: */

/**
* Seguidor de solicitudes y fallas.
* Dominio p�blico. Sin garant�as. 2004. 
* @author Vladimir T�mara. vtamara@users.sourceforge.net
*
* Cr�ditos: http://www.sf.net/projects/sivel
*/

if (session_id()=="") {
	session_start();
}
require_once 'misc.php';
require_once 'DBM2.php';
require_once 'DB/DataObject.php';
require_once 'Contraprestacion.php';
require_once 'HTML/Form.php';
require_once 'Contraprestacion.php';
include 'aut/conf.php';


error_reporting(E_ALL);

$aut_usuario="";
$dbh = autenticaUsuario($dsn, $accno, $aut_usuario);

$tabla="contraprestacion";

echo "<html><head><title>Contraprestaciones</title></head>";
echo "<body><center><h1>Contraprestaciones</h1></center>";


$d = DB_DataObject::factory($tabla);
if (PEAR::isError($d)) {
    die($d->getMessage());
}

$k=$d->keys();

$titulo=$_DB_DATAOBJECT_FORMBUILDER['CONFIG']['select_display_field'];

if (isset($d->fb_select_display_field)) {
    $titulo=$d->fb_select_display_field;
}
$d->find();

echo "<table border='1' width='100%'>";
echo "<tr><th>De</th><th>Para</th><th>Por</th><th>Tiempo</th></tr>";
while ($d->fetch()) {
    $vd=get_object_vars($d);
    $t=$vd[$titulo];

    $pk="";
    if (is_array($k)) {
        $sep="";
        foreach ($k as $nl) {
            $pk.=$sep.$nl."=".($d->$nl);
            $sep=",";
        }
    }
    echo sprintf('<tr><td>%s</td><td>%s</td><td>%s <a href="contrap_detalle.php?id=%s">%s</a></td><td>%s</td>',
        $d->da,
        $d->recibe,
        $d->fecha,
        $pk,
        $t,
        $d->horas);
}
echo "</table>";

echo '<hr>';  #<a href="contrap_detalle.php">Nuevo</a>';
navega('Nueva contraprestaci�n', 'contrap_detalle.php', $aut_usuario);
echo '</body></table>';

?> 
