<?php
/**
 * Table Definition for public.solicitud
 */
require_once 'DB/DataObject.php';

class DataObjects_Public_solicitud extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'public.solicitud';                // table name
    public $codigo;                          // int4(4)  
    public $cod_categoria;                   // int4(4)  
    public $abierto;                         // bool(1)  
    public $cod_responde;                    // varchar(-1)  
    public $cod_enviado;                     // varchar(-1)  
    public $correo_invitado;                 // varchar(-1)  
    public $dependencia;                     // int4(4)  
    public $prioridad;                       // int4(4)  
    public $apertura;                        // date(4)  
    public $cierre;                          // date(4)  
    public $horas;                           // int4(4)  
    public $desc_corta;                      // varchar(-1)  
    public $desc_detallada;                  // varchar(-1)  
    public $repetir;                         // varchar(-1)  
    public $solucion;                        // varchar(-1)  
    public $porcentaje;                      // int4(4)  
    public $subsolicitud_de			//int4(4)

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Public_solicitud',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
