
-- Llena tablas con datos de ejemplo 
-- Dominio público . Sin garantías. 2004


INSERT INTO categoria VALUES (1,'Software','',null);
INSERT INTO categoria VALUES (101,'Consecución de software','',1);
INSERT INTO categoria VALUES (102,'Instalación de software nuevo','',1);
INSERT INTO categoria VALUES (103,'Remplazo de software existente','',1);
INSERT INTO categoria VALUES (104,'Actualización de software','',1);
INSERT INTO categoria VALUES (105,'Soporte en uso de software','',1);
INSERT INTO categoria VALUES (106,'Capacitación en uso de software','',1);
INSERT INTO categoria VALUES (107,'Reportar falla en programa','',1);
INSERT INTO categoria VALUES (110,'Desarrollo','',1);
INSERT INTO categoria VALUES (1100,'Requerimientos','',110);
INSERT INTO categoria VALUES (1101,'Diseño','',110);
INSERT INTO categoria VALUES (1102,'Implementación','',110);
INSERT INTO categoria VALUES (1103,'Pruebas','',110);
INSERT INTO categoria VALUES (1104,'Aseguramiento de calidad','',110);
INSERT INTO categoria VALUES (11040,'Seguridad','',1104);

INSERT INTO categoria VALUES (2,'Seguridad','',null);
INSERT INTO categoria VALUES (201,'Chequeo de claves','',2);
INSERT INTO categoria VALUES (202,'Reportar falla de seguridad','',2);


INSERT INTO categoria VALUES (3,'Hardware','',null);
INSERT INTO categoria VALUES (301,'Instalación/actualización de hardware nuevo','',3);
INSERT INTO categoria VALUES (302,'Reportar falla de hardware','',3);


INSERT INTO responde (id, nombre, clave) VALUES ('invitado','Usuario invitado',
	'ac037c61854d527f164f8e9d23df1918');

INSERT INTO dependencia VALUES (1,'Organización 1',
	'La organización 1',null,null,'invitado',null);


INSERT INTO solicitud (codigo, cod_categoria,abierto,cod_responde,cod_enviado,
	correo_invitado, dependencia,prioridad,apertura,cierre,desc_corta,
	desc_detallada,
	repetir,solucion,porcentaje) VALUES (1, 101,true,'invitado',
	'invitado','', 1,4, '2006-2-2',null,
	'Adquirir versión oficial de OpenBSD 3.9', 
	'Por liberar el 2006-4-1.',
	null,null,0);

INSERT INTO solicitud (codigo, cod_categoria,abierto,cod_responde,cod_enviado,
	correo_invitado, dependencia,prioridad,apertura,cierre,
	desc_corta, 
	desc_detallada, 
	repetir,solucion,porcentaje) 
	VALUES (2, 104,true,'invitado', 'invitado', 
	'invitado@dominio.org', 1, 4, '2006-2-2', null,
	'Instalar OpenBSD 3.8', 
	'',
	null,null,0);

INSERT INTO prerequisito (cod_sol, cod_solpre) 
	VALUES (2,1)
;


INSERT INTO comentario (codigo, cod_solicitud, respuesta_a,
	fecha, cod_enviado, correo_invitado, 
	desc_corta, 
	comentario,
	tiempo_invertido) 
	VALUES ('1', '1', NULL,
	'2006-2-2', 'invitado', '',  
	'Es mejor comprar CD oficial',
	'Puede verse como en http://openbsd.org.',
	'10');

