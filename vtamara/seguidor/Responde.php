
<?php
# vim: set expandtab tabstop=4 shiftwidth=4 foldmethod=marker:

# Dominio publico.  2004. Sin garantias. 

require_once 'DB/DataObject.php';

class DataObjects_Responde extends DB_DataObject
{
	var $__table = 'responde';  // table name
	var $id;                   
	var $nombre;              
	var $direccion;          
	var $telefono;          
	var $correo;           
	var $homepage;        
	var $clave;         

	/* ZE2 compatibility trick*/
	function __clone() { return $this;}

	/* Static get */
	function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Contraprestacion',$k,$v); }

//	var $fb_linkDisplayFields = array('nombre');
	var $fb_fieldLabels= array('id' => 'Identificaci�n',
		'nombre' =>     'Nombre',
		'direccion' =>  'Direccion',
		'telefono' =>   'Tel�fono',
		'correo' =>     'Correo',
		'homepage' =>   'Homepage',
		'clave' =>      'Clave'
	);
    var $fb_hidePrimaryKey = true;


}

?>


