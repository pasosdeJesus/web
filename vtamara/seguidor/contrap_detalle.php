<?php
# vim: set expandtab tabstop=4 shiftwidth=4 foldmethod=marker:

# Permite editar o agregar contraprestaci�n
# Dominio publico.  2005. Sin garantias.
# @author: Vladimir T�mara Pati�o. vtamara@pasosdeJesus.org
# Referencias: http://www.sf.net/projects/sivel

if (session_id()=="") {
	session_start();
}
require_once "misc.php";
require_once "aut/conf.php";

error_reporting(E_ALL);

$aut_usuario="";
$dbh = autenticaUsuario($dsn, $accno, $aut_usuario);

$tabla='contraprestacion';

$d = DB_DataObject::factory($tabla);
if (PEAR::isError($d)) {
    die($d->getMessage());
}
if (isset($_GET['id'])) {
    $a=explode(',', $_GET['id']);
    foreach($a as $v) {
        $r=explode('=',$v);
        $n=$r[0];
        $d->$n=$r[1];
    } 
    if ($d->find()!=1) {
            die("Se esperaba un s�lo registro");
    }
    $d->fetch();
}

$b =& DB_DataObject_FormBuilder::create($d);
$b->createSubmit=0;
$f = $b->getForm($_SERVER['REQUEST_URI']);
//$f->setRequiredNote($mreq);
$ed=array();
if (!isset($_GET['id'])) {
    $f->setDefaults(array('da' => $aut_usuario));
    $e =& $f->createElement('submit', 'a�adir', 'A�adir');
    $ed[] =& $e;
}
else {
    $e =& $f->createElement('submit', 'actualizar', 'Actualizar');
    $ed[] =& $e;
    $e =& $f->createElement('submit', 'eliminar', 'Eliminar');
    $ed[] =& $e;
}
$f->addGroup($ed, null, '', '&nbsp;', false);


if ($f->validate()) {
    if (isset($f->_submitValues['actualizar']) || 
    isset($f->_submitValues['a�adir']) ) {
        $res=$f->process(array($b,'processForm'), false);
    }
    else {
        foreach($f->_submitValues as $k => $v) {
            $d->$k=$v;
        }
        $res=& $d->delete();
    }
    if ($res) {
        header('Location: contrap_lista.php');
    }
    if (PEAR::isError($d->_lastError)) {
        echo $d->_lastError->getMessage()."<br>";
        echo $d->_lastError->userinfo."<br>";
    }
}

echo $f->toHtml();

?>
