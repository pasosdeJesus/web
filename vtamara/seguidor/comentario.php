<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 foldmethod=marker: */

/**
* Seguidor de solicitudes y fallas.
* Dominio p�blico. 2004. 
* @author Vladimir T�mara. vtamara@users.sourceforge.net

* Cr�ditos: 
* Se ha empleado porciones cortas de c�digo y documentaci�n disponible en:
*   http://us2.php.net/manual/es/function.pg-connect.php
*   Documentaci�n y ejemplos de DB Pear.
*   http://structio.sourceforge.net/sigue

* Formulario para ver, editar o agregar comentarios.

**/



if (session_id()=="") {
	session_start();
}

require_once 'MDB2.php';
require_once 'HTML/Form.php';
require_once 'Mail.php';
include('aut/conf.php');
require_once 'misc.php';

error_reporting(E_ALL);

$ocorreo=Mail::factory('smtp',array('host' => 'localhost'));

$codigo=isset($_REQUEST['codigo']) ? $_REQUEST['codigo'] : "";
$cod_solicitud=isset($_REQUEST['cod_solicitud']) ? $_REQUEST['cod_solicitud'] : "";
$respuesta_a=isset($_REQUEST['respuesta_a']) ? $_REQUEST['respuesta_a'] : "";

//$receptor_correo=$_REQUEST['cod_responde'];  Reciben por lo menos 
// responsable y enviador de solicitud
if ($respuesta_a!='') {
    $q="SELECT correo FROM responde, solicitud WHERE responde.id=solicitud.cod_responde AND solicitud.id= '".$dbh->quote($respuesta_a)."';";
	$sth=$dbh->query($q);
	if (PEAR::isError($sth) || $sth==NULL) {
		die($sth->getMessage());
	}
	$row=array();
	if (!$sth->fetchInto($row)) {
		die("Problema buscando correo de responable de solicitud");
	}
    if ($row[0]!=$aut_user) {
	    $receptor_correo=$row[0];
    }

	 $q="SELECT correo FROM responde, solicitud WHERE responde.id=solicitud.cod_enviado AND solicitud.id=$respuesta_a;";
	$sth=$dbh->query($q);
	if (PEAR::isError($sth) || $sth==NULL) {
		die($sth->getMessage());
	}
	$row=array();
	if (!$sth->fetchInto($row)) {
		die("Problema buscando correo de responable de solicitud");
	}

    if ($row[0]!=$aut_usuario)  {
	    $receptor_correo.=",".$row[0];
    }

}
else {
    $receptor_correo='';
}

$aut_usuario="";
$dbh = autenticaUsuario($dsn, $accno, $aut_usuario);

if ($cod_solicitud=='') {
    die("Este formulario no se emplea de forma aislada");
}

$ncol=array("codigo","cod_solicitud","respuesta_a","fecha",
"cod_enviado","correo_invitado","desc_corta","comentario",
"tiempo_invertido");

$tit="";
$boton_sub='Actualizar';
if (isset($_REQUEST['submit']) && $_REQUEST['submit']=='A�adir') {

	// Crear nuevo registro, escogiendo c�digo y haciendo INSERT
	$q="SELECT MAX(codigo) FROM comentario;";
	$sth=$dbh->query($q);
	if (PEAR::isError($sth) || $sth==NULL) {
		die($sth->getMessage());
	}
	$row=array();
	if (!$sth->fetchInto($row)) {
		die("Problema escogiendo nuevo c�digo");
	}
	$codb=((int)$row[0])+1;
	$q="INSERT INTO comentario (";
	$values="";
	$sep="";
	foreach($ncol as $f) {
		$r=!isset($_REQUEST[$f]) || $_REQUEST[$f]=='' ? 'NULL' 
			: $dbh->quote($_REQUEST[$f]);
		$q.="$sep $f";
		if ($f=="codigo") {
			$values.=$sep." ".$dbh->quote($codb);
		}
		else if ($f=="cod_solicitud") {
			$values.="$sep ".$dbh->quote($cod_solicitud);
		}
		else if ($f=="respuesta_a") {
			if ($respuesta_a=='') {
				$values.="$sep NULL";
			}
			else {
				$values.="$sep ".$dbh->quote($respuesta_a);
			}
		}
		else if ($f=="cod_enviado") {
			$values.="$sep ".$dbh->quote($aut_usuario);
		}
		else if ($f=="correo_invitado") {
			$values.="$sep ".$dbh->quote(isset($_SESSION['correo_invitado'])?
                $_SESSION['correo_invitado']:
                '');
		}
		else if ($f=="fecha") {
			$values.="$sep ".$dbh->quote(date("j-M-Y"));
		}
		else {
			$values.="$sep $r";
		}
		$sep=",";
	}
	$q.=") VALUES (".$values.");";
	$sth=$dbh->query($q);
	If (PEAR::isError($sth) || $sth==NULL) {
		die($sth->getMessage());
	}

	$tit="Comentario a�adido. ";

}
if (isset($_REQUEST['submit']) && $_REQUEST['submit']=='Actualizar') {
    if ($aut_usuario==$nom_invitado) {
        die("El usuario invitado no puede actualizar informaci�n");
    }        
	// Actualizar registro existente con UPDATE
	$q="UPDATE comentario SET ";
	$sep="";
	foreach($ncol as $v) {
		if ($v!='codigo' && $v!='respuesta_a' && $v!='cod_enviado' 
			&& $v!='correo_invitado' && $v!='fecha') {
			$r=$_REQUEST[$v]=='' ? 'NULL' : $dbh->quote($_REQUEST[$v]);
			$q.="$sep $v=".$r;
			$sep=",";
		}
	}
	$q.=" WHERE codigo=".$dbh->quote($codigo).";";  
	$sth=$dbh->query($q);
	if (PEAR::isError($sth) || $sth==NULL) {
		die($sth->getMessage());
	}

	$tit="Comentario actualizado. ";
}

if (isset($_REQUEST['submit']) && $_REQUEST['submit']=='Eliminar') {

    if ($aut_usuario==$nom_invitado) {
        die("El usuario invitado no puede eliminar informaci�n");
    }
    $q="DELETE FROM comentario";
    $q.=" WHERE codigo=".$dbh->quote($codigo).";";
    $sth=$dbh->query($q); 
    if (PEAR::isError($sth) || $sth==NULL) {
        die($sth->getMessage());
    }
    $tit="Comentario eliminado. ";
    echo "<html><head><title>$tit</title></head>\n";
    echo "<body><h1>$tit</h1>\n";
    echo "<hr>";


}
else {
    if ($codigo!='') {
        // Ver datos asociados a un c�digo
        $tit.="Examinando comentario.";
        $q="SELECT ".implode(',',array_values($ncol)).
        " FROM comentario WHERE codigo='$codigo';";  

        $sth=$dbh->query($q);
        if (PEAR::isError($sth) || $sth==NULL) {
            die($sth->getMessage());
        }
        $row = array();
        if (!$sth->fetchInto($row)) {
            die("C�digo desconocido");
        }
        $campo=array();
        foreach ($ncol as $k => $v) {
            $campo[$v]=$row[$k];
        }
    }
    else {
        $tit.="Nuevo comentario.";
        // Formulario en blanco listo para a�adir
        foreach ($ncol as $k => $v) {
            $campo[$v]="";
        }
        $campo['cod_solicitud']=$cod_solicitud;
        $campo['fecha']=date("j-M-Y");
        $campo['cod_enviado']=$aut_usuario;
        $campo['correo_invitado']=isset($_SESSION['correo_invitado'])?
        $_SESSION['correo_invitado']:
        '';
        $boton_sub='A�adir';
    }

    $form=new HTML_Form($_SERVER['PHP_SELF']);

    echo "<html><head><title>$tit</title></head>\n";
    echo "<body><h1>$tit</h1>\n";
    echo "<hr>";

    $form->addHidden("codigo", $campo['codigo']);
    $form->addHidden("cod_solicitud", $campo['cod_solicitud']);
    $form->addHidden("respuesta_a", $campo['respuesta_a']);

    $responde=$dbh->getAssoc("SELECT id,nombre FROM responde".
    " ORDER BY nombre;");
    $form->addPlaintext("Enviado por", $responde[$campo['cod_enviado']]);

    $form->addText("desc_corta","Descripci�n corta",
    $campo['desc_corta'],60,60);

    $form->addPlaintext("Fecha", $campo['fecha']);
    $form->addHidden("fecha", $campo['fecha']);


    $form->addTextarea("comentario","Comentario",
    $campo['comentario'], 60,3, 1000);

    $form->addText("tiempo_invertido","Tiempo invertido (horas)", 
    $campo['tiempo_invertido'],4,4);


    $form->addSubmit("submit", $boton_sub); 
    if ($boton_sub=="Actualizar") {
        $form->addSubmit('submit', 'Eliminar');
    }

    $form->display(); 
}

$dbh->disconnect();

if ($ocorreo!=NULL && $GLOBALS['CORREO'] && $receptor_correo!='' ) {
    $rec=correo_us($dbh, $receptor_correo);
    $headers['To'] = $rec;
    $headers['Subject'] = "Seguidor. Comentario. $tit. $tema_correo";
    $headers['From'] = correo_us($dbh, $emisor_correo);

    $cuerpo_correo="";
    foreach($_REQUEST as $k => $v) {
        if ($k == 'codigo') {
            $cuerpo_correo.=$k.": ".$codb."\n";
        }
        else if ($k != 'submit' && $k != '_fields'
                && $k != 'PHPSESSID') {
            $cuerpo_correo.=$k.": ".$v."\n";
        }
    }
    echo "rec=$rec, headers=$headers, cuerpo_correo=$cuerpo_correo";
    $ocorreo->send($rec, $headers, $cuerpo_correo);
}



echo "<hr>";
navega('', '', $aut_usuario);
echo "</body></html>";
?> 
