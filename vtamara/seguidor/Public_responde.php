<?php
/**
 * Table Definition for public.responde
 */
require_once 'DB/DataObject.php';

class DataObjects_Public_responde extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'public.responde';                 // table name
    public $id;                              // varchar(-1)  
    public $nombre;                          // varchar(-1)  
    public $direccion;                       // varchar(-1)  
    public $telefono;                        // varchar(-1)  
    public $correo;                          // varchar(-1)  
    public $homepage;                        // varchar(-1)  
    public $clave;                           // varchar(-1)  
    public $habilitado;                      // bool(1)  

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Public_responde',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
