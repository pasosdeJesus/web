#!/bin/sh

# Dominio p�blico. 2004. Sin garant�as. vtamara@informatik.uni-kl.de
# Con base en script disponible en http://structio.sourceforge.net/repasa

# The command line management of this script is based on public
# domain code of WWWeb Tide Team 
#    http://www.ebbtide.com/Util/ksh_parse.html 

# Reading configuration variables
if (test ! -f confv.sh) then {
        cp confv.empty confv.sh
} fi;
. ./confv.sh


# Reading functions to help in configuration
. herram/confaux.sh

#Parsing options

BASENAME=$(basename $0)
USAGE="$BASENAME [-v] [-h] [-M] [-p prefijo]"
# Remember: if you add a switch above, don't forget to add it to: 
#	1) "Parse command line options" section 
#	2) "MANPAGE_HEREDOC" section
#	3) "check for help" section
ARG_COUNT=0 	# This nubmer must reflect true argument count
OPT_FLAG=0 	# Command line mistake flag
OPT_COUNT=0	# Number of options on the command line
MAN_FLAG=0 	# Default: no man pages requited
HELP_FLAG=0	# Default: no help required
VERBOSE_FLAG=0 	# Default: no verbose
WARNING=0 	# General purpose no fail warning flag

# initialize local variables
vbs=""
prefix=""
confdoc="doc/confrep.ent" 

# Parse command line options
while getopts :p:Mhv arguments 
do
   # remember treat r: switches with: R_VALE = $OPTARG ;;
   case $arguments in
      p)    prefix=$OPTARG;;
      M)    MAN_FLAG=1 ;;		# Display man page
      v)    # increment the verboseness count and...
	    VERBOSE_FLAG=$(($VERBOSE_FLAG+1))
	    # gather up all "-v" switches
	    vbs="$vbs -v"
	    ;;
      h)    HELP_FLAG=1;;		# display on-line help
      \?)   echo "Opci�n no reconocida: $OPTARG" >&2	# flag illegal switch 
	    OPT_FLAG=1;;
   esac
done
OPT_COUNT=$(($OPTIND-1))
shift $OPT_COUNT

options_help="
   -p prefijo	Prefijo de la ruta de instalaci�n (por defecto /usr/local)
   -h           Presenta ayuda corta
   -M           Presenta ayuda m�s completa
   -v           Presenta informaci�n de depuraci�n durante ejecuci�n"
 
# check for man page request
if (test "$MAN_FLAG" = "1" ) then {
	if (test "$PAGER" = "" ) then {
		if ( test "$VERBOSE_FLAG" -gt "0" ) then {
			echo "$BASENAME: Resetting PAGER variable to more" >&2

	       	} fi;
	       	export PAGER=more;
	} fi;
	$PAGER << MANPAGE_HEREDOC

NOMBRE

	$BASENAME - Configura fuentes de $PROYECTO

	$USAGE


DESCRIPCI�N

	Establece el valor de las variables de configuraci�n y genera
	archivos en diversos formatos empleados por las fuentes de 
	$PROYECTO.
	Las variables de configuraci�n y sus valores por defecto est�n
	en confv.empty (debajo de cada variable hay un comentario con la 
	descripci�n).
	Este script genera los archivos
		confv.sh, Make.inc, confv.ml y $confdoc
	con las variables de configuraci�n instanciadas.  
	Para la instanciaci�n este script puede:

	* Verificar el valor de algunas variables (por ejemplo que
	  la versi�n de un programa sea la requerida).
	* Buscar valor para algunas variables (por ejemplo ubicaci�n
	  de alg�n programa).
	* Completar el valor de algunas variables (por ejemplo fecha actual)
	* Dejar el valor por defecto configurado en confv.empty (por ejemplo 
	  nombre del proyecto y versi�n).
	* Pedir informaci�n al usuario en los casos que no logra instanciar


OPCIONES

$options_help


EJEMPLOS

	./conf.sh
	Configura fuentes y deja como prefijo para la ruta de instalaci�n 
	"/usr/local"

	./conf.sh -p /usr/
	Configura fuentes y deja como prefijo para la ruta de instalaci�n
	"/usr"


EST�NDARES
	Este script pretende ser portable. Debe cumplir POSIX.


FALLAS


VER TAMBI�N
	Para mejorar este script o hacer uno similar ver fuentes de 
	herram/confaux.sh


CR�DITOS Y DERECHOS DE REPRODUCCI�N 

 	Script de dominio p�blico.  Sin garant�as.
	Fuentes disponibles en: http://structio.sourceforge.net/repasa
	Puede enviar reportes de problemas a 
		structio-info@lists.sourceforge.net

	Incluye porciones de c�digo dominio p�blico escritas por:
	  Miembros de Structio http://structio.sourceforge.net
	  WWWeb Tide Team http://www.ebbtide.com/Util/
	Puede ver m�s detalles sobre los derechos y cr�ditos de este script en
	las fuentes.
MANPAGE_HEREDOC
   exit 0;
} fi;

# check for help
if (test "$HELP_FLAG" = "1" ) then {
   echo " Utilizaci�n: $USAGE"
   cat << HLP_OP
$options_help
HLP_OP
   exit 0
} fi;

# check for illegal switches
if (test "$OPT_FLAG" = "1") then {
   echo "$BASENAME: Se encontr� alguna opci�n invalida" >&2
   echo "Utilizaci�n: $USAGE" >&2
   exit 1
}
elif (test "$#" != "$ARG_COUNT" ) then {
   echo "$BASENAME: se encontraron $# argumentos, pero se esperaban $ARG_COUNT." >&2
   echo "Utilizaci�n: $USAGE" >&2
   exit 1;
} fi;


echo "Configurando $PROJECT $PRJ_VERSION";
if (test "$prefix" != "") then {
	INSBIN="$prefix/bin";
	changeVar INSBIN 1;
	INSXML="$prefix/share/xml/structio";
	changeVar INSXML 1;
	INSDATA="$prefix/share/repasa";
	changeVar INSDATA 1;
	INSMAN="$prefix/man";
	changeVar INSMAN 1;
	ALTXML=`echo $INSXML | sed -e "s|^/usr|/opt|g"`;
	changeVar ALTXML 1;
} fi;

if (test "$VERBOSE_FLAG" -gt "0") then {
	echo "Chequeando y detectando valor de variables de configuraci�n";
} fi;
check "OCAMLC" "" "test -x \$OCAMLC" `which ocamlc 2> /dev/null`
verocamlc=`$OCAMLC -v | grep "version" | sed -e "s|.*version \([0-9]*\).*|\1|g"`;
if (test "$?" != 0 -o "x$verocamlc" = "x") then {
	echo "  $PROJECT s�lo se ha probado con versiones de Ocaml posteriores a 3.0";
} 
elif (test "$verocamlc" -lt 3) then {
	echo "  $PROJECT s�lo se ha probado con versiones de Ocaml posteriores a 3.0";
} fi;
check "OCAMLOPT" "optional" "test -x \$OCAMLOPT" `which ocamlopt 2> /dev/null`
check "OCAMLDEP" "" "test -x \$OCAMLDEP" `which ocamldep 2> /dev/null`
libloc=`$OCAMLC -where`;
check "OCAMLLIB" "" "test -f \$OCAMLLIB/pervasives.cmi" "$libloc";

r=`echo "t" 2>/dev/null >$INSLIB/lrepasa_test.tmp`;
if (test "$?" != 0 -a "$prefix" != "") then {
	echo "  * No es posible escribir en la ruta."
	INSLIB="$prefix/lib/ocaml";
	changeVar INSLIB 1;
}
else {
	rm -f $OCAMLLIB/lrepasa_test.tmp;
} fi;

check "OCAMLYACC" "" "test -x \$OCAMLYACC" `which ocamlyacc 2> /dev/null`;
check "OCAMLLEX" "" "test -x \$OCAMLLEX" `which ocamllex 2> /dev/null`;
check "LABLTK_PATH" "optional" "test -f \$LABLTK_PATH/labltk.cma" "$libloc/labltk" "$libloc";

REPASATK_RULE="repasatk";
if (test "$LABLTK_PATH" = "") then {
	REPASATK_RULE=""
} fi;
changeVar REPASATK_RULE 1;

check "MARKUP_PATH" "" "test -f \$MARKUP_PATH/markup.cma" "$libloc";
check "MAILPROG" "optional" "test -x \$MAILPROG" `which mail 2> /dev/null`

check "PEAR_NET_SMTP" "" "pear list | grep Net_SMTP > /dev/null" "Net_SMTP"
check "W3M" "optional" "test -x \$W3M" `which w3m 2> /dev/null` `which lynx 2> /dev/null`

l=`echo $W3M | sed -e "s|.*lynx.*|si|g"`
W3M_OPT="";
if (test "$l" = "si") then {
	W3M_OPT="-nolist";
} fi;
changeVar W3M_OPT 1;

if (test "$ACT_PROC" = "act-ncftpput") then {
	check "NCFTPPUT" "" "test -x \$NCFTPPUT" `which ncftpput 2> /dev/null`
}
elif (test "$ACT_PROC" = "act-scp") then {
	check "SCP" "" "test -x \$SCP" `which scp 2> /dev/null`
} fi;

FECHA_ACT=`date "+%d/%m/%Y"`;
changeVar FECHA_ACT 1;
m=`date "+%m" | sed -e "s/01/Enero/;s/02/Febrero/;s/03/Marzo/;s/04/Abril/;s/05/Mayo/;s/06/Junio/;s/07/Julio/;s/08/Agosto/;s/09/Septiembre/;s/10/Octubre/;s/11/Noviembre/;s/12/Diciembre/"`
a=`date "+%Y"`
MES_ACT="$m de $a";
changeVar MES_ACT 1;


if (test "$VERBOSE_FLAG" -gt "0") then {
	echo "Guardando variables de configuraci�n"
} fi;
changeConfv;

if (test "$VERBOSE_FLAG" -gt "0") then {
	echo "Generando Make.inc";
} fi;

echo "# Some variables for Makefile" > Make.inc;
echo "# This file is generated automatically by conf.sh.  Don't edit" >> Make.inc;
echo "" >> Make.inc

if (test "$VERBOSE_FLAG" -gt "1") then {
	echo "Adding configuration variables to Make.inc";
} fi;
addMakeConfv Make.inc;

echo "# Other variables" >> Make.inc
INCLUDES="-I $MARKUP_PATH";
if (test "x$LABLTK_PATH" != "x") then {
	INCLUDES="$INCLUDES -I $LABLTK_PATH";
} fi;
echo "INCLUDES=$INCLUDES" >> Make.inc

touch depend

if (test "$VERBOSE_FLAG" -gt "0") then {
	echo "Generando confv.ml";
} fi;

echo "(** Configuration variables. *)" > confv.ml;
echo "(* This file is generated automatically by conf.sh.  Don't edit *)" >> confv.ml; 
echo >> confv.ml;
addOcamlConfv confv.ml;

if (test "$VERBOSE_FLAG" -gt "0") then {
	echo "Modificando $confdoc"
} fi;
cp $confdoc $confdoc.bak
sed -e "s/REPASA-VERSION[ ]*\"[^\"]*\"/REPASA-VERSION \"$PRY_VERSION\"/g;s|REPASA-INSDATA[ ]*\"[^\"]*\"|REPASA-INSDATA \"$INSDATA\"|g" $confdoc.bak > $confdoc

echo "Configuraci�n completada";

