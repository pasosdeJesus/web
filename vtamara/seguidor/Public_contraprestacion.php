<?php
/**
 * Table Definition for public.contraprestacion
 */
require_once 'DB/DataObject.php';

class DataObjects_Public_contraprestacion extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'public.contraprestacion';         // table name
    public $codigo;                          // int4(4)  
    public $da;                              // varchar(-1)  
    public $recibe;                          // varchar(-1)  
    public $fecha;                           // date(4)  
    public $horas;                           // int4(4)  
    public $desc_corta;                      // varchar(-1)  
    public $desc_detallada;                  // varchar(-1)  

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Public_contraprestacion',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
