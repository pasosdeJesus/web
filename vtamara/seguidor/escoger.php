<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 foldmethod=marker: */

/**
* Seguidor de solicitudes y fallas.
* Dominio p�blico. Sin garant�as. 2004. 
* @author Vladimir T�mara. vtamara@users.sourceforge.net
*
* Cr�ditos: 
* Se ha empleado porciones cortas de c�digo y documentaci�n disponible en:
*   http://us2.php.net/manual/es/function.pg-connect.php
*   Documentaci�n y ejemplos de DB Pear.
*   http://structio.sourceforge.net/sigue
*
*/

if (session_id()=="") {
	session_start();
}

require_once("misc.php");
require_once 'MDB2.php';
require_once 'HTML/Form.php';
include('aut/conf.php');

error_reporting(E_ALL);


$aut_usuario="";
$dbh = autenticaUsuario($dsn, $accno, $aut_usuario);


// Agregar botones que permitan preseleccionar y a�adir nuevo

$buscarDesc=isset($_REQUEST['buscarDesc'])
    ? $_REQUEST['buscarDesc'] : '';
$fini=isset($_REQUEST['fini'])?$_REQUEST['fini']:'';
$ffin=isset($_REQUEST['ffin'])?$_REQUEST['ffin']:'';
$cod_categoria=isset($_REQUEST['cod_categoria']) ?
$_REQUEST['cod_categoria'] : 'todas';
$abierto=isset($_REQUEST['abierto'])?$_REQUEST['abierto']:'t';
$cod_responde=isset($_REQUEST['cod_responde'])?$_REQUEST['cod_responde']:'todos';
$cod_enviado=isset($_REQUEST['cod_enviado'])?$_REQUEST['cod_enviado']:'todos';
$dependencia=isset($_REQUEST['dependencia'])?$_REQUEST['dependencia']:'todas';
$prioridad=isset($_REQUEST['prioridad'])?$_REQUEST['prioridad']:'todas';
$porcentaje=isset($_REQUEST['porcentaje'])?$_REQUEST['porcentaje']:'todos';

$ord1=isset($_REQUEST['ord1'])?$_REQUEST['ord1']:'prioridad';
$ord2=isset($_REQUEST['ord2'])?$_REQUEST['ord2']:'cod_categoria';
$ord3=isset($_REQUEST['ord3'])?$_REQUEST['ord3']:'cod_responde';

$tit="Listado de solicitudes";
echo "<html><head><title>$tit</title></head>\n";
echo "<body><h1>$tit</h1>\n";
echo "<form action='".$_SERVER['PHP_SELF']."' method='POST'>\n";

//echo "OJO: buscarDesc=$buscarDesc;  fini=$fini; ffin=$ffin; cod_categoria=$cod_categoria; abierto=$abierto; cod_responde=$cod_responde; cod_enviado=$cod_enviado; dependencia=$dependencia; prioridad=$prioridad; porcentaje=$porcentaje; ord1=$ord1; ord2=$ord2; ord3=$ord3";

echo "<hr>\n";
echo "<h3>Filtro</h3>";
echo "Que contenga: \n";
echo "<input name='buscarDesc' size='50' value='$buscarDesc' maxlength='50'/>\n";
// Sintaxis de LIKE
// X_Y remplaza un caracter
// X% empieza en X
// %Y termina en Y
// %A% en alg�na parte aparece A
echo "<br>Rango de fechas para apertura: \n";
echo "<input name='fini' size='12' value='$fini' maxlength='12'/>\n";
echo "a <input name='ffin' size='12' value='$ffin' maxlength='12'/><br>\n";

function getAssoc($d, $q) {
    $res=$d->query($q);
    while(($row=$res->fetchRow())) {
        $a[$row

    $d->
$categorias=arrayFromTree(getAssoc($dbh, "SELECT codigo, desc_corta, cod_mama ".
		"FROM categoria;"));
$categorias['todas']="Todas";
echo "Categor�a: ".creaSelect($categorias, 'cod_categoria', $cod_categoria)."<br>";

echo "Estado: ".creaSelect(array('t'=>"Abierto", 'f'=>"Cerrado"),
    'abierto', $abierto);

$asignado=$dbh->getAssoc("SELECT id,nombre FROM responde".
                " ORDER BY nombre;");
$asignado['todos']="Todos";
echo "Asignado a: ".creaSelect($asignado, 'cod_responde', $cod_responde);

$enviado=$dbh->getAssoc("SELECT id,nombre FROM responde".
                " ORDER BY nombre;");
$enviado['todos']="Todos";
echo "Enviado por: ".creaSelect($enviado, 'cod_enviado', $cod_enviado)."<br>";

$dependencias=arrayFromTree($dbh->getAssoc("SELECT codigo,desc_corta,cod_mama ".
                "FROM dependencia;"));
$dependencias['todas']="Todas";
echo "Dependencia: ".creaSelect($dependencias, 'dependencia', 
    $dependencia)."<br>";

$prioridades=array(1 => "1",
    2 => "2", 3 => "3", 4 => "4", 5 => "5", 'todas' => 'Todas');
echo "Prioridad: ".creaSelect($prioridades, 'prioridad', $prioridad);

$porcentajes=array();
for ($i=0;$i<=100;$i+=5) {
    $porcentajes[$i]=$i;
}
$porcentajes['todos']='Todos';
echo "Porcentaje: ".creaSelect($porcentajes, 'porcentaje', $porcentaje);


echo "<h3>Ordenado por</h3>";
  
$opord=array('prioridad' => 'Prioridad',
  'cod_categoria'=>'Categoria',
  'cod_responde'=>'Asignado a',
  'cod_enviado'=>'Enviado por',
  'dependencia'=>'Dependencia',
  'apertura'=>'Fecha de apertura',
  'cierre'=>'Fecha de cierre',
  'porcentaje DESC'=>'Porcentaje');

echo "1: ".creaSelect($opord, "ord1", $ord1);
echo "2: ".creaSelect($opord, "ord2", $ord2);
echo "3: ".creaSelect($opord, "ord3", $ord3);

echo "<br><center><input name='submit' type='submit' value='Refrescar' /></center>";
echo "</form>\n";

echo "<hr>";

$ncol=array("codigo","cod_categoria","abierto","cod_responde",
"cod_enviado","dependencia","prioridad","apertura",
"cierre","desc_corta","desc_detallada","repetir","solucion",
"porcentaje");


$where = "WHERE abierto=".$dbh->quote($abierto)." ";
if ($buscarDesc!='') {
    $where.="AND (desc_corta LIKE ".$dbh->quote("%$buscarDesc%")." OR ".
        "desc_detallada LIKE ".$dbh->quote("%$buscarDesc%")." OR ".
        "repetir LIKE ".$dbh->quote("%$buscarDesc%")." OR ".
        "solucion LIKE ".$dbh->quote("%$buscarDesc%").") ";
}
if ($fini!='') {
    $where.="AND apertura>=".$dbh->quote($fini)." ";
}
if ($ffin!='') {
    $where.="AND apertura<=".$dbh->quote($ffin)." ";
}
if ($cod_categoria!='todas') {
    $where.="AND cod_categoria=".$dbh->quote($cod_categoria)." ";
}
if ($cod_responde!='todos') {
    $where.="AND cod_responde=".$dbh->quote($cod_responde)." ";
}
if ($cod_enviado!='todos') {
    $where.="AND cod_enviado=".$dbh->quote($cod_enviado)." ";
}
if ($dependencia!='todas') {
    $where.="AND dependencia=".$dbh->quote($dependencia)." ";
}
if ($prioridad!='todas') {
    $where.="AND prioridad=".$dbh->quote($prioridad)." ";
}
if ($porcentaje!='todos') {
    $where.="AND porcentaje=".$dbh->quote($porcentaje)." ";
}

$q="SELECT ".implode(',',array_values($ncol))." FROM solicitud
    $where ORDER BY $ord1, $ord2, $ord3 ;";  
//echo "CONSULTA *$q*";
$sth=$dbh->query($q);
if (PEAR::isError($sth)) {
    die($sth->getMessage());
}

echo "<table><tr><th>C�d</th><th>Prio</th><th>Cat</th><th>Apertura</th><th>Descripci�n</th><th>Enviado por</th><th>Asignado a</th><th>%</th></tr>";
$row=array();
while ($sth->fetchInto($row)) {
    $campo=array();
    foreach ($row as $k => $v) {
        $campo[$ncol[$k]]=$v;
    }

    echo "<tr><td><a href='solicitud.php?codigo=".$campo['codigo']."'>".
        $campo["codigo"]."</a></td>";
    echo "<td>".$campo["prioridad"]."</td>";
    echo "<td>".$campo["cod_categoria"]."</td>";
    echo "<td>".$campo["apertura"]."</td>";
    echo "<td>".$campo["desc_corta"]."</td>";
    echo "<td>".$campo["cod_enviado"]."</td>";
    echo "<td>".$campo["cod_responde"]."</td>";
    echo "<td>".$campo["porcentaje"]."</td>";

}
echo "</table>";

echo "<hr>\n";

navega("Nueva solicitud", "solicitud.php", $aut_usuario);
/* echo "<table width='100%'><tr>".
     "<td><a href='solicitud.php'>Nueva solicitud</a></td>".
     "<td><a href='tiempo.php'>Consultar Tiempo</a></td>".
     "<td><a href='contrap_lista.php'>Contraprestaci�n</td>".
     "<td><a href='cuenta.php'>Mi cuenta</td>".
     "<td><a href='salir.php'>Salir (".$aut_usuario
        .")</a></td></tr></table>\n"; */

$_SESSION['accion']='ver';

?> 
