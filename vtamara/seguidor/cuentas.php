<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 foldmethod=marker: */

/**
* Seguidor de solicitudes y fallas.
* Dominio p�blico. Sin garant�as. 2006. 
* @author Vladimir T�mara. vtamara@users.sourceforge.net
*
* Listado de cuentas.
*
*/

if (session_id()=="") {
	session_start();
}

require_once("misc.php");
require_once 'DBM2.php';
require_once 'HTML/Form.php';
include('aut/conf.php');

error_reporting(E_ALL);


$aut_usuario="";
$dbh = autenticaUsuario($dsn, $accno, $aut_usuario);


// Agregar botones que permitan preseleccionar y a�adir nuevo

$tit="Listado de usuarios";
echo "<html><head><title>$tit</title></head>\n";
echo "<body><h1>$tit</h1>\n";
echo "<form action='".$_SERVER['PHP_SELF']."' method='POST'>\n";

$ncol=array("id","nombre","direccion","telefono",
    "correo","homepage","habilitado");

$q="SELECT ".implode(',',array_values($ncol))." FROM responde;";

//echo "CONSULTA *$q*";
$sth=$dbh->query($q);
if (PEAR::isError($sth)) {
    die($sth->getMessage());
}

echo "<table width=100% border=1><tr><th>Id</th><th>Nombre</th><th>Direcci�n</th><th>Tel�fono</th><th>Correo</th><th>Hab.</th></tr>";
$row=array();
while ($sth->fetchInto($row)) {
    $campo=array();
    foreach ($row as $k => $v) {
        $campo[$ncol[$k]]=$v;
    }

    echo "<tr><td>";
    if ($campo["homepage"]!='') {
        echo "<a href=\"".$campo['homepage']."\">".$campo['id']."</a>";
    }
    else {
        echo $campo['id'];
    }
    echo "</td>";
    echo "<td>".$campo["nombre"]."</td>";
    echo "<td>".$campo["direccion"]."</td>";
    echo "<td>".$campo["telefono"]."</td>";
    echo "<td>".$campo["correo"]."</td>";
    echo "<td>".($campo["habilitado"]=='t'?"Si":"No")."</td>";
    echo "</tr>";

}
echo "</table>";

echo "<hr>\n";

navega('', '', $aut_usuario);


?> 
