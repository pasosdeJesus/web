<?php
/**
 * Table Definition for public.prerequisito
 */
require_once 'DB/DataObject.php';

class DataObjects_Public_prerequisito extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'public.prerequisito';             // table name
    public $cod_sol;                         // int4(4)  
    public $cod_solpre;                      // int4(4)  

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Public_prerequisito',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
