
<?php
# vim: set expandtab tabstop=4 shiftwidth=4 foldmethod=marker:

# Dominio publico.  2004. Sin garantias. 

require_once 'DB/DataObject.php';

class DataObjects_Contraprestacion extends DB_DataObject
{
	var $__table = 'contraprestacion'; // table name
	var $codigo;                      // int4(4)  not_null primary_key
	var $da;                          // bpchar(-1) not null
	var $recibe;                      // bpchar (-1)  not null
	var $fecha;                       // date(4) not_null
	var $horas;                       // int4(4)  not_null
	var $desc_corta;                  // bpchar(-1)  not_null
	var $desc_detallada;             // bpchar(-1)  

	/* ZE2 compatibility trick*/
	function __clone() { return $this;}

	/* Static get */
	function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Contraprestacion',$k,$v); }

//	var $fb_linkDisplayFields = array('nombre');
	var $fb_fieldLabels= array('codigo' => 'C�digo',
		'da' => 'De',
		'recibe' => 'Para',
		'fecha' => 'Fecha',
		'horas' => 'Horas',
		'desc_corta' => 'Descripci�n Corta',
		'desc_detallada' => 'Descripci�n Detallada'
	);
    var $fb_hidePrimaryKey = true;
    var $fb_textFields = array('desc_detallada');
    var $fb_dateFields=array('fecha');
    var $fb_select_display_field='desc_corta';
    var $fb_addFormHeader=false;

    function postGenerateForm(&$form) {
        $t =& $form->getElement('horas');
        $t->setSize(3);
        $t =& $form->getElement('desc_corta');
        $t->setSize(60);
        $t =& $form->getElement('desc_detallada');
        $t->setCols(60);
        $t->setRows(3);
    }

}

?>


