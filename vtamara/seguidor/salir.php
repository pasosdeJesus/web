<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 foldmethod=marker: */

/**
* Retira autenticación de usuario.
* Dominio público. Sin garantías. 2004. 
* Basado en 
    http://pear.php.net/manual/en/package.authentication.auth.intro.php
*/

include "aut/conf.php";
require_once "misc.php";

$params = array(
    "dsn" => $dsn,
    "table" => "responde",
    "usernamecol" => "id",
    "passwordcol" => "clave"
); 
$a = new Auth("MDB2", $params, "loginFunction");

$a->start();
if ($a->checkAuth()) {
        $a->logout();
}


?> 
<html>
<head><title>Gracias</title></head>
<body>
Gracias.
<br>
Enlaces recomendados porque se ha usado o han apoyado 
al proyecto <a href="http://www.pasosdeJesus.org/seguidor/">seguidor</a>:
<ul>
<li> <a href="http://www.pasosdeJesus.org">http://www.pasosdeJesus.org</a>
<li> <a href="http://structio.sf.net">http://structio.sf.net</a>
<li> <a href="http://ceudes.org.co">http://ceudes.org.co</a>
<li> <a href="http://www.sofhouse.net">http://www.sofhouse.net</a>
<li> <a href="http://uvirtual.ean.edu.co">http://uvirtual.ean.edu.co</a>
<li>  <a href="http://oblatas.pasosdeJesus.org">http://oblatas.pasosdeJesus.org</a>
<li>  <a href="http://www.reiniciar.org">http://www.reiniciar.org</a>
<li>  <a href="http://www.justapaz.org">http://www.justapaz.org</a>
<li>  <a href="http://www.jypinterfranciscana.com">http://www.jypinterfranciscana.com</a>
</ul> 
</body>
</html>
