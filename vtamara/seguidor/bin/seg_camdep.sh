#!/bin/sh
# Cambia una dependencia. Dominio p�blico. Sin garant�as. 2004

codigo=$1;
cod_mama=$2;
if (test "$codigo" = "") then {
	echo "Primer par�metro debe ser c�digo de dependencia por cambiar";
	exit 1;
} fi;

if (test "$cod_mama" = "") then {
	cod_mama="null";
} else {
	cod_mama="'$cod_mama'";
} fi;


DIRSEG=.

if (test ! -f $DIRSEG/vardb.sh) then {
	echo "Por favor ejecute desde el directorio de seguidor";
	exit 1;
} fi;

. $DIRSEG/vardb.sh

echo -n "Descripci�n corta: ";
read desc_corta;
echo "Descripci�n larga: ";
read desc_larga;
echo -n "Direcci�n: ";
read direccion;
echo -n "Tel�fono: ";
read telefono;
echo -n "Homepage: ";
read homepage;
echo -n "Id. de responsable que es contacto principal: ";
read contacto;

psql $socketopt -U $dbuser -d $dbname -c "UPDATE dependencia SET  desc_corta='$desc_corta', desc_larga='$desc_larga', direccion='$direccion', telefono='$telefono', homepage='$homepage', contacto='$contacto', cod_mama=$cod_mama WHERE codigo='$codigo';"

