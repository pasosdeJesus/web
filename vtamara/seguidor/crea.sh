#!/bin/sh
# Crea y llena base de datos en PostgreSQL 
# Dominio p�blico. Sin garant�as. 2004. vtamara@users.sourceforge.net

. ./vardb.sh

echo "Advertencia: este script borrara la informaci�n existente en "
echo "la base '$dbname' del usuario '$dbuser'";
echo "Presione [Enter] para continuar o detenga con [Ctrl]+[C]";
read;

dropdb $socketopt $dbname; 
dropuser $socketopt $dbuser;
createuser $socketopt -A -d -P $dbuser
createdb $socketopt -O $dbuser $dbname;
if (test ! -f estructura.pgsql) then {
	echo "Debe haber un script SQL estructura.pgsql que cree tablas";
	exit 1;
} fi;
psql $socketopt -U $dbuser -W -d $dbname -f estructura.pgsql;

if (test -f privado/datos.pgsql) then {
	psql $socketopt -U $dbuser -W -d $dbname -f privado/datos.pgsql;
} elif (test -f datos.pgsql) then {
	psql $socketopt -U $dbuser -W -d $dbname -f datos.pgsql;
} fi;

