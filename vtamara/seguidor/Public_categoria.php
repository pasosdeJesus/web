<?php
/**
 * Table Definition for public.categoria
 */
require_once 'DB/DataObject.php';

class DataObjects_Public_categoria extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'public.categoria';                // table name
    public $codigo;                          // int4(4)  
    public $desc_corta;                      // varchar(-1)  
    public $desc_larga;                      // varchar(-1)  
    public $cod_mama;                        // int4(4)  

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Public_categoria',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
