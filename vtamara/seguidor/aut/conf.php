<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 foldmethod=marker: */
# Base de datos
$dbservidor="unix(/tmp)"; # Si prefiere TCP/IP (no recomendado) use tcp(localhost)
$dbnombre="seguidor";
$dbusuario="vtamara";
$dbclave="xyz";

$socketopt="-h /var/www/tmp";  # Opci�n para psql que indica directorio con 
# sockets.
# Esta relacionada con $dbservidor si se realiza conexi�n con sockets, debe
# corresponder a lo que diga la varialbe unix_socket_directory de
# postgresql.conf (por defecto es /tmp).
# En caso de usar Apache con chroot puede configurarse esta y 
# unix_socket_directory en /var/www/tmp (dejando $dbservidor en unix(/tmp) ).


# Cadena en caso de no existir usuario o clave
$accno="Acceso no autorizado\n";

$GLOBALS['CORREO']=true;

$GLOBALS['PALABRA_SITIO']='sigamos el ejemplo de Jes�s';

# Nombre de usuario invitado (debe coincidir con el de la base de datos).
$nom_invitado='invitado';

# Las que siguen normalmente no tendr� que modificarlas

$dsn = "pgsql://$dbusuario:$dbclave@$dbservidor/$dbnombre";

require_once "PEAR.php";

require_once('DB/DataObject.php');
require_once('DB/DataObject/FormBuilder.php');

$config = parse_ini_file('DataObject.ini', true);
foreach ($config as $class => $values) {
	$options = &PEAR::getStaticProperty($class, 'options');
	$options = $values;
}
 
$_DB_DATAOBJECT_FORMBUILDER['CONFIG']=$config['DB_DataObject_FormBuilder'];

?>
