<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 foldmethod=marker: */

/**
 * Seguidor de solicitudes y fallas.
 * Dominio p�blico. 2004. 
 * @author Vladimir T�mara. vtamara@users.sourceforge.net

 * Cr�ditos: 
 * Se ha empleado porciones cortas de c�digo y documentaci�n disponible en:
 *   http://us2.php.net/manual/es/function.pg-connect.php
 *   Documentaci�n y ejemplos de DB Pear.
 *   http://structio.sourceforge.net/sigue

 * Formulario para ver, editar o agregar una solicitud.
 **/

/** Muestra un comentario y como sublista la(s) respuesta(s) que haya tenido */
function muestra_com($dbh, $codb, $row) {
	if ($row[0]!="NULL") {
		echo "<li> ".$row[2]." <a href=\"comentario.php?cod_solicitud=$codb&".
			"codigo=".$row[0]."\">".
			$row[4]."</a> ".$row[3]. " - ".$row[5];
		$ra="respuesta_a='".$row[0]."'";
	}
	else {
		$ra="respuesta_a IS NULL";
	}
	$q="SELECT codigo, respuesta_a, fecha, cod_enviado, desc_corta, ".
		"tiempo_invertido FROM comentario WHERE cod_solicitud='$codb' ".
		"AND $ra ORDER BY fecha;";
	$sth=$dbh->query($q);
	if (PEAR::isError($sth) || $sth==NULL) {
		die($sth->getMessage());
	}
	if ($sth->numRows($sth)>0) {
		echo "<ul>";
		$filar=array();
		while ($sth->fetchInto($filar)) {
			muestra_com($dbh, $codb, $filar);
		}
		echo "</ul>";
	}
}


/** Retorn correo de un usuario. */
function correo_us($dbh, $us) {
	$q="SELECT correo FROM responde WHERE id='$us';";
	$sth=$dbh->query($q);
	if (PEAR::isError($sth) || $sth==NULL) {
		die($sth->getMessage());
	}
	$filar=array();
	if ($dbh->numRows($sth)>0 && $sth->fetchInto($filar)) {
		return $filar[0];
	}
	return "";
}

if (session_id()=="") {
	session_start();
}
require_once 'MDB2.php';
require_once 'HTML/Form.php';
require_once 'Mail.php';
include('aut/conf.php');
require_once 'misc.php';

error_reporting(E_ALL);

$ocorreo=Mail::factory('smtp',array('host' => 'localhost'));

$codb=isset($_REQUEST['codigo']) ? $_REQUEST['codigo'] : "";
$aut_usuario="";
$dbh = autenticaUsuario($dsn, $accno, $aut_usuario);
$ncol=array("codigo","cod_categoria","abierto","cod_responde", 
    "subsolicitud_de",
		"cod_enviado","correo_invitado", "dependencia","prioridad","apertura",
		"cierre", "horas", "desc_corta","desc_detallada","repetir","solucion",
		"porcentaje");

$tit="";
$emisor_correo=$aut_usuario;
$receptor_correo= isset($_REQUEST['cod_responde']) ? $_REQUEST['cod_responde'] :
    '';
$tema_correo=isset($_REQUEST['desc_corta']) ? $_REQUEST['desc_corta'] : '';
$boton_sub='Actualizar';
if (isset($_REQUEST['submit']) && $_REQUEST['submit']=='A�adir') {

	// Crear nuevo registro, escogiendo c�digo y haciendo INSERT
	$q="SELECT MAX(codigo) FROM solicitud;";
	$sth=$dbh->query($q);
	if (PEAR::isError($sth) || $sth==NULL) {
		die($sth->getMessage());
	}
	$row=array();
	if (!$sth->fetchInto($row)) {
		die("Problema escogiendo nuevo c�digo");
	}
	$codb=((int)$row[0])+1;
	$q="INSERT INTO solicitud (";
	$values="";
	$sep="";
	foreach($ncol as $f) {
		$q.="$sep $f";
		if ($f=="codigo") {
			$values.="$sep ".$dbh->quote($codb);
		}
		else if ($f=="cod_enviado") {
			$values.="$sep ".$dbh->quote($aut_usuario);
		}
		else if ($f=="correo_invitado") {
			$values.="$sep ".$dbh->quote(
					isset($_SESSION['correo_invitado'])?
					$_SESSION['correo_invitado']:
					'');
		}
		else if ($f=="apertura") {
			$values.="$sep ".$dbh->quote(date("j-M-Y"));
		}
		else if ($f=="desc_corta") {
			$r=$_REQUEST[$f]=='' ? 'NULL' : $dbh->quote($_REQUEST[$f]);
			$values.="$sep $r";
		}
		else if ($f=="cod_responde") {
			$r=$_REQUEST[$f]=='' ? 'NULL' : $dbh->quote($_REQUEST[$f]);
			$values.="$sep $r";
		}
		else {
			$r=$_REQUEST[$f]=='' ? 'NULL' : $dbh->quote($_REQUEST[$f]);
			$values.="$sep $r";
		}
		$sep=",";
	}
	$q.=") VALUES (".$values.");";
	$sth=$dbh->query($q);
	if (PEAR::isError($sth) || $sth==NULL) {
		die($sth->getMessage());
	}

	if (isset($_REQUEST['prerequisitos'])) {
		foreach($_REQUEST['prerequisitos'] as $p) {
			$pq=$dbh->quote($p);	
			$q="INSERT INTO prerequisito (cod_sol, cod_solpre)
				VALUES ('$codb', $pq);";
			$sth=$dbh->query($q);
			if (PEAR::isError($sth) || $sth==NULL) {
				die($sth->getMessage());
			}
		}
	}

	$tit="Solicitud a�adida ";

}
if (isset($_REQUEST['submit']) && $_REQUEST['submit']=='Actualizar') {
	if ($aut_usuario==$nom_invitado) {
		die("El usuario invitado no puede actualizar informaci�n");
	}        

	// Actualizar registro existente con UPDATE
	$q="UPDATE solicitud SET ";
	$sep="";
	foreach($ncol as $v) {
		if ($v!='codigo' && $v!='cod_enviado' 
				&& $v!='correo_invitado' && $v!='apertura') {
			$r=$_REQUEST[$v]=='' ? 'NULL' : $dbh->quote($_REQUEST[$v]);
			$q.="$sep $v=".$r;
			$sep=",";
		}
	}
	$q.=" WHERE codigo=".$dbh->quote($codb).";";  
	$sth=$dbh->query($q);
	if (PEAR::isError($sth) || $sth==NULL) {
		die($sth->getMessage());
	}
#Actualizamos prerequisitos sacando existentes y agregando seleccionados
	$q="DELETE FROM prerequisito WHERE cod_sol='$codb';";
	$sth=$dbh->query($q);
	if (PEAR::isError($sth) || $sth==NULL) {
		die($sth->getMessage());
	}
    if (isset($_REQUEST['prerequisitos'])) {
	    foreach($_REQUEST['prerequisitos'] as $p) {
		    $pq=$dbh->quote($p);	
		    $q="INSERT INTO prerequisito (cod_sol, cod_solpre)
			    VALUES ('$codb', $pq);";
		    $sth=$dbh->query($q);
		    if (PEAR::isError($sth) || $sth==NULL) {
			    die($sth->getMessage());
		    }
        }
	}


	$tit="Solicitud actualizada ";
}
if (isset($_REQUEST['submit']) && $_REQUEST['submit']=='Eliminar') {
	if ($aut_usuario==$nom_invitado) {
		die("El usuario invitado no puede eliminar informaci�n");
	}        
	$q="DELETE FROM prerequisito WHERE cod_sol='$codb';";
	$sth=$dbh->query($q);
	if (PEAR::isError($sth) || $sth==NULL) {
		die($sth->getMessage());
	}

	$q="DELETE FROM prerequisito WHERE cod_solpre='$codb';";
	$sth=$dbh->query($q);
	if (PEAR::isError($sth) || $sth==NULL) {
		die($sth->getMessage());
	}

	$q="DELETE FROM comentario WHERE cod_solicitud='$codb';";
	$sth=$dbh->query($q);
	if (PEAR::isError($sth) || $sth==NULL) {
		die($sth->getMessage());
	}

	$q="DELETE FROM solicitud ";
	$q.=" WHERE codigo=".$dbh->quote($codb).";";  
	$sth=$dbh->query($q);
	if (PEAR::isError($sth) || $sth==NULL) {
		die($sth->getMessage());
	}
	$tit="Solicitud eliminada ";
	echo "<html><head><title>$tit</title></head>\n";
	echo "<div style='text-align: center;'><font size='+3'>
				<span style='color: purple'>$tit</span></font></div><br>";

//    echo "<body><h1>$tit</h1>\n";
	echo "<hr>";
}
else {

# Posibles prerequisitos son solicitudes abiertas
	$pos_prereq=$dbh->getAssoc("SELECT codigo,codigo || ' - ' || desc_corta ".
			"FROM solicitud WHERE abierto='t';");

	if ($codb!='') {
		// Ver datos asociados a un c�digo
		$tit .= "Examinando solicitud";
		$q="SELECT ".implode(',',array_values($ncol)).
			" FROM solicitud WHERE codigo='$codb';";  
		$sth=$dbh->query($q);
		if (PEAR::isError($sth) || $sth==NULL) {
			die($sth->getMessage());
		} else if ($sth->numRows($sth)!=1) {
			die("C�digo desconocido");
		}
		$row = array();
		$sth->fetchInto($row);
		$campo=array();
		foreach ($ncol as $k => $v) {
			$campo[$v]=$row[$k];
		}

# De los posibles prerequisitos sacar $codb si est�
		if (in_array($codb,$pos_prereq)) {
			unset($pos_prereq[$codb]);
		}

# Prerequisitos seleccionados
		$q="SELECT cod_solpre, cod_solpre FROM prerequisito WHERE cod_sol='$codb';";
		$campo['prerequisitos'] = $dbh->getAssoc($q);
	}
	else {
		$tit.="Nueva solicitud";
		// Formulario en blanco listo para a�adir
		foreach ($ncol as $k => $v) {
			$campo[$v]="";
		}
		$campo['apertura']=date("j-M-Y");
		$campo['cod_enviado']=$aut_usuario;
		$campo['correo_invitado']=isset($_SESSION['correo_invitado'])?
			$_SESSION['correo_invitado']:
			'';
		$boton_sub='A�adir';
		$campo['prerequisitos']=array();
	}


    echo "<html><head><enter><title>$tit</title></center></head>\n";
    echo "<div style='text-align: center;'><font size='+3'>
				<span style='color: blue'>$tit</span></font></div><br>";

//    echo "<body><center><h1>$tit</center></h1>\n";
	echo "<hr>";

	$form=new HTML_Form($_SERVER['PHP_SELF']);

	$form->addHidden("codigo", $campo['codigo']);

	$form->addText("desc_corta","Descripci�n corta",
			$campo['desc_corta'],60,60);

	$categoria=$dbh->getAssoc("SELECT codigo, desc_corta, cod_mama ".
        "FROM categoria;");
    $a = arrayFromTree($categoria);
	$s = $form->addSelect("cod_categoria","Categoria",
			$a,
            (int)$campo['cod_categoria']);


	$form->addSelect("abierto","Estado", 
			array('t'=> "Abierto", 'f'=> "Cerrado"),
			$campo['abierto']);

	$responde=$dbh->getAssoc("SELECT id,nombre FROM responde ".
        "WHERE habilitado=TRUE".
		" ORDER BY nombre;");
    if (PEAR::isError($responde)) {
        die($responde->getMessage());
    }
	$form->addSelect("cod_responde","Responde",$responde,
			$campo['cod_responde']);

     $possolicitud=$dbh->getAssoc("SELECT codigo, desc_corta FROM solicitud ".
		" ORDER BY codigo ;");
    if (PEAR::isError($possolicitud)) {
        die($possolicitud->getMessage());
    }
	$form->addSelect("subsolicitud_de","Subsolicitud de", $possolicitud,
			(int)$campo['subsolicitud_de']);


	$form->addPlaintext("Enviado por", $responde[$campo['cod_enviado']]);

	$dependencia=$dbh->getAssoc("SELECT codigo,desc_corta,cod_mama ".
			"FROM dependencia;");
	$form->addSelect("dependencia","Dependencia",
			arrayFromTree($dependencia),
			(int)$campo['dependencia']);

	$form->addSelect("prioridad","Prioridad", array(1 => "1",
				2 => "2", 3 => "3", 4 => "4", 5 => "5"),
			(int)$campo['prioridad']);

	$form->addPlaintext("Fecha de apertura", $campo['apertura']);
	$form->addHidden("apertura", $campo['apertura']);

	$form->addText("cierre","Fecha de cierre",
			$campo['cierre'],12,12);

	$form->addText("horas","Horas esperadas",
			$campo['horas'],5,5);

	$form->addTextarea("desc_detallada","Descripci�n detallada",
			$campo['desc_detallada'],
			60,3, 1000);

	$form->addTextarea("repetir",
			"�C�mo repetir falla?",
			$campo['repetir'],
			60,3,1000);

	$form->addTextarea("solucion","Soluci�n o sugerencia",
			$campo['solucion'],
			60,3,1000);

	$form->addSelect("prerequisitos","Prerequisitos", $pos_prereq,
			$campo['prerequisitos'],5,'',true);


	$por=array();
	for ($i=0;$i<=100;$i+=5) {
		$por[$i]=$i;
	}
	$form->addSelect("porcentaje", "Porcentaje completado", $por,
			$campo['porcentaje']);

	$form->addSubmit("submit", $boton_sub); 
	if ($boton_sub=="Actualizar") {
		$form->addSubmit("submit", "Eliminar"); 
	}

	$form->display(); 


	if ($codb!='') {
		echo "<hr><h2>Comentarios: </h2>\n";
		$fila=array(0=> "NULL");
		muestra_com($dbh, $codb, $fila);
		echo "<a href=\"comentario.php?cod_solicitud=$codb\">".
			"A�adir comentario</a><br>";
	}
}

if ($ocorreo!=NULL && $GLOBALS['CORREO'] && $receptor_correo!='' ) {
	$rec=correo_us($dbh, $receptor_correo);
	$headers['To'] = $rec;
	$headers['Subject'] = "Seguidor. $tit. $tema_correo";
	$headers['From'] = correo_us($dbh, $emisor_correo);

	$cuerpo_correo="";
	foreach($_REQUEST as $k => $v) {
		if ($k == 'codigo') {
			$cuerpo_correo.=$k.": ".$codb."\n";
		}
		else if ($k != 'submit' && $k != '_fields'
				&& $k != 'PHPSESSID') {
			$cuerpo_correo.=$k.": ".$v."\n";
		}
	}


	$ocorreo->send($rec, $headers, $cuerpo_correo);
}

$dbh->disconnect();
echo "<hr>";
navega('', '', $aut_usuario);
echo "</body></html>";
?> 
