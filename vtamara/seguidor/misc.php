<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 foldmethod=marker: */

// Funciones diversas �tiles en varias fuentes PHP.
// Dominio p�blico. 2004. Sin garant�as

// @author Vladimir T�mara. vtamara@users.sourceforge.net
// Cr�ditos: 
// Se ha empleado porciones cortas de c�digo y documentaci�n disponible en:
//   http://structio.sourceforge.net/sigue

require_once "Auth.php";
require_once "MDB2.php";

/** 
* Crea un SELECT para un formulario HTML, a partir de una arreglo.
* Cada item ser� una opci�n, la llave ser� lalve de la opci�n, el valor,
* ser� texto por mostrar.
* @param array $arreglo
* @param string $nombre nombre del SELECT
* @param string $seleccionado llave de opci�n que debe aparecer elegida
* @param bool $invertido si es cierto se usa orden inverso al del arreglo en 
*	SELECT
* @author Igor T�mara, 2002. Dominio p�blico. http://structio.sourceforge.net/sigue
*/
function creaSelect($arreglo, $nombre, $seleccionado, $invertido=false){

	$r="<select name=\"$nombre\">\n";
	$i=0;
    if ($invertido) {
        $arr=array_reverse($arreglo);
    }
    else {
        $arr=$arreglo;
    }
	foreach($arr as $key => $value) {
		if ($seleccionado==$key) {
			$s="selected=\"1\"";
		}
		else {
			$s="";
		}
		$r.="  <option value=\"$key\" $s>$value</option>\n";
	}
	$r.="</select>\n";

    return $r;
}

// Tomada de file:///usr/doc/phpdoc/html/function.srand.html
function make_seed() {
        list($usec, $sec) = explode(' ', microtime());
            return (float) $sec + ((float) $usec * 100000);
        }


/* Autenticaci�n con base en 
    http://pear.php.net/manual/en/package.authentication.auth.intro.php
*/
function loginFunction()
{
    echo "<form method=\"post\" action=\"" . $_SERVER['PHP_SELF'] . "\">";
    echo "<table border='0'>";
    echo "<tr><td bgcolor='#c0c0c0' colspan='2'>Autenticaci�n</td></tr>";
    echo "<tr><td>Usuario:</td><td><input type=\"text\" name=\"username\"></td></tr>";
    echo "<tr><td>Clave:</td><td><input type=\"password\" name=\"password\"><br></td></tr>";
    echo "<tr><td colspan='2' align='center'><input type=\"submit\" value=\"Enviar\"></tr></td>";
    echo "</table>";
    echo "</form>";
}

/** autentica un usuario listado como posible responsable
    en la base de datos.  
    @param dsn URL de base de datos
    @param usu  Login del usuario
    @param pw   Clave del usuario
    @param accno Mensaje de error por mostrar si no autentica
*/
function autenticaUsuario($dsn,  $accno, &$usuario) {

    $params = array(
        "dsn" => $dsn,
        "table" => "responde",
        "usernamecol" => "id",
        "passwordcol" => "clave"
    );
    $options=array();
    $dbh =& MDB2::connect($dsn, $options);
    if (PEAR::isError($dbh)) {
        echo "dsn=$dsn<br>";
        die("Posiblemente PostgreSQL no est� funcionando: ".
            $dbh->getMessage() . " - " .
            $dbh->getUserInfo());
    }
    $a = new Auth("MDB2", $params, "loginFunction");

    $a->start();
    if ($a->checkAuth()) {
        $options = array(
            'debug'       => 5
        );

        $dbh =& MDB2::connect($dsn, $options);
        $usuario=$a->getUsername();

        return $dbh;
    }
    else {
        echo md5($_REQUEST['password']);
        die($accno);         
    }
}

/** autentica un usuario listado como posible responsable
    en la base de datos.  
    @param dsn URL de base de datos
    @param usu  Login del usuario
    @param pw   Clave del usuario
    @param accno Mensaje de error por mostrar si no autentica
*/
function autenticaUsuario_HTTP($dsn,  $accno) {
    trigger_error("1 PHP_AUTH_USER=".$_SERVER['PHP_AUTH_USER'].
        ", PHP_AUTH_PW=".$_SERVER['PHP_AUTH_PW'].
        ", ultimoQueSalio=".$_SESSION['ultimoQueSalio'].
        ", autenticado=".$_SESSION['autenticado'].
        ", headers-sent=".(headers_sent()?"si":"no"));
 
    if (!isset($_SERVER['PHP_AUTH_USER']) || 
    (isset($_SESSION['ultimoQueSalio']) && 
    $_SERVER['PHP_AUTH_USER']==$_SESSION['ultimoQueSalio'])) {
        srand(make_seed());
        $randval = rand();
        trigger_error("1.5 PHP_AUTH_USER=".$_SERVER['PHP_AUTH_USER'].
        ", ultimoQueSalio=".$_SESSION['ultimoQueSalio'].
        ", randval=".$randval.
        ", autenticado=".$_SESSION['autenticado'].
        ", headers-sent=".(headers_sent()?"si":"no"));
        $_SESSION['ultimoQueSalio']='';
        $_SESSION['autenticado']='';
        header("WWW-Authenticate: Basic realm=\"Seguidor (".$randval.").  Puede usar login invitado y cualquier clave (si usa su correo electr�nico podr� recibir notificaci�n de respuestas a su solicitud o comentario). \"");
        header("HTTP/1.0 401 Unauthorized");
        echo $accno;
        exit;
    } 
    $_SESSION['ultimoQueSalio']='--';
    $usu=$_SERVER['PHP_AUTH_USER'];
    $pw=$_SERVER['PHP_AUTH_PW'];

    $options = array(
        'debug'       => 5
    );

    $dbh =& DB::connect($dsn, $options);
    if (PEAR::isError($dbh)) {
        $_SESSION['ultimoQueSalio']=$_SERVER['PHP_AUTH_USER'];
        $_SESSION['autenticado']='';
        die($dbh->getMessage());
    }

    if (!isset($_SESSION['autenticado'])  || 
        $_SESSION['autenticado']!=$usu) {

        $_SESSION['correo_invitado']="";
        if ($usu==$nom_invitado) {
            if (eregi("[a-z][-a-z_0-9.]*@[a-z][-a-z_0-9]*[.]\([a-z][-a-z_0-9]*[.]\)*[a-z][-a-z_0-9]*",$pw)) { // Posible correo, validar
               $_SESSION['correo_invitado']=$pw;
                trigger_error("Invitado con correo: $pw");
            }
        }
        else {
            $q="SELECT clave FROM responde WHERE id='".$dbh->quote($usu)."';";
            $sth=$dbh->query($q);
            if ($sth==NULL || PEAR::isError($sth)) {
                $_SESSION['ultimoQueSalio']=$_SERVER['PHP_AUTH_USER'];
                $_SESSION['autenticado']='';
                die($sth->getMessage());
            }
            $row=array();

            if (!$sth->fetchInto($row)) {
                //            echo "OJO clave mala, deber�a ser ".md5($_SERVER['PHP_AUTH_PW']); 
                $_SESSION['ultimoQueSalio']=$_SERVER['PHP_AUTH_USER'];
                $_SESSION['autenticado']='';
                die($accno);
            }

            if ($row[0]!=md5($_SERVER['PHP_AUTH_PW'])) {
                $_SESSION['ultimoQueSalio']=$_SERVER['PHP_AUTH_USER'];
                $_SESSION['autenticado']='';
                die($accno);
            }
            else  {
                $_SESSION['autenticado']=$usu;
            }
        }
    }
	return $dbh;
}


/** Da nombre a un nodo de un �rbol */
function stringFromNode($assoc, $k) {
	$codpapa=$assoc[$k][1];
	if ($codpapa=='') {
		return $assoc[$k][0];
	}
	else {
		return stringFromNode($assoc,$codpapa)." :: ".$assoc[$k][0];
	}
} 

/** Crea un arreglo con nombres de todos los nodos de un �rbol */
function arrayFromTree($assoc) {
	$nassoc=array();

	foreach($assoc as $k => $v) {
		$nassoc[$k]=stringFromNode($assoc,$k);
	}
	return $nassoc;
}

/**
 * Presenta pie de p�gina con opciones de navegaci�n
 * $nnuevo Si no es vaci� mensaje para poner como primera opci�n de navegaci�n
 * $enlnuevo URL al cual remitirse con nuevo (cuando no es vac�o).
 * $aut_usuario Usuario
 */
function navega($nnuevo, $enlnuevo, $aut_usuario) {
    echo "<table width='100%'><tr>";
    if ($nnuevo!="" && $enlnuevo!="") {
        echo "<td><a href='$enlnuevo'>$nnuevo</a></td>";
    }
    echo "<td><a href='escoger.php'>Solicitudes</a></td>".
        "<td><a href='tiempo.php'>Tiempo</a></td>".
        "<td><a href='contrap_lista.php'>Contraprestaci�n</td>".
        "<td><a href='micuenta.php'>Mi Cuenta</td>".
        "<td><a href='cuentas.php'>L. Usuarios</td>".
        "<td><a href='salir.php'>Salir (".$aut_usuario.
        ")</a></td></tr></table>\n";
}

