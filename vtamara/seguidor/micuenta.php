<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 foldmethod=marker: */

/**
* Seguidor de solicitudes y fallas.
* Dominio p�blico. 2006. 
* @author Vladimir T�mara. vtamara@pasosdeJesus.org

* Formulario para ver, editar datos de cuenta
**/


if (session_id()=="") {
	session_start();
}

require_once 'MDB2.php';
require_once 'HTML/Form.php';
require_once 'Mail.php';
include('aut/conf.php');
require_once 'misc.php';

error_reporting(E_ALL);

$ocorreo=Mail::factory('smtp',array('host' => 'localhost'));

$aut_usuario="";
$dbh = autenticaUsuario($dsn, $accno, $aut_usuario);

$tit="";

$ncol=array('id', 'nombre', 'direccion', 'telefono', 
    'correo', 'homepage', 'clave');
if (isset($_REQUEST['submit']) && $_REQUEST['submit']=='Actualizar') {
    if ($aut_usuario==$nom_invitado) {
        die("El usuario invitado no puede actualizar informaci�n");
    }        
	// Actualizar registro existente con UPDATE
	$q="UPDATE responde SET ";
	$sep="";
	foreach($ncol as $v) {
        if ($v=='clave' && $_REQUEST['claveplana']!='') {
            $r=md5($_REQUEST['claveplana']);
            $q.="$sep $v='".$r."'";
			$sep=",";
        }
        else if ($v!='id' && $v!='clave') {
			$r=$_REQUEST[$v]=='' ? 'NULL' : $dbh->quote($_REQUEST[$v]);
			$q.="$sep $v=".$r;
			$sep=",";
		}
	}
	$q.=" WHERE id=".$dbh->quote($aut_usuario).";";  
	$sth=$dbh->query($q);
	if (PEAR::isError($sth) || $sth==NULL) {
		die($sth->getMessage());
	}

	$tit="Comentario actualizado. ";
}

// Cargar datos de usuario
$tit.="Mi cuenta";
$q="SELECT ".implode(',',array_values($ncol)).
    " FROM responde WHERE id=".
    $dbh->quote($aut_usuario).";";  
$sth=$dbh->query($q);
if (PEAR::isError($sth) || $sth==NULL) {
    die($sth->getMessage());
}
$row = array();
if (!$sth->fetchInto($row)) {
    die("Usuario desconocido");
}
$campo=array();
foreach ($ncol as $k => $v) {
    $campo[$v]=$row[$k];
}

$form=new HTML_Form($_SERVER['PHP_SELF']);

echo "<html><head><title>$tit</title></head>\n";
echo "<body><h1>$tit</h1>\n";
echo "<hr>";

$form->addHidden("id", $campo['id']);

$form->addText("nombre","Nombre",
    $campo['nombre'],60,60);
$form->addText("direccion","Direccion",
    $campo['direccion'],60,60);
$form->addText("telefono","Telefono",
    $campo['telefono'],60,60);
$form->addText("correo","Correo",
    $campo['correo'],60,60);
$form->addText("homepage","P�gina personal",
    $campo['homepage'],60,60);
$form->addPassword("claveplana","Clave",
    '',60,60);

$form->addSubmit("submit", 'Actualizar'); 
$form->display(); 

$dbh->disconnect();

echo "<hr>";
navega('', '', $aut_usuario);
echo "</body></html>";
?> 
