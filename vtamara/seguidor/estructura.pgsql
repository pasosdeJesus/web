--
-- Estructura de la base de datos para seguidor
-- Dominio público. 2004. Sin garantías. vtamara@users.sourceforge.net
--


-- responde: personas que pueden responder una solicitud
--

CREATE TABLE responde (
	id VARCHAR(16) PRIMARY KEY,
	nombre VARCHAR(100) NOT NULL,
	direccion VARCHAR(100),
	telefono VARCHAR(100),
	correo VARCHAR(100),
	homepage VARCHAR(200),
	clave	VARCHAR(50) NOT NULL,  --- con md5
	habilitado BOOLEAN DEFAULT TRUE
);


-- 
--  categoria: posibles categorias y subcategorias
--

CREATE TABLE categoria (
	codigo INTEGER PRIMARY KEY,
	desc_corta VARCHAR(50) NOT NULL,
	desc_larga VARCHAR(250),
	cod_mama INTEGER references categoria
);

-- 
--  institución: posibles dependencias y subdependencias
--

CREATE TABLE dependencia (
	codigo INTEGER PRIMARY KEY,
	desc_corta VARCHAR(50) NOT NULL,
	desc_larga VARCHAR(200),
	direccion VARCHAR(100),
	telefono VARCHAR(100),
	homepage VARCHAR(200),
	contacto VARCHAR(16) REFERENCES responde,
	cod_mama INTEGER REFERENCES dependencia
);


--
-- solicitud: una solicitud de seguimiento
--

CREATE TABLE solicitud (
	codigo 		SERIAL PRIMARY KEY,
	cod_categoria 	INTEGER REFERENCES categoria,
	abierto		BOOL, -- t Abierto, f Cerrado
	cod_responde	VARCHAR(16) REFERENCES responde, -- Responsable asignado
	cod_enviado 	VARCHAR(16) NOT NULL REFERENCES responde, -- Quien envió
	correo_invitado VARCHAR(100), --En caso de usuario invitado con correo autenticado, correo al cual notificar
	dependencia	INTEGER REFERENCES dependencia,
	prioridad	INTEGER CHECK (prioridad>='1' AND prioridad<='5'),
	apertura	DATE NOT NULL,
	cierre		DATE CHECK (cierre IS NULL or cierre>=apertura),
	horas		NUMERIC,
	desc_corta	VARCHAR(60) NOT NULL,
	desc_detallada	VARCHAR(1000),
	repetir		VARCHAR(1000), 	-- en caso de fallas como repetir
	solucion	VARCHAR(1000),	-- en caso de fallas
	porcentaje	INTEGER CHECK (porcentaje>='0' AND porcentaje <='100'
				AND porcentaje % '5'='0'),		-- porcentaje de resolución
	subsolicitud_de INTEGER REFERENCES solicitud
);

--
-- Una solicitud puede tener solicitudes prerequisito
CREATE TABLE prerequisito (
	cod_sol		INTEGER NOT NULL REFERENCES solicitud,
	cod_solpre	INTEGER NOT NULL REFERENCES solicitud
);


-- 
-- comentario: comentario a una solicitud de seguimiento (puede ser progreso)
--

CREATE TABLE comentario (
	codigo 		 SERIAL PRIMARY KEY,
	cod_solicitud 	 INTEGER REFERENCES solicitud,
	respuesta_a 	 INTEGER REFERENCES comentario,
   	fecha		 DATE NOT NULL,	
	cod_enviado 	 VARCHAR(16) NOT NULL REFERENCES responde,	
	correo_invitado	 VARCHAR(100), --correo validado al cual notificar en caso de invitado
	desc_corta	 VARCHAR(60) NOT NULL,
	comentario	 VARCHAR(1000),
	tiempo_invertido NUMERIC --tiempo, util para comentarios de adm.
);

-- 
-- contraprestacion: contraprestación en forma de trueque la hora es la 
-- unidad de comparación
--

CREATE TABLE contraprestacion (
	codigo 		 SERIAL PRIMARY KEY,
	da		VARCHAR(16) NOT NULL REFERENCES responde, 
	recibe		VARCHAR(16) NOT NULL REFERENCES responde, 
   	fecha		 DATE NOT NULL,	
	horas		 NUMERIC NOT NULL,
	desc_corta	 VARCHAR(60) NOT NULL,
	desc_detallada	 VARCHAR(1000)
);


