<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 foldmethod=marker: */


/**
* Seguidor de solicitudes y fallas.
* Dominio p�blico. 2004. 
* @author Vladimir T�mara. vtamara@users.sourceforge.net

* Formulario para ver tiempos invertidos por desarrolladores
**/

if (session_id()=="") {
	session_start();
}
require_once 'MDB2.php';
require_once 'HTML/Form.php';
include('aut/conf.php');
require_once 'misc.php';

error_reporting(E_ALL);

$aut_usuario="";
$dbh = autenticaUsuario($dsn, $accno, $aut_usuario);


$tit="Tiempo invertido en soluciones";
echo "<html><head><title>$tit</title></head>\n";
echo "<body><h1>$tit</h1>\n";

$q="SELECT id FROM responde ORDER by id";
//echo "CONSULTA *$q*";
$sth=$dbh->query($q);
if (PEAR::isError($sth)) {
    die($sth->getMessage());
}

echo "<table border='1'><tr><th>Id</th><th>Comentarios</th><th>Tiempo</th><th>Retribuido</th>";
$row=array();

while ($sth->fetchInto($row)) {
	$id=$row[0];
	$s="SELECT sum(horas) FROM contraprestacion WHERE recibe='$id';";
	$sq=$dbh->query($s);
	if (PEAR::isError($sq)) {
    		die($sq->getMessage());
	}
    $row2=array();
    $sq->fetchInto($row2);
    $horascont=$row2[0];

	echo "<tr><td>$id</td>";
	$s="SELECT cod_solicitud, fecha, desc_corta, tiempo_invertido FROM comentario WHERE cod_enviado='$id' ORDER by fecha";
	$sq=$dbh->query($s);
	if (PEAR::isError($sq)) {
    		die($sq->getMessage());
	}
	echo "<td>";
	$row2=array();
	$tt=0;
	while ($sq->fetchInto($row2)) {

		$tt+=$row2[3]; 
        if ($tt>$horascont) {
            echo "<b>";
        }
		echo "<a href='solicitud.php?codigo=".$row2[0]."'>".$row2[1]." ".$row2[2]."(".$row2[3].")</a>";
        if ($tt>$horascont) {
            echo "</b>";
        }
        echo "<br>"; 
	}

	echo "</td><td>".$tt."</td>"; 
    echo "<td>".$horascont."</td>";

	echo "</tr>";

}
echo "</table>";

echo "<hr>\n";

navega("","",$aut_usuario);

?> 
