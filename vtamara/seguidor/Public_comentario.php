<?php
/**
 * Table Definition for public.comentario
 */
require_once 'DB/DataObject.php';

class DataObjects_Public_comentario extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'public.comentario';               // table name
    public $codigo;                          // int4(4)  
    public $cod_solicitud;                   // int4(4)  
    public $respuesta_a;                     // int4(4)  
    public $fecha;                           // date(4)  
    public $cod_enviado;                     // varchar(-1)  
    public $correo_invitado;                 // varchar(-1)  
    public $desc_corta;                      // varchar(-1)  
    public $comentario;                      // varchar(-1)  
    public $tiempo_invertido;                // numeric(-1)  

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Public_comentario',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
